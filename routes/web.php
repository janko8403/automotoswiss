<?php
use App\CarHome;
use App\CarScauto;
use App\CarAllianz;
use App\CarRest;
use Carbon\Carbon;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
	$mytime = Carbon::now();
	$homes = CarHome::where('end_date_of_auction', '>', $mytime)->orderBy('end_date_of_auction', 'asc')->paginate(12);
    return view('welcome', compact('homes'));
});


// Pages
Route::get('/o-nas', 'PagesController@about');
Route::get('/transport', 'PagesController@transport');
Route::get('/cookie', 'PagesController@cookie');

Route::resource('/rest', 'RestController');
Route::resource('/scauto', 'ScautoController');
Route::resource('/allianz', 'AllianzController');
Route::get('/axa', 'HomeCarController@index');
Route::get('/home/{id}', 'HomeCarController@show');

// Moje aukcje
Route::resource('/moje-aukcje', 'MyAuctionsController');

// Auta sprowadzone
Route::resource('/aukcje', 'AuctionsController');

// Opis
Route::resource('/opis', 'ShowController');

// Dashboard
Route::get('cms/dashboard', 'DashboardController@dashboard');

// Aktualizacja bazy danych
Route::get('/cms/update', 'UpdateController@index');
Route::get('/cms/update/update_rest', 'UpdateController@update_rest');
Route::get('/cms/update/update_allianz', 'UpdateController@update_allianz');
Route::get('/cms/update/update_scauto', 'UpdateController@update_scauto');
Route::get('/cms/update/update_home', 'UpdateController@update_home');

// Licytacje klientów
Route::get('/cms/customers', 'CustomersController@customers');
Route::get('/cms/customers/{user_id}', 'CustomersController@show');

// Edit pages
Route::resource('/cms/page', 'EditPagesController');

// Users
Route::resource('cms/user', 'UsersController');

// Cars
Route::resource('cms/car', 'CarsController');

// Register
Route::get('/zarejestruj-sie', 'RegController@index');
Route::post('/zarejestruj-sie', 'RegController@store');

// Login
Route::get('/zaloguj-sie', 'LoginUserController@index');

// Form
Route::resource('/kontakt', 'FormController');

// Search
Route::get('/search', 'SearchController@search');

// Access denied
Route::get('/brak-dostepu', 'AccessDeniedController@index');


// Route::get('/cars_json', 'CarsJsonController@cars_json');



Auth::routes();


