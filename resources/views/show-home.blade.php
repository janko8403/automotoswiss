@extends('page.left')

@section('content')

    @php 
        $mytime = Carbon\Carbon::now(new DateTimeZone('Europe/Berlin'));
    @endphp

    @if (Session::has('add_auction'))
        <div class="alert-message" role="alert">
            <div class="row">
                <div class="col-md-12">
                    <div class="message">
                        {{Session::get('add_auction')}}
                    </div>
                </div>
            </div>
        </div>
    @endif

    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12 show">
                    <div class="card">
                        <div class="card-header" data-background-color="orange">
                            <h4 class="title">{{ $home->car_name }}</h4>
                            <p class="category">Czas do końca aukcji: <span>{!! ($home->end_date_of_auction < $mytime) ? 'aukcja zakończona' : $home->end_date_of_auction !!}</span></p>
                        </div>
                        <div class="card-content">
                            <p><a target="_blank" href="{{ $home->originalauctionlink }}">Zobacz link do oryginalnej aukcji <i class="material-icons">link</i></a></p>
                            
                            <br>
                            <div class="row">
                                <div class="col-md-6 col-md-offset-3">
                                    <img src="{{ $home->photos_0 }}" alt="{{ $home->marka }}" class="img-responsive img-center">
                                    <br>
                                </div>
                                <div class="col-md-12">
                                    <div class="row">
                                        @if (!empty($home->photos_1))
                                            <div class="col-md-2">
                                                <div class="img-article">
                                                    <a href="{{ $home->photos_1 }}" data-toggle="lightbox" data-gallery="example-gallery">
                                                        <img src="{{ $home->photos_1 }}" alt="{{ $home->marka }}" class="img-responsive img-center">
                                                    </a>
                                                </div>
                                            </div>
                                        @endif
                                        @if (!empty($home->photos_2))
                                            <div class="col-md-2">
                                                <div class="img-article">
                                                    <a href="{{ $home->photos_2 }}" data-toggle="lightbox" data-gallery="example-gallery">
                                                        <img src="{{ $home->photos_2 }}" alt="{{ $home->marka }}" class="img-responsive img-center">
                                                    </a>
                                                </div>
                                            </div>
                                         @endif
                                        @if (!empty($home->photos_3))
                                            <div class="col-md-2">
                                                <div class="img-article">
                                                    <a href="{{ $home->photos_3 }}" data-toggle="lightbox" data-gallery="example-gallery">
                                                        <img src="{{ $home->photos_3 }}" alt="{{ $home->marka }}" class="img-responsive img-center">
                                                    </a>
                                                </div>
                                            </div>
                                         @endif
                                        @if (!empty($home->photos_4))
                                            <div class="col-md-2">
                                                <div class="img-article">
                                                    <a href="{{ $home->photos_4 }}" data-toggle="lightbox" data-gallery="example-gallery">
                                                        <img src="{{ $home->photos_4 }}" alt="{{ $home->marka }}" class="img-responsive img-center">
                                                    </a>
                                                </div>
                                            </div>
                                         @endif
                                        @if (!empty($home->photos_5))
                                            <div class="col-md-2">
                                                <div class="img-article">
                                                    <a href="{{ $home->photos_5 }}" data-toggle="lightbox" data-gallery="example-gallery">
                                                        <img src="{{ $home->photos_5 }}" alt="{{ $home->marka }}" class="img-responsive img-center">
                                                    </a>
                                                </div>
                                            </div>
                                         @endif
                                        @if (!empty($home->photos_6))
                                            <div class="col-md-2">
                                                <div class="img-article">
                                                    <a href="{{ $home->photos_6 }}" data-toggle="lightbox" data-gallery="example-gallery">
                                                        <img src="{{ $home->photos_6 }}" alt="{{ $home->marka }}" class="img-responsive img-center">
                                                    </a>
                                                </div>
                                            </div>
                                         @endif
                                    </div>
                
                                    <div class="empty-space"></div>

                                    <div class="row">
                                        @if (!empty($home->photos_7))
                                            <div class="col-md-2">
                                                <div class="img-article">
                                                    <a href="{{ $home->photos_7 }}" data-toggle="lightbox" data-gallery="example-gallery">
                                                        <img src="{{ $home->photos_7 }}" alt="{{ $home->marka }}" class="img-responsive img-center">
                                                    </a>
                                                </div>
                                            </div>
                                        @endif
                                        @if (!empty($home->photos_8))
                                            <div class="col-md-2">
                                                <div class="img-article">
                                                    <a href="{{ $home->photos_8 }}" data-toggle="lightbox" data-gallery="example-gallery">
                                                        <img src="{{ $home->photos_8 }}" alt="{{ $home->marka }}" class="img-responsive img-center">
                                                    </a>
                                                </div>
                                            </div>
                                        @endif
                                        @if (!empty($home->photos_9))
                                            <div class="col-md-2">
                                                <div class="img-article">
                                                    <a href="{{ $home->photos_9 }}" data-toggle="lightbox" data-gallery="example-gallery">
                                                        <img src="{{ $home->photos_9 }}" alt="{{ $home->marka }}" class="img-responsive img-center">
                                                    </a>
                                                </div>
                                            </div>
                                        @endif
                                        @if (!empty($home->photos_10))
                                            <div class="col-md-2">
                                                <div class="img-article">
                                                    <a href="{{ $home->photos_10 }}" data-toggle="lightbox" data-gallery="example-gallery">
                                                        <img src="{{ $home->photos_10 }}" alt="{{ $home->marka }}" class="img-responsive img-center">
                                                    </a>
                                                </div>
                                            </div>
                                        @endif
                                        @if (!empty($home->photos_11))
                                            <div class="col-md-2">
                                                <div class="img-article">
                                                    <a href="{{ $home->photos_11 }}" data-toggle="lightbox" data-gallery="example-gallery">
                                                        <img src="{{ $home->photos_11 }}" alt="{{ $home->marka }}" class="img-responsive img-center">
                                                    </a>
                                                </div>
                                            </div>
                                        @endif
                                        @if (!empty($home->photos_12))
                                            <div class="col-md-2">
                                                <div class="img-article">
                                                    <a href="{{ $home->photos_12 }}" data-toggle="lightbox" data-gallery="example-gallery">
                                                        <img src="{{ $home->photos_12 }}" alt="{{ $home->marka }}" class="img-responsive img-center">
                                                    </a>
                                                </div>
                                            </div>
                                        @endif
                                    </div>

                                    <div class="empty-space"></div>
                                    
                                    <div class="row">
                                        @if (!empty($home->photos_13))
                                            <div class="col-md-2">
                                                <div class="img-article">
                                                    <a href="{{ $home->photos_13 }}" data-toggle="lightbox" data-gallery="example-gallery">
                                                        <img src="{{ $home->photos_13 }}" alt="{{ $home->marka }}" class="img-responsive img-center">
                                                    </a>
                                                </div>
                                            </div>
                                        @endif
                                        @if (!empty($home->photos_14))
                                            <div class="col-md-2">
                                                <div class="img-article">
                                                    <a href="{{ $home->photos_14 }}" data-toggle="lightbox" data-gallery="example-gallery">
                                                        <img src="{{ $home->photos_14 }}" alt="{{ $home->marka }}" class="img-responsive img-center">
                                                    </a>
                                                </div>
                                            </div>
                                        @endif
                                        @if (!empty($home->photos_15))
                                            <div class="col-md-2">
                                                <div class="img-article">
                                                    <a href="{{ $home->photos_15 }}" data-toggle="lightbox" data-gallery="example-gallery">
                                                        <img src="{{ $home->photos_15 }}" alt="{{ $home->marka }}" class="img-responsive img-center">
                                                    </a>
                                                </div>
                                            </div>
                                        @endif
                                        @if (!empty($home->photos_16))
                                            <div class="col-md-2">
                                                <div class="img-article">
                                                    <a href="{{ $home->photos_16 }}" data-toggle="lightbox" data-gallery="example-gallery">
                                                        <img src="{{ $home->photos_16 }}" alt="{{ $home->marka }}" class="img-responsive img-center">
                                                    </a>
                                                </div>
                                            </div>
                                        @endif
                                        @if (!empty($home->photos_17))
                                            <div class="col-md-2">
                                                <div class="img-article">
                                                    <a href="{{ $home->photos_17 }}" data-toggle="lightbox" data-gallery="example-gallery">
                                                        <img src="{{ $home->photos_17 }}" alt="{{ $home->marka }}" class="img-responsive img-center">
                                                    </a>
                                                </div>
                                            </div>
                                        @endif
                                        @if (!empty($home->photos_18))
                                            <div class="col-md-2">
                                                <div class="img-article">
                                                    <a href="{{ $home->photos_18 }}" data-toggle="lightbox" data-gallery="example-gallery">
                                                        <img src="{{ $home->photos_18 }}" alt="{{ $home->marka }}" class="img-responsive img-center">
                                                    </a>
                                                </div>
                                            </div>
                                        @endif
                                    </div>

                                    <div class="empty-space"></div>
                                    
                                    <div class="row">
                                        @if (!empty($home->photos_19))
                                            <div class="col-md-2">
                                                <div class="img-article">
                                                    <a href="{{ $home->photos_19 }}" data-toggle="lightbox" data-gallery="example-gallery">
                                                        <img src="{{ $home->photos_19 }}" alt="{{ $home->marka }}" class="img-responsive img-center">
                                                    </a>
                                                </div>
                                            </div>
                                        @endif
                                        @if (!empty($home->photos_20))
                                            <div class="col-md-2">
                                                <div class="img-article">
                                                    <a href="{{ $home->photos_20 }}" data-toggle="lightbox" data-gallery="example-gallery">
                                                        <img src="{{ $home->photos_20 }}" alt="{{ $home->marka }}" class="img-responsive img-center">
                                                    </a>
                                                </div>
                                            </div>
                                        @endif
                                        @if (!empty($home->photos_21))
                                            <div class="col-md-2">
                                                <div class="img-article">
                                                    <a href="{{ $home->photos_21 }}" data-toggle="lightbox" data-gallery="example-gallery">
                                                        <img src="{{ $home->photos_21 }}" alt="{{ $home->marka }}" class="img-responsive img-center">
                                                    </a>
                                                </div>
                                            </div>
                                        @endif
                                        @if (!empty($home->photos_22))
                                            <div class="col-md-2">
                                                <div class="img-article">
                                                    <a href="{{ $home->photos_22 }}" data-toggle="lightbox" data-gallery="example-gallery">
                                                        <img src="{{ $home->photos_22 }}" alt="{{ $home->marka }}" class="img-responsive img-center">
                                                    </a>
                                                </div>
                                            </div>
                                        @endif
                                        @if (!empty($home->photos_23))
                                            <div class="col-md-2">
                                                <div class="img-article">
                                                    <a href="{{ $home->photos_23 }}" data-toggle="lightbox" data-gallery="example-gallery">
                                                        <img src="{{ $home->photos_23 }}" alt="{{ $home->marka }}" class="img-responsive img-center">
                                                    </a>
                                                </div>
                                            </div>
                                        @endif
                                        @if (!empty($home->photos_24))
                                            <div class="col-md-2">
                                                <div class="img-article">
                                                    <a href="{{ $home->photos_24 }}" data-toggle="lightbox" data-gallery="example-gallery">
                                                        <img src="{{ $home->photos_24 }}" alt="{{ $home->marka }}" class="img-responsive img-center">
                                                    </a>
                                                </div>
                                            </div>
                                        @endif
                                    </div>

                                    <div class="empty-space"></div>
                                    
                                    <div class="row">
                                        @if (!empty($home->photos_25))
                                            <div class="col-md-2">
                                                <div class="img-article">
                                                    <a href="{{ $home->photos_25 }}" data-toggle="lightbox" data-gallery="example-gallery">
                                                        <img src="{{ $home->photos_25 }}" alt="{{ $home->marka }}" class="img-responsive img-center">
                                                    </a>
                                                </div>
                                            </div>
                                        @endif
                                        @if (!empty($home->photos_26))
                                            <div class="col-md-2">
                                                <div class="img-article">
                                                    <a href="{{ $home->photos_26 }}" data-toggle="lightbox" data-gallery="example-gallery">
                                                        <img src="{{ $home->photos_26 }}" alt="{{ $home->marka }}" class="img-responsive img-center">
                                                    </a>
                                                </div>
                                            </div>
                                        @endif
                                        @if (!empty($home->photos_27))
                                            <div class="col-md-2">
                                                <div class="img-article">
                                                    <a href="{{ $home->photos_27 }}" data-toggle="lightbox" data-gallery="example-gallery">
                                                        <img src="{{ $home->photos_27 }}" alt="{{ $home->marka }}" class="img-responsive img-center">
                                                    </a>
                                                </div>
                                            </div>
                                        @endif
                                        @if (!empty($home->photos_28))
                                            <div class="col-md-2">
                                                <div class="img-article">
                                                    <a href="{{ $home->photos_28 }}" data-toggle="lightbox" data-gallery="example-gallery">
                                                        <img src="{{ $home->photos_28 }}" alt="{{ $home->marka }}" class="img-responsive img-center">
                                                    </a>
                                                </div>
                                            </div>
                                        @endif
                                        @if (!empty($home->photos_29))
                                            <div class="col-md-2">
                                                <div class="img-article">
                                                    <a href="{{ $home->photos_29 }}" data-toggle="lightbox" data-gallery="example-gallery">
                                                        <img src="{{ $home->photos_29 }}" alt="{{ $home->marka }}" class="img-responsive img-center">
                                                    </a>
                                                </div>
                                            </div>
                                        @endif
                                        @if (!empty($home->photos_30))
                                            <div class="col-md-2">
                                                <div class="img-article">
                                                    <a href="{{ $home->photos_30 }}" data-toggle="lightbox" data-gallery="example-gallery">
                                                        <img src="{{ $home->photos_30 }}" alt="{{ $home->marka }}" class="img-responsive img-center">
                                                    </a>
                                                </div>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <h6><strong>Model:</strong> {{ $home->car_name }}</h6>
                                    <h6><strong>Opis:</strong></h6>
                                    {!! $home->description !!}
                                </div>
                            </div>

                        </div>
                    </div>

                    @if (Auth::check() && is_user())
                        <div class="card" style="{!! ($home->end_date_of_auction < $mytime) ? 'display:none' : '' !!}">
                            
                            <div class="card-content">
                                <div class="empty-space"></div>
                                <h4>Podaj swoją cenę we <strong>frankach szwajcarskich (Chf)</strong></h4>
                                <p style="font-size: 11px;"><strong>Oferta musi być wyższa niż poprzednia oferta. W przeciwnym wypadku nie zostanie umieszczona.</strong></p>


                                <div class="row">
                                    <div class="col-md-4">
                                        <form role="form" method="POST" action="{{ url('/opis') }}">
                                            {{ csrf_field() }}

                                            <input type="hidden" name="operator" value="{{ $home->operator }}">
                                            <input type="hidden" name="date" value="{{ $home->end_date_of_auction }}">
                                            <input type="hidden" name="name" value="{{ $home->car_name }}">
                                            <input type="hidden" name="auct_id" value="{{ $home->id }}">
                                            <input type="hidden" name="user_id" value="{{ Auth::user()->id }}">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group label-floating {{ $errors->has('price') ? ' has-error' : '' }}">
                                                        <label class="control-label">Twoja cena</label>
                                                        <input type="number" class="form-control" name="price" value="{{ old('price') }}">

                                                        @if ($errors->has('price'))
                                                            <span class="help-block">
                                                                <strong>{{ $errors->first('price') }}</strong>
                                                            </span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            <button type="submit" class="btn btn-orange">Licytuję</button>
                                            <div class="clearfix"></div>
                                        </form>
                                    </div>
                                </div>
                            </div>

                        </div>
                    @else
                        <div class="card">
                            <div class="card-content">
                                <div class="empty-space"></div>
                                <h4>Musisz być zalogowanym aby licytować!</h4>
                            </div>
                        </div>
                    @endif

                </div>
            </div>
        </div>
    </div>

@endsection