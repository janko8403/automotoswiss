@extends('page.left')

@section('content')

    <div class="content">
        <div class="container-fluid">
            <div class="row">

                @if ($cars->isEmpty())
                    <div class="alert alert-danger" role="alert">
                        Brak sprowadzonych pojazdów
                    </div>
                @else

                    @foreach ($cars as $car)
                        <div class="col-lg-4 col-md-6 col-sm-6">
                            <div class="card card-stats">
                                <div class="card-header-main">
                                    <img src="{{ asset('storage/car/' . str_slug($car->model, '-') . '/img' . $car->images) }}" alt="{{ $car->marka }} {{ $car->model }} {{ $car->version }}">
                                </div>
                                <div class="card-content-car">
                                    <div class="box-title-main">
                                        <h3 class="title">{{ $car->marka }} {{ $car->model }} {{ $car->version }} {{ $car->old }}</h3>
                                    </div>
                                        
                                    <div class="time-left">Przebieg:<span> {{ $car->capacity }}</span></div>
                                </div>
                                <div class="card-footer">
                                    <a class="stats" href="{{ url('/aukcje/' . $car->id) }}">Sprawdź</a>
                                </div>
                            </div>
                        </div>
                    @endforeach

                @endif

            </div>
        </div>
    </div>

@endsection