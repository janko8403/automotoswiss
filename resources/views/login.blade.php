@extends('page.left')

@section('content')

    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12 show">
                    <div class="card">
                        <div class="card-header" data-background-color="orange">
                            <h4 class="title">Zaloguj się</h4>
                            <p class ="category">Wpisz hasło dostępu</p>
                        </div>
                        <div class="card-content">
                            <div class="col-md-10 col-md-offset-1">
                                <form role="form" method="POST" action="{{ url('/login') }}">
                                    {{ csrf_field() }}
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group label-floating {{ $errors->has('email') ? ' has-error' : '' }}">
                                                <label class="control-label">Email</label>
                                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>

                                                @if ($errors->has('email'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('email') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group label-floating {{ $errors->has('password') ? ' has-error' : '' }}">
                                                <label class="control-label">Hasło</label>
                                                <input id="password" type="password" class="form-control" name="password" required>

                                                @if ($errors->has('password'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('password') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <button type="submit" class="btn btn-orange">Zaloguj się</button>
                                    <div class="clearfix"></div>
                                </form>
                            </div>
                            <div class="empty-space"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
@endsection