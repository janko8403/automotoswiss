@extends('page.left')

@section('content')

    @php 
        $mytime = Carbon\Carbon::now(new DateTimeZone('Europe/Berlin'));
    @endphp

    @if (Session::has('add_auction'))
        <div class="alert-message" role="alert">
            <div class="row">
                <div class="col-md-12">
                    <div class="message">
                        {{Session::get('add_auction')}}
                    </div>
                </div>
            </div>
        </div>
    @endif

    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12 show">
                    <div class="card">
                        <div class="card-header" data-background-color="orange">
                            <h4 class="title">{{ $allianz->marka }} {{ $allianz->typ }}{{ $allianz->model }}</h4>
                            <p class="category">Czas do końca aukcji: <span>{!! ($allianz->end_date_of_auction < $mytime) ? 'aukcja zakończona' : $allianz->end_date_of_auction !!}</span></p>
                        </div>
                        <div class="card-content">
                            <br>
                            <div class="row">
                                <div class="col-md-6 col-md-offset-3">
                                    <img src="{{ $allianz->photos_0 }}" alt="{{ $allianz->marka }}" class="img-responsive img-center">
                                    <br>
                                </div>
                                <div class="col-md-12">
                                    <div class="row">
                                        @if (!empty($allianz->photos_1))
                                            <div class="col-md-2">
                                                <div class="img-article">
                                                    <a href="{{ $allianz->photos_1 }}" data-toggle="lightbox" data-gallery="example-gallery">
                                                        <img src="{{ $allianz->photos_1 }}" alt="{{ $allianz->marka }}" class="img-responsive img-center">
                                                    </a>
                                                </div>
                                            </div>
                                        @endif
                                        @if (!empty($allianz->photos_2))
                                            <div class="col-md-2">
                                                <div class="img-article">
                                                    <a href="{{ $allianz->photos_2 }}" data-toggle="lightbox" data-gallery="example-gallery">
                                                        <img src="{{ $allianz->photos_2 }}" alt="{{ $allianz->marka }}" class="img-responsive img-center">
                                                    </a>
                                                </div>
                                            </div>
                                         @endif
                                        @if (!empty($allianz->photos_3))
                                            <div class="col-md-2">
                                                <div class="img-article">
                                                    <a href="{{ $allianz->photos_3 }}" data-toggle="lightbox" data-gallery="example-gallery">
                                                        <img src="{{ $allianz->photos_3 }}" alt="{{ $allianz->marka }}" class="img-responsive img-center">
                                                    </a>
                                                </div>
                                            </div>
                                         @endif
                                        @if (!empty($allianz->photos_4))
                                            <div class="col-md-2">
                                                <div class="img-article">
                                                    <a href="{{ $allianz->photos_4 }}" data-toggle="lightbox" data-gallery="example-gallery">
                                                        <img src="{{ $allianz->photos_4 }}" alt="{{ $allianz->marka }}" class="img-responsive img-center">
                                                    </a>
                                                </div>
                                            </div>
                                         @endif
                                        @if (!empty($allianz->photos_5))
                                            <div class="col-md-2">
                                                <div class="img-article">
                                                    <a href="{{ $allianz->photos_5 }}" data-toggle="lightbox" data-gallery="example-gallery">
                                                        <img src="{{ $allianz->photos_5 }}" alt="{{ $allianz->marka }}" class="img-responsive img-center">
                                                    </a>
                                                </div>
                                            </div>
                                         @endif
                                        @if (!empty($allianz->photos_6))
                                            <div class="col-md-2">
                                                <div class="img-article">
                                                    <a href="{{ $allianz->photos_6 }}" data-toggle="lightbox" data-gallery="example-gallery">
                                                        <img src="{{ $allianz->photos_6 }}" alt="{{ $allianz->marka }}" class="img-responsive img-center">
                                                    </a>
                                                </div>
                                            </div>
                                         @endif
                                    </div>
                
                                    <div class="empty-space"></div>

                                    <div class="row">
                                        @if (!empty($allianz->photos_7))
                                            <div class="col-md-2">
                                                <div class="img-article">
                                                    <a href="{{ $allianz->photos_7 }}" data-toggle="lightbox" data-gallery="example-gallery">
                                                        <img src="{{ $allianz->photos_7 }}" alt="{{ $allianz->marka }}" class="img-responsive img-center">
                                                    </a>
                                                </div>
                                            </div>
                                        @endif
                                        @if (!empty($allianz->photos_8))
                                            <div class="col-md-2">
                                                <div class="img-article">
                                                    <a href="{{ $allianz->photos_8 }}" data-toggle="lightbox" data-gallery="example-gallery">
                                                        <img src="{{ $allianz->photos_8 }}" alt="{{ $allianz->marka }}" class="img-responsive img-center">
                                                    </a>
                                                </div>
                                            </div>
                                        @endif
                                        @if (!empty($allianz->photos_9))
                                            <div class="col-md-2">
                                                <div class="img-article">
                                                    <a href="{{ $allianz->photos_9 }}" data-toggle="lightbox" data-gallery="example-gallery">
                                                        <img src="{{ $allianz->photos_9 }}" alt="{{ $allianz->marka }}" class="img-responsive img-center">
                                                    </a>
                                                </div>
                                            </div>
                                        @endif
                                        @if (!empty($allianz->photos_10))
                                            <div class="col-md-2">
                                                <div class="img-article">
                                                    <a href="{{ $allianz->photos_10 }}" data-toggle="lightbox" data-gallery="example-gallery">
                                                        <img src="{{ $allianz->photos_10 }}" alt="{{ $allianz->marka }}" class="img-responsive img-center">
                                                    </a>
                                                </div>
                                            </div>
                                        @endif
                                        @if (!empty($allianz->photos_11))
                                            <div class="col-md-2">
                                                <div class="img-article">
                                                    <a href="{{ $allianz->photos_11 }}" data-toggle="lightbox" data-gallery="example-gallery">
                                                        <img src="{{ $allianz->photos_11 }}" alt="{{ $allianz->marka }}" class="img-responsive img-center">
                                                    </a>
                                                </div>
                                            </div>
                                        @endif
                                        @if (!empty($allianz->photos_12))
                                            <div class="col-md-2">
                                                <div class="img-article">
                                                    <a href="{{ $allianz->photos_12 }}" data-toggle="lightbox" data-gallery="example-gallery">
                                                        <img src="{{ $allianz->photos_12 }}" alt="{{ $allianz->marka }}" class="img-responsive img-center">
                                                    </a>
                                                </div>
                                            </div>
                                        @endif
                                    </div>

                                    <div class="empty-space"></div>
                                    
                                    <div class="row">
                                        @if (!empty($allianz->photos_13))
                                            <div class="col-md-2">
                                                <div class="img-article">
                                                    <a href="{{ $allianz->photos_13 }}" data-toggle="lightbox" data-gallery="example-gallery">
                                                        <img src="{{ $allianz->photos_13 }}" alt="{{ $allianz->marka }}" class="img-responsive img-center">
                                                    </a>
                                                </div>
                                            </div>
                                        @endif
                                        @if (!empty($allianz->photos_14))
                                            <div class="col-md-2">
                                                <div class="img-article">
                                                    <a href="{{ $allianz->photos_14 }}" data-toggle="lightbox" data-gallery="example-gallery">
                                                        <img src="{{ $allianz->photos_14 }}" alt="{{ $allianz->marka }}" class="img-responsive img-center">
                                                    </a>
                                                </div>
                                            </div>
                                        @endif
                                        @if (!empty($allianz->photos_15))
                                            <div class="col-md-2">
                                                <div class="img-article">
                                                    <a href="{{ $allianz->photos_15 }}" data-toggle="lightbox" data-gallery="example-gallery">
                                                        <img src="{{ $allianz->photos_15 }}" alt="{{ $allianz->marka }}" class="img-responsive img-center">
                                                    </a>
                                                </div>
                                            </div>
                                        @endif
                                        @if (!empty($allianz->photos_16))
                                            <div class="col-md-2">
                                                <div class="img-article">
                                                    <a href="{{ $allianz->photos_16 }}" data-toggle="lightbox" data-gallery="example-gallery">
                                                        <img src="{{ $allianz->photos_16 }}" alt="{{ $allianz->marka }}" class="img-responsive img-center">
                                                    </a>
                                                </div>
                                            </div>
                                        @endif
                                        @if (!empty($allianz->photos_17))
                                            <div class="col-md-2">
                                                <div class="img-article">
                                                    <a href="{{ $allianz->photos_17 }}" data-toggle="lightbox" data-gallery="example-gallery">
                                                        <img src="{{ $allianz->photos_17 }}" alt="{{ $allianz->marka }}" class="img-responsive img-center">
                                                    </a>
                                                </div>
                                            </div>
                                        @endif
                                        @if (!empty($allianz->photos_18))
                                            <div class="col-md-2">
                                                <div class="img-article">
                                                    <a href="{{ $allianz->photos_18 }}" data-toggle="lightbox" data-gallery="example-gallery">
                                                        <img src="{{ $allianz->photos_18 }}" alt="{{ $allianz->marka }}" class="img-responsive img-center">
                                                    </a>
                                                </div>
                                            </div>
                                        @endif
                                    </div>

                                    <div class="empty-space"></div>
                                    
                                    <div class="row">
                                        @if (!empty($allianz->photos_19))
                                            <div class="col-md-2">
                                                <div class="img-article">
                                                    <a href="{{ $allianz->photos_19 }}" data-toggle="lightbox" data-gallery="example-gallery">
                                                        <img src="{{ $allianz->photos_19 }}" alt="{{ $allianz->marka }}" class="img-responsive img-center">
                                                    </a>
                                                </div>
                                            </div>
                                        @endif
                                        @if (!empty($allianz->photos_20))
                                            <div class="col-md-2">
                                                <div class="img-article">
                                                    <a href="{{ $allianz->photos_20 }}" data-toggle="lightbox" data-gallery="example-gallery">
                                                        <img src="{{ $allianz->photos_20 }}" alt="{{ $allianz->marka }}" class="img-responsive img-center">
                                                    </a>
                                                </div>
                                            </div>
                                        @endif
                                        @if (!empty($allianz->photos_21))
                                            <div class="col-md-2">
                                                <div class="img-article">
                                                    <a href="{{ $allianz->photos_21 }}" data-toggle="lightbox" data-gallery="example-gallery">
                                                        <img src="{{ $allianz->photos_21 }}" alt="{{ $allianz->marka }}" class="img-responsive img-center">
                                                    </a>
                                                </div>
                                            </div>
                                        @endif
                                        @if (!empty($allianz->photos_22))
                                            <div class="col-md-2">
                                                <div class="img-article">
                                                    <a href="{{ $allianz->photos_22 }}" data-toggle="lightbox" data-gallery="example-gallery">
                                                        <img src="{{ $allianz->photos_22 }}" alt="{{ $allianz->marka }}" class="img-responsive img-center">
                                                    </a>
                                                </div>
                                            </div>
                                        @endif
                                        @if (!empty($allianz->photos_23))
                                            <div class="col-md-2">
                                                <div class="img-article">
                                                    <a href="{{ $allianz->photos_23 }}" data-toggle="lightbox" data-gallery="example-gallery">
                                                        <img src="{{ $allianz->photos_23 }}" alt="{{ $allianz->marka }}" class="img-responsive img-center">
                                                    </a>
                                                </div>
                                            </div>
                                        @endif
                                        @if (!empty($allianz->photos_24))
                                            <div class="col-md-2">
                                                <div class="img-article">
                                                    <a href="{{ $allianz->photos_24 }}" data-toggle="lightbox" data-gallery="example-gallery">
                                                        <img src="{{ $allianz->photos_24 }}" alt="{{ $allianz->marka }}" class="img-responsive img-center">
                                                    </a>
                                                </div>
                                            </div>
                                        @endif
                                    </div>

                                    <div class="empty-space"></div>
                                    
                                    <div class="row">
                                        @if (!empty($allianz->photos_25))
                                            <div class="col-md-2">
                                                <div class="img-article">
                                                    <a href="{{ $allianz->photos_25 }}" data-toggle="lightbox" data-gallery="example-gallery">
                                                        <img src="{{ $allianz->photos_25 }}" alt="{{ $allianz->marka }}" class="img-responsive img-center">
                                                    </a>
                                                </div>
                                            </div>
                                        @endif
                                        @if (!empty($allianz->photos_26))
                                            <div class="col-md-2">
                                                <div class="img-article">
                                                    <a href="{{ $allianz->photos_26 }}" data-toggle="lightbox" data-gallery="example-gallery">
                                                        <img src="{{ $allianz->photos_26 }}" alt="{{ $allianz->marka }}" class="img-responsive img-center">
                                                    </a>
                                                </div>
                                            </div>
                                        @endif
                                        @if (!empty($allianz->photos_27))
                                            <div class="col-md-2">
                                                <div class="img-article">
                                                    <a href="{{ $allianz->photos_27 }}" data-toggle="lightbox" data-gallery="example-gallery">
                                                        <img src="{{ $allianz->photos_27 }}" alt="{{ $allianz->marka }}" class="img-responsive img-center">
                                                    </a>
                                                </div>
                                            </div>
                                        @endif
                                        @if (!empty($allianz->photos_28))
                                            <div class="col-md-2">
                                                <div class="img-article">
                                                    <a href="{{ $allianz->photos_28 }}" data-toggle="lightbox" data-gallery="example-gallery">
                                                        <img src="{{ $allianz->photos_28 }}" alt="{{ $allianz->marka }}" class="img-responsive img-center">
                                                    </a>
                                                </div>
                                            </div>
                                        @endif
                                        @if (!empty($allianz->photos_29))
                                            <div class="col-md-2">
                                                <div class="img-article">
                                                    <a href="{{ $allianz->photos_29 }}" data-toggle="lightbox" data-gallery="example-gallery">
                                                        <img src="{{ $allianz->photos_29 }}" alt="{{ $allianz->marka }}" class="img-responsive img-center">
                                                    </a>
                                                </div>
                                            </div>
                                        @endif
                                        @if (!empty($allianz->photos_30))
                                            <div class="col-md-2">
                                                <div class="img-article">
                                                    <a href="{{ $allianz->photos_30 }}" data-toggle="lightbox" data-gallery="example-gallery">
                                                        <img src="{{ $allianz->photos_30 }}" alt="{{ $allianz->marka }}" class="img-responsive img-center">
                                                    </a>
                                                </div>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                                
                                <div class="col-md-12">
                                    <h6><strong>Pierwsza rejestracja:</strong> {{ $allianz->pierwsza_rejestracja }}</h6>
                                    <h6><strong>Vin:</strong> {{ $allianz->vin }}</h6>
                                    <h6><strong>Nowy:</strong> {{ $allianz->nowy }}</h6>
                                    <h6><strong>Kolor:</strong> {{ $allianz->kolor }}</h6>
                                    <h6><strong>Przebieg:</strong> {{ $allianz->przebieg}} <strong>tyś. km</strong></h6>
                                    <h6><strong>Dowód własności:</strong> {{ $allianz->dowod_wlasnosci }}</h6>
                                    <h6><strong>MFK:</strong> {{ $allianz->mfk }}</h6>
                                    <h6><strong>Stan:</strong> {{ $allianz->stan }}</h6>
                                    <h6><strong>Wyposazenie:</strong> {{ $allianz->wyposazenie }}</h6>
                                    <h6><strong>Opis uszkodzenia:</strong> {{ $allianz->opis_uszkodzenia }}</h6>
                                    <h6><strong>Typ uszkodzenia:</strong> {{ $allianz->typ_uszkodzenia }}</h6>
                                    <h6><strong>Wcześniejsze uszkodzenia:</strong> {{ $allianz->wczesniejsze_uszkodzenia }}</h6>
                                    <h6><strong>Lokalizacja uszkodzenia:</strong> {{ $allianz->lokalizacja_uszkodzenia }}</h6>
                                    <h6><strong>Koniec aukcji:</strong> {{ $allianz->end_date_of_auction }}</h6>
                                    
                                    {{-- <h6><strong>Pocztek aukcji:</strong> {{ $allianz->poczatek }}</h6> --}}
                                    <p></p>
                                </div>
                            </div>

                        </div>
                    </div>

                    @if (Auth::check() && is_user())
                        <div class="card" style="{!! ($allianz->end_date_of_auction < $mytime) ? 'display:none' : '' !!}">
                            <div class="card-content">
                                <div class="empty-space"></div>
                                <h4>Podaj swoją cenę we <strong>frankach szwajcarskich (Chf)</strong></h4>
                                <p style="font-size: 11px;"><strong>Oferta musi być wyższa niż poprzednia oferta. W przeciwnym wypadku nie zostanie umieszczona.</strong></p>

                                <div class="row">
                                    <div class="col-md-4">
                                        <form role="form" method="POST" action="{{ url('/opis') }}">
                                            {{ csrf_field() }}

                                            <input type="hidden" name="operator" value="{{ $allianz->operator }} ">
                                            <input type="hidden" name="date" value="{{ $allianz->end_date_of_auction }} ">
                                            <input type="hidden" name="name" value="{{ $allianz->marka }} {{ $allianz->typ }}{{ $allianz->model }}">
                                            <input type="hidden" name="auct_id" value="{{ $allianz->id }}">
                                            <input type="hidden" name="user_id" value="{{ Auth::user()->id }}">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group label-floating {{ $errors->has('price') ? ' has-error' : '' }}">
                                                        <label class="control-label">Twoja cena</label>
                                                        <input type="number" class="form-control" name="price" value="{{ old('price') }}">

                                                        @if ($errors->has('price'))
                                                            <span class="help-block">
                                                                <strong>{{ $errors->first('price') }}</strong>
                                                            </span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            <button type="submit" class="btn btn-orange">Licytuję</button>
                                            <div class="clearfix"></div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @else
                        <div class="card">
                            <div class="card-content">
                                <div class="empty-space"></div>
                                <h4>Musisz być zalogowanym aby licytować!</h4>
                            </div>
                        </div>
                    @endif

                </div>
            </div>
        </div>
    </div>

@endsection