@extends('page.left')

@section('content')

            <img src="images/header.jpg" class="header img-responsive">
    </div>

    <div class="content">
        <div class="container-fluid">
            <div class="row">

                {{-- @php
                    require_once 'classes/swissCars.php';

                    $swissCarsInit = new SwissCars;
            
                    $swissCarsInit -> checkLogin();
                    $arrayWithCars = $swissCarsInit -> getCars('zabezpieczone-tresci');

                    foreach ($arrayWithCars as $cars) {
                        $photos_0 = $cars["links_to_photos"][0]; 

                        if (!empty($cars["links_to_photos"][1] )) {
                            $photos_1 = $cars["links_to_photos"][1]; 
                        }
                        if (!empty($cars["links_to_photos"][2] )) {
                            $photos_2 = $cars["links_to_photos"][2]; 
                        }
                        if (!empty($cars["links_to_photos"][3] )) {
                            $photos_3 = $cars["links_to_photos"][3]; 
                        }
                        if (!empty($cars["links_to_photos"][4] )) {
                            $photos_4 = $cars["links_to_photos"][4]; 
                        }
                        if (!empty($cars["links_to_photos"][5] )) {
                            $photos_5 = $cars["links_to_photos"][5]; 
                        }
                        $marka = $cars["description"]["Marka"]; 
                        $typ = $cars["description"]["Typ"]; 
                        $model = $cars["description"]["Model"]; 
                        $nowy = $cars["description"]["Nowy"]; 
                        $kolor = $cars["description"]["Kolor"]; 
                        $vin = $cars["description"]["VIN"]; 
                        $pierwsza_rejestracja = $cars["description"]["Pierwsza rejestracja"]; 
                        if (!empty($cars["description"]["Przebieg"])) {
                            $przebieg = $cars["description"]["Przebieg"]; 
                        }
                        $dowod_wlasnosci = $cars["description"]["Dowod wlasnosci"]; 
                        $stan = $cars["description"]["Stan"]; 
                        $wyposazenie = $cars["description"]["Wyposazenie"]; 
                        $opis_uszkodzenia = $cars["description"]["Opis uszkodzenia"]; 
                        $typ_uszkodzenia = $cars["description"]["Typ uszkodzenia"]; 
                        $poczatek = $cars["description"]["Poczatek"]; 
                        $koniec = $cars["description"]["Koniec"]; 
                        $end_date_of_auction = $cars["end_date_of_auction"]; 

                        $endDateOfAuction = new DateTime($cars["end_date_of_auction"] );   
                        $now = new DateTime(date('Y-m-d H:i', time() + 4200));
                        $timeLeft = $endDateOfAuction->diff($now)->format("%H h i %i min");

                        DB::table('car_allianzs')->insert(array('kto'=> 'Allianz', 'photos_0' => $photos_0, 'photos_1' => $photos_1, 'photos_2' => $photos_2, 'photos_3' => $photos_3, 'photos_4' => $photos_4, 'photos_5' => $photos_5, 'marka' => $marka, 'typ' => $typ, 'model' => $model, 'nowy' => $nowy, 'kolor' => $kolor, 'vin' => $vin, 'pierwsza_rejestracja' => $pierwsza_rejestracja, 'przebieg' => $przebieg, 'mfk' => '', 'dowod_wlasnosci' => $dowod_wlasnosci, 'stan' => $stan, 'wyposazenie' => $wyposazenie, 'opis_uszkodzenia' => $opis_uszkodzenia, 'typ_uszkodzenia' => $typ_uszkodzenia, 'poczatek' => $poczatek, 'koniec' => $koniec, 'end_date_of_auction' => $end_date_of_auction));
                    }

                @endphp --}}

                {{-- @php
                    require_once 'classes/swissCars.php';

                    $swissCarsInit = new SwissCars;
            
                    $swissCarsInit -> checkLogin();
                    $arrayWithCars = $swissCarsInit -> getCars('scauto');
                    

                    foreach ($arrayWithCars as $cars) {
                        $photos_0 = $cars["links_to_photos"][0]; 

                        if (!empty($cars["links_to_photos"][1] )) {
                            $photos_1 = $cars["links_to_photos"][1]; 
                        }
                        if (!empty($cars["links_to_photos"][2] )) {
                            $photos_2 = $cars["links_to_photos"][2]; 
                        }
                        if (!empty($cars["links_to_photos"][3] )) {
                            $photos_3 = $cars["links_to_photos"][3]; 
                        }
                        if (!empty($cars["links_to_photos"][4] )) {
                            $photos_4 = $cars["links_to_photos"][4]; 
                        }
                        if (!empty($cars["links_to_photos"][5] )) {
                            $photos_5 = $cars["links_to_photos"][5]; 
                        }
                        $marka = $cars["description"]["Marka"]; 
                        $typ = $cars["description"]["Typ"]; 
                        $model = $cars["description"]["Model"]; 
                        $waga = $cars["description"]["Waga"]; 
                        $typ_pojazdu = $cars["description"]["Typ pojazdu"]; 
                        $typ_silnika = $cars["description"]["Typ silnika"]; 
                        $skrzynia_biegow = $cars["description"]["Skrzynia biegów"]; 
                        $naped = $cars["description"]["Napęd"]; 
                        $moc = $cars["description"]["Moc"]; 
                        $pojemnosc_silnika = $cars["description"]["Pojemność silnika"]; 
                        $przebieg = $cars["description"]["Przebieg"]; 
                        $vin = $cars["description"]["VIN"]; 
                        $pierwsza_rejestracja = $cars["description"]["Pierwsza rejestracja"]; 
                        $koniec = $cars["description"]["Koniec"]; 
                        $end_date_of_auction = $cars["end_date_of_auction"]; 

                        $endDateOfAuction = new DateTime($cars["end_date_of_auction"] );   
                        $now = new DateTime(date('Y-m-d H:i', time() + 4200));
                        $timeLeft = $endDateOfAuction->diff($now)->format("%H h i %i min");

                        DB::table('car_scautos')->insert(array('kto'=> 'Scauto', 'photos_0' => $photos_0, 'photos_1' => $photos_1, 'photos_2' => $photos_2, 'photos_3' => $photos_3, 'photos_4' => $photos_4, 'photos_5' => $photos_5, 'marka' => $marka, 'typ' => $typ, 'model' => $model, 'typ_pojazdu' => $typ_pojazdu, 'typ_silnika' => $typ_silnika, 'vin' => $vin, 'pierwsza_rejestracja' => $pierwsza_rejestracja, 'przebieg' => $przebieg, 'naped' => $naped, 'moc' => $moc, 'pojemnosc_silnika' => $pojemnosc_silnika, 'waga' => $waga, 'koniec' => $koniec, 'end_date_of_auction' => $end_date_of_auction));
                    }

                @endphp --}}

                {{-- @php
                    require_once 'classes/swissCars.php';

                    $swissCarsInit = new SwissCars;
            
                    $swissCarsInit -> checkLogin();
                    $arrayWithCars = $swissCarsInit -> getCars('rest');
                

                    foreach ($arrayWithCars as $cars) {
                        if (!empty($cars["links_to_photos"][0] )) {
                            $photos_0 = $cars["links_to_photos"][0]; 
                        }

                        if (!empty($cars["links_to_photos"][1] )) {
                            $photos_1 = $cars["links_to_photos"][1]; 
                        }
                        if (!empty($cars["links_to_photos"][2] )) {
                            $photos_2 = $cars["links_to_photos"][2]; 
                        }
                        if (!empty($cars["links_to_photos"][3] )) {
                            $photos_3 = $cars["links_to_photos"][3]; 
                        }
                        if (!empty($cars["links_to_photos"][4] )) {
                            $photos_4 = $cars["links_to_photos"][4]; 
                        }
                        if (!empty($cars["links_to_photos"][5] )) {
                            $photos_5 = $cars["links_to_photos"][5]; 
                        }
                        $marka = $cars["description"]["Marka"]; 
                        $podwozie = $cars["description"]["Podwozie"]; 
                        $model = $cars["description"]["Model"]; 
                        $opony = $cars["description"]["Opony"]; 
                        $kolor = $cars["description"]["Kolor"]; 
                        $konstrukcja = $cars["description"]["Konstrukcja"]; 
                        $pierwsza_rejestracja = $cars["description"]["Pierwsza rejestracja"]; 
                        $przebieg = $cars["description"]["Przebieg"]; 
                        $mfk = $cars["description"]["MFK"]; 
                        $dowod_wlasnosci = $cars["description"]["Dowod wlasnosci"]; 
                        $wyposazenie = $cars["description"]["Wyposazenie"]; 
                        $opis_uszkodzenia = $cars["description"]["Opis uszkodzen"]; 
                        $wystrzelone_poduszki = $cars["description"]["Wystrzelone poduszki"]; 
                        $koniec = $cars["description"]["Koniec"]; 
                        $end_date_of_auction = $cars["end_date_of_auction"]; 

                        $endDateOfAuction = new DateTime($cars["end_date_of_auction"] );   
                        $now = new DateTime(date('Y-m-d H:i', time() + 4200));
                        $timeLeft = $endDateOfAuction->diff($now)->format("%H h i %i min");

                        $car = DB::table('car_rests')->where('photos_0', '=', $photos_0)
                                                    ->orWhere('photos_1', '=', $photos_1)->first();


                        var_dump($car);

                        // if(!empty($car)) {
                        //     DB::table('car_rests')->update(array('kto'=> 'Rest', 'photos_0' => $photos_0, 'photos_1' => $photos_1, 'photos_2' => $photos_2, 'photos_3' => $photos_3, 'photos_4' => $photos_4, 'photos_5' => $photos_5, 'marka' => $marka, 'podwozie' => $podwozie, 'model' => $model, 'opony' => $opony, 'kolor' => $kolor, 'konstrukcja' => $konstrukcja, 'pierwsza_rejestracja' => $pierwsza_rejestracja, 'przebieg' => $przebieg, 'mfk' => $mfk, 'dowod_wlasnosci' => $dowod_wlasnosci, 'wyposazenie' => $wyposazenie, 'opis_uszkodzenia' => $opis_uszkodzenia, 'wystrzelone_poduszki' => $wystrzelone_poduszki, 'koniec' => $koniec, 'end_date_of_auction' => $end_date_of_auction));
                        // } else {
                        //     DB::table('car_rests')->insert(array('kto'=> 'Rest', 'photos_0' => $photos_0, 'photos_1' => $photos_1, 'photos_2' => $photos_2, 'photos_3' => $photos_3, 'photos_4' => $photos_4, 'photos_5' => $photos_5, 'marka' => $marka, 'podwozie' => $podwozie, 'model' => $model, 'opony' => $opony, 'kolor' => $kolor, 'konstrukcja' => $konstrukcja, 'pierwsza_rejestracja' => $pierwsza_rejestracja, 'przebieg' => $przebieg, 'mfk' => $mfk, 'dowod_wlasnosci' => $dowod_wlasnosci, 'wyposazenie' => $wyposazenie, 'opis_uszkodzenia' => $opis_uszkodzenia, 'wystrzelone_poduszki' => $wystrzelone_poduszki, 'koniec' => $koniec, 'end_date_of_auction' => $end_date_of_auction));
                        // }


                    }

                @endphp --}}

                {{-- <form method="GET" action="{{ url('/search') }}" class="navbar-form navbar-left">

                                <input type="text" name="q">
                    
                                <button type="submit" class="btn btn-default">Szukaj</button>
                            </form> --}}

                @php 
                    $mytime = Carbon\Carbon::now(new DateTimeZone('Europe/Berlin'));
                @endphp

                @foreach ($homes as $home)
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="card card-stats">
                            <div class="card-header-main">
                                <a href="{{ url('/home/' . $home->id) }}">
                                    <img src="{{ $home->photos_0 }}" alt="{{ $home->car_name }}">
                                </a>
                            </div>
                            <div class="card-content-car">
                                <div class="box-title-main">
                                    <h3 class="title">{{ $home->car_name }}</h3>
                                </div>    
                                <div class="time-left">Data zakończenia aukcji: <br> <span data-countdown="{{ $home->end_date_of_auction }}"></span></div>
                                <div class="date-left">{{ $home->end_date_of_auction }}</div>
                            </div>
                            <div class="card-footer">
                                <a class="stats" href="{{ url('/home/' . $home->id) }}">Licytuj</a>
                            </div>
                        </div>
                    </div>
                @endforeach

            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="text-center">{{ $homes }}</div>
                </div>
            </div>
        </div>
    </div>

@endsection