@extends('page.left')

@section('content')

    @php 
        $mytime = Carbon\Carbon::now(new DateTimeZone('Europe/Berlin'));
    @endphp

    @if (Session::has('add_auction'))
        <div class="alert-message" role="alert">
            <div class="row">
                <div class="col-md-12">
                    <div class="message">
                        {{Session::get('add_auction')}}
                    </div>
                </div>
            </div>
        </div>
    @endif

    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12 show">
                    <div class="card">
                        <div class="card-header" data-background-color="orange">
                            <h4 class="title">{{ $scauto->marka }} {{ $scauto->typ }}{{ $scauto->model }}</h4>
                            <p class="category">Czas do końca aukcji: <span>{!! ($scauto->end_date_of_auction < $mytime) ? 'aukcja zakończona' : $scauto->end_date_of_auction !!}</span></p>
                        </div>
                        <div class="card-content">
                            <br>
                            <div class="row">
                                <div class="col-md-6 col-md-offset-3">
                                    <img src="{{ $scauto->photos_0 }}" alt="{{ $scauto->marka }}" class="img-responsive img-center">
                                    <br>
                                </div>
                                <div class="col-md-12">
                                    <div class="row">
                                        @if (!empty($scauto->photos_1))
                                            <div class="col-md-2">
                                                <div class="img-article">
                                                    <a href="{{ $scauto->photos_1 }}" data-toggle="lightbox" data-gallery="example-gallery">
                                                        <img src="{{ $scauto->photos_1 }}" alt="{{ $scauto->marka }}" class="img-responsive img-center">
                                                    </a>
                                                </div>
                                            </div>
                                        @endif
                                        @if (!empty($scauto->photos_2))
                                            <div class="col-md-2">
                                                <div class="img-article">
                                                    <a href="{{ $scauto->photos_2 }}" data-toggle="lightbox" data-gallery="example-gallery">
                                                        <img src="{{ $scauto->photos_2 }}" alt="{{ $scauto->marka }}" class="img-responsive img-center">
                                                    </a>
                                                </div>
                                            </div>
                                         @endif
                                        @if (!empty($scauto->photos_3))
                                            <div class="col-md-2">
                                                <div class="img-article">
                                                    <a href="{{ $scauto->photos_3 }}" data-toggle="lightbox" data-gallery="example-gallery">
                                                        <img src="{{ $scauto->photos_3 }}" alt="{{ $scauto->marka }}" class="img-responsive img-center">
                                                    </a>
                                                </div>
                                            </div>
                                         @endif
                                        @if (!empty($scauto->photos_4))
                                            <div class="col-md-2">
                                                <div class="img-article">
                                                    <a href="{{ $scauto->photos_4 }}" data-toggle="lightbox" data-gallery="example-gallery">
                                                        <img src="{{ $scauto->photos_4 }}" alt="{{ $scauto->marka }}" class="img-responsive img-center">
                                                    </a>
                                                </div>
                                            </div>
                                         @endif
                                        @if (!empty($scauto->photos_5))
                                            <div class="col-md-2">
                                                <div class="img-article">
                                                    <a href="{{ $scauto->photos_5 }}" data-toggle="lightbox" data-gallery="example-gallery">
                                                        <img src="{{ $scauto->photos_5 }}" alt="{{ $scauto->marka }}" class="img-responsive img-center">
                                                    </a>
                                                </div>
                                            </div>
                                         @endif
                                        @if (!empty($scauto->photos_6))
                                            <div class="col-md-2">
                                                <div class="img-article">
                                                    <a href="{{ $scauto->photos_6 }}" data-toggle="lightbox" data-gallery="example-gallery">
                                                        <img src="{{ $scauto->photos_6 }}" alt="{{ $scauto->marka }}" class="img-responsive img-center">
                                                    </a>
                                                </div>
                                            </div>
                                         @endif
                                    </div>
                
                                    <div class="empty-space"></div>

                                    <div class="row">
                                        @if (!empty($scauto->photos_7))
                                            <div class="col-md-2">
                                                <div class="img-article">
                                                    <a href="{{ $scauto->photos_7 }}" data-toggle="lightbox" data-gallery="example-gallery">
                                                        <img src="{{ $scauto->photos_7 }}" alt="{{ $scauto->marka }}" class="img-responsive img-center">
                                                    </a>
                                                </div>
                                            </div>
                                        @endif
                                        @if (!empty($scauto->photos_8))
                                            <div class="col-md-2">
                                                <div class="img-article">
                                                    <a href="{{ $scauto->photos_8 }}" data-toggle="lightbox" data-gallery="example-gallery">
                                                        <img src="{{ $scauto->photos_8 }}" alt="{{ $scauto->marka }}" class="img-responsive img-center">
                                                    </a>
                                                </div>
                                            </div>
                                        @endif
                                        @if (!empty($scauto->photos_9))
                                            <div class="col-md-2">
                                                <div class="img-article">
                                                    <a href="{{ $scauto->photos_9 }}" data-toggle="lightbox" data-gallery="example-gallery">
                                                        <img src="{{ $scauto->photos_9 }}" alt="{{ $scauto->marka }}" class="img-responsive img-center">
                                                    </a>
                                                </div>
                                            </div>
                                        @endif
                                        @if (!empty($scauto->photos_10))
                                            <div class="col-md-2">
                                                <div class="img-article">
                                                    <a href="{{ $scauto->photos_10 }}" data-toggle="lightbox" data-gallery="example-gallery">
                                                        <img src="{{ $scauto->photos_10 }}" alt="{{ $scauto->marka }}" class="img-responsive img-center">
                                                    </a>
                                                </div>
                                            </div>
                                        @endif
                                        @if (!empty($scauto->photos_11))
                                            <div class="col-md-2">
                                                <div class="img-article">
                                                    <a href="{{ $scauto->photos_11 }}" data-toggle="lightbox" data-gallery="example-gallery">
                                                        <img src="{{ $scauto->photos_11 }}" alt="{{ $scauto->marka }}" class="img-responsive img-center">
                                                    </a>
                                                </div>
                                            </div>
                                        @endif
                                        @if (!empty($scauto->photos_12))
                                            <div class="col-md-2">
                                                <div class="img-article">
                                                    <a href="{{ $scauto->photos_12 }}" data-toggle="lightbox" data-gallery="example-gallery">
                                                        <img src="{{ $scauto->photos_12 }}" alt="{{ $scauto->marka }}" class="img-responsive img-center">
                                                    </a>
                                                </div>
                                            </div>
                                        @endif
                                    </div>

                                    <div class="empty-space"></div>
                                    
                                    <div class="row">
                                        @if (!empty($scauto->photos_13))
                                            <div class="col-md-2">
                                                <div class="img-article">
                                                    <a href="{{ $scauto->photos_13 }}" data-toggle="lightbox" data-gallery="example-gallery">
                                                        <img src="{{ $scauto->photos_13 }}" alt="{{ $scauto->marka }}" class="img-responsive img-center">
                                                    </a>
                                                </div>
                                            </div>
                                        @endif
                                        @if (!empty($scauto->photos_14))
                                            <div class="col-md-2">
                                                <div class="img-article">
                                                    <a href="{{ $scauto->photos_14 }}" data-toggle="lightbox" data-gallery="example-gallery">
                                                        <img src="{{ $scauto->photos_14 }}" alt="{{ $scauto->marka }}" class="img-responsive img-center">
                                                    </a>
                                                </div>
                                            </div>
                                        @endif
                                        @if (!empty($scauto->photos_15))
                                            <div class="col-md-2">
                                                <div class="img-article">
                                                    <a href="{{ $scauto->photos_15 }}" data-toggle="lightbox" data-gallery="example-gallery">
                                                        <img src="{{ $scauto->photos_15 }}" alt="{{ $scauto->marka }}" class="img-responsive img-center">
                                                    </a>
                                                </div>
                                            </div>
                                        @endif
                                        @if (!empty($scauto->photos_16))
                                            <div class="col-md-2">
                                                <div class="img-article">
                                                    <a href="{{ $scauto->photos_16 }}" data-toggle="lightbox" data-gallery="example-gallery">
                                                        <img src="{{ $scauto->photos_16 }}" alt="{{ $scauto->marka }}" class="img-responsive img-center">
                                                    </a>
                                                </div>
                                            </div>
                                        @endif
                                        @if (!empty($scauto->photos_17))
                                            <div class="col-md-2">
                                                <div class="img-article">
                                                    <a href="{{ $scauto->photos_17 }}" data-toggle="lightbox" data-gallery="example-gallery">
                                                        <img src="{{ $scauto->photos_17 }}" alt="{{ $scauto->marka }}" class="img-responsive img-center">
                                                    </a>
                                                </div>
                                            </div>
                                        @endif
                                        @if (!empty($scauto->photos_18))
                                            <div class="col-md-2">
                                                <div class="img-article">
                                                    <a href="{{ $scauto->photos_18 }}" data-toggle="lightbox" data-gallery="example-gallery">
                                                        <img src="{{ $scauto->photos_18 }}" alt="{{ $scauto->marka }}" class="img-responsive img-center">
                                                    </a>
                                                </div>
                                            </div>
                                        @endif
                                    </div>

                                    <div class="empty-space"></div>
                                    
                                    <div class="row">
                                        @if (!empty($scauto->photos_19))
                                            <div class="col-md-2">
                                                <div class="img-article">
                                                    <a href="{{ $scauto->photos_19 }}" data-toggle="lightbox" data-gallery="example-gallery">
                                                        <img src="{{ $scauto->photos_19 }}" alt="{{ $scauto->marka }}" class="img-responsive img-center">
                                                    </a>
                                                </div>
                                            </div>
                                        @endif
                                        @if (!empty($scauto->photos_20))
                                            <div class="col-md-2">
                                                <div class="img-article">
                                                    <a href="{{ $scauto->photos_20 }}" data-toggle="lightbox" data-gallery="example-gallery">
                                                        <img src="{{ $scauto->photos_20 }}" alt="{{ $scauto->marka }}" class="img-responsive img-center">
                                                    </a>
                                                </div>
                                            </div>
                                        @endif
                                        @if (!empty($scauto->photos_21))
                                            <div class="col-md-2">
                                                <div class="img-article">
                                                    <a href="{{ $scauto->photos_21 }}" data-toggle="lightbox" data-gallery="example-gallery">
                                                        <img src="{{ $scauto->photos_21 }}" alt="{{ $scauto->marka }}" class="img-responsive img-center">
                                                    </a>
                                                </div>
                                            </div>
                                        @endif
                                        @if (!empty($scauto->photos_22))
                                            <div class="col-md-2">
                                                <div class="img-article">
                                                    <a href="{{ $scauto->photos_22 }}" data-toggle="lightbox" data-gallery="example-gallery">
                                                        <img src="{{ $scauto->photos_22 }}" alt="{{ $scauto->marka }}" class="img-responsive img-center">
                                                    </a>
                                                </div>
                                            </div>
                                        @endif
                                        @if (!empty($scauto->photos_23))
                                            <div class="col-md-2">
                                                <div class="img-article">
                                                    <a href="{{ $scauto->photos_23 }}" data-toggle="lightbox" data-gallery="example-gallery">
                                                        <img src="{{ $scauto->photos_23 }}" alt="{{ $scauto->marka }}" class="img-responsive img-center">
                                                    </a>
                                                </div>
                                            </div>
                                        @endif
                                        @if (!empty($scauto->photos_24))
                                            <div class="col-md-2">
                                                <div class="img-article">
                                                    <a href="{{ $scauto->photos_24 }}" data-toggle="lightbox" data-gallery="example-gallery">
                                                        <img src="{{ $scauto->photos_24 }}" alt="{{ $scauto->marka }}" class="img-responsive img-center">
                                                    </a>
                                                </div>
                                            </div>
                                        @endif
                                    </div>

                                    <div class="empty-space"></div>
                                    
                                    <div class="row">
                                        @if (!empty($scauto->photos_25))
                                            <div class="col-md-2">
                                                <div class="img-article">
                                                    <a href="{{ $scauto->photos_25 }}" data-toggle="lightbox" data-gallery="example-gallery">
                                                        <img src="{{ $scauto->photos_25 }}" alt="{{ $scauto->marka }}" class="img-responsive img-center">
                                                    </a>
                                                </div>
                                            </div>
                                        @endif
                                        @if (!empty($scauto->photos_26))
                                            <div class="col-md-2">
                                                <div class="img-article">
                                                    <a href="{{ $scauto->photos_26 }}" data-toggle="lightbox" data-gallery="example-gallery">
                                                        <img src="{{ $scauto->photos_26 }}" alt="{{ $scauto->marka }}" class="img-responsive img-center">
                                                    </a>
                                                </div>
                                            </div>
                                        @endif
                                        @if (!empty($scauto->photos_27))
                                            <div class="col-md-2">
                                                <div class="img-article">
                                                    <a href="{{ $scauto->photos_27 }}" data-toggle="lightbox" data-gallery="example-gallery">
                                                        <img src="{{ $scauto->photos_27 }}" alt="{{ $scauto->marka }}" class="img-responsive img-center">
                                                    </a>
                                                </div>
                                            </div>
                                        @endif
                                        @if (!empty($scauto->photos_28))
                                            <div class="col-md-2">
                                                <div class="img-article">
                                                    <a href="{{ $scauto->photos_28 }}" data-toggle="lightbox" data-gallery="example-gallery">
                                                        <img src="{{ $scauto->photos_28 }}" alt="{{ $scauto->marka }}" class="img-responsive img-center">
                                                    </a>
                                                </div>
                                            </div>
                                        @endif
                                        @if (!empty($scauto->photos_29))
                                            <div class="col-md-2">
                                                <div class="img-article">
                                                    <a href="{{ $scauto->photos_29 }}" data-toggle="lightbox" data-gallery="example-gallery">
                                                        <img src="{{ $scauto->photos_29 }}" alt="{{ $scauto->marka }}" class="img-responsive img-center">
                                                    </a>
                                                </div>
                                            </div>
                                        @endif
                                        @if (!empty($scauto->photos_30))
                                            <div class="col-md-2">
                                                <div class="img-article">
                                                    <a href="{{ $scauto->photos_30 }}" data-toggle="lightbox" data-gallery="example-gallery">
                                                        <img src="{{ $scauto->photos_30 }}" alt="{{ $scauto->marka }}" class="img-responsive img-center">
                                                    </a>
                                                </div>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <h6><strong>Vin:</strong> {{ $scauto->vin }}</h6>
                                    <h6><strong>Pierwsza rejestracja:</strong> {{ $scauto->pierwsza_rejestracja }}</h6>
                                    <h6><strong>Przebieg:</strong> {{ $scauto->przebieg}} <strong>tyś. km</strong></h6>
                                    <h6><strong>Typ pojazdu:</strong> {{ $scauto->typ_pojazdu}}</h6>
                                    <h6><strong>Typ silnika:</strong> {{ $scauto->typ_silnika}}</h6>
                                    @if (!$scauto->naped == NULL)
                                    <h6><strong>Napęd:</strong> {{ $scauto->naped}}</h6>
                                    @endif
                                    <h6><strong>Moc:</strong> {{ $scauto->moc}}</h6>
                                    <h6><strong>Pojemnosc silnika:</strong> {{ $scauto->pojemnosc_silnika}}</h6>
                                    <h6><strong>Waga:</strong> {{ $scauto->waga}}</h6>
                                    <h6><strong>Koniec aukcji:</strong> {{ $scauto->end_date_of_auction }}</h6>
                                    <p></p>
                                    {!! $scauto->description2 !!}
                                </div>
                            </div>

                        </div>
                    </div>

                    @if (Auth::check() && is_user())
                        <div class="card" style="{!! ($scauto->end_date_of_auction < $mytime) ? 'display:none' : '' !!}">
                            
                            <div class="card-content">
                                
                                <div class="empty-space"></div>
                                <h4>Podaj swoją cenę we <strong>frankach szwajcarskich (Chf)</strong></h4>
                                <p style="font-size: 11px;"><strong>Oferta musi być wyższa niż poprzednia oferta. W przeciwnym wypadku nie zostanie umieszczona.</strong></p>

                                <div class="row">
                                    <div class="col-md-4">
                                        <form role="form" method="POST" action="{{ url('/opis') }}">
                                            {{ csrf_field() }}

                                            <input type="hidden" name="operator" value="{{ $scauto->operator }} ">
                                            <input type="hidden" name="date" value="{{ $scauto->end_date_of_auction }} ">
                                            <input type="hidden" name="name" value="{{ $scauto->marka }} {{ $scauto->typ }}{{ $scauto->model }}">
                                            <input type="hidden" name="auct_id" value="{{ $scauto->id }}">
                                            <input type="hidden" name="user_id" value="{{ Auth::user()->id }}">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group label-floating {{ $errors->has('price') ? ' has-error' : '' }}">
                                                        <label class="control-label">Twoja cena</label>
                                                        <input type="number" class="form-control" name="price" value="{{ old('price') }}">

                                                        @if ($errors->has('price'))
                                                            <span class="help-block">
                                                                <strong>{{ $errors->first('price') }}</strong>
                                                            </span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            <button type="submit" class="btn btn-orange">Licytuję</button>
                                            <div class="clearfix"></div>
                                        </form>
                                    </div>
                                </div>

                            </div>
                        </div>
                    @else
                        <div class="card">
                            <div class="card-content">
                                <div class="empty-space"></div>
                                <h4>Musisz być zalogowanym aby licytować!</h4>
                            </div>
                        </div>
                    @endif

                </div>
            </div>
        </div>
    </div>

@endsection