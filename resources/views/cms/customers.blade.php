@extends('cms.left')

@section('content')

	<div class="content">
	    <div class="container-fluid">
	        <div class="row">
	            <div class="col-md-12 show">
	                <div class="card">
	                    <div class="card-header" data-background-color="orange">
	                        <h4 class="title">Licytacje użytkowników</h4>
	                        <p class ="category">Wszystkie aukcje</p>
	                    </div>
	                    <div class="card-content">
	                    <br>

	                    	@if ( $users->isEmpty())
                                <div class="alert alert-danger" role="alert">
                                    Brak licytacji
                                </div>
                            @else

		                        <table class="table table-hover">
		                            <thead class="text-warning">
		                                <th>ID</th>
		                                <th>Użytkownik</th>
		                                <th>Ilość licytacji</th>
		                                <th>Akcja</th>
		                            </thead>
		                            <tbody>

		                            	@foreach ( $users as $user)
		                            		@if ($user->role_id == 2)
		                            			@php
		                            				$count = $user->auctions->count();
		                            			@endphp
		                            			@if ($count > 0)
					                                <tr>
					                                    <td>{{ $user->id }}</td>
					                                    <td>{{ $user->name  }}</td>
					                                    <td>{{ $user->auctions->count() }}</td>
					                                    <td class="td-actions text-right">
					                                        <a href="{{ url('/cms/customers/' . $user->id) }}" rel="tooltip" title="Zobacz licytacje tego użytkownika" class="btn btn-primary btn-simple btn-xs">
					                                            <i class="material-icons">link</i>
					                                        </a>
					                                    </td>
					                                </tr>
				                                @endif
			                                @endif
		                                @endforeach
		                                
		                            </tbody>
		                        </table>

	                        @endif

	                    </div>
	                </div>
	            </div>
	        </div>
	    </div>
	</div>
	
@endsection