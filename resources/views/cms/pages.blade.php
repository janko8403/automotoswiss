@extends('cms.left')

@section('content')

    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header" data-background-color="orange">
                            <h4 class="title">Strony</h4>
                            <p class ="category">Dodane strony</p>
                        </div>
                        <div class="card-content">
                        <br>

                            @if ($pages->isEmpty())
                                <div class="alert alert-danger" role="alert">
                                    Brak pojazdów
                                </div>
                            @else

                                <table class="table table-hover">
                                    <thead class="text-warning">
                                        <th>ID</th>
                                        <th>Strona</th>
                                        <th>Data modyfikacji</th>
                                        <th>Akcja</th>
                                    </thead>
                                    <tbody>

                                    @foreach ($pages as $page)

                                        <tr>
                                            <td>{{ $page->id }}</td>
                                            <td>{{ $page->name }}</td>
                                            <td>{{ $page->updated_at }}</td>
                                            <td class="td-actions text-right">
                                                <a href="{{ url('cms/page/' . $page->id . '/edit') }}" rel="tooltip" title="Edytuj stronę" class="btn btn-primary btn-simple btn-xs">
                                                    <i class="material-icons">mode_edit</i>
                                                </a>
                                            </td>
                                        </tr>

                                    @endforeach

                                    </tbody>
                                </table>

                            @endif

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
@endsection