<!doctype html>
<html lang="{{ app()->getLocale() }}">

<head>
    <meta charset="utf-8" />
    <link rel="apple-touch-icon" sizes="76x76" href="/apple-icon.png" />
    <link rel="icon" type="image/png" href="/favicon.png" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />

    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/material-dashboard.css') }}" rel="stylesheet" />
   
    <link href="{{ asset('css/demo.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/style.css') }}" rel="stylesheet" />
    
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600,700,800,900&subset=latin,latin-ext" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300|Material+Icons' rel='stylesheet' type='text/css'>

</head>

    <body>

        <div class="wrapper">
            <div class="sidebar" data-color="purple" data-image="images/sidebar-1.jpg">
        
                <div class="logo">
                    <a href="{{url('/cms/dashboard')}}" class="simple-text">
                        <img src="{{ asset('images/logo.png') }}" alt="">
                    </a>
                </div>

                <div class="sidebar-wrapper">
                    <ul class="nav">
                        @if (Auth::check() && Auth::user()->role->type === 'admin')
                            <li class="{{ (Request::is('cms/dashboard') ? 'active' : '') }}">
                                <a href="{{ url('cms/dashboard') }}">
                                    <i class="material-icons">dashboard</i>
                                    <p>Dashboard</p>
                                </a>
                            </li>
                            <li class="{{ (Request::is('cms/user') ? 'active' : '') }}">
                                <a href="{{ url('cms/user') }}">
                                    <i class="material-icons">people</i>
                                    <p>Użytkownicy</p>
                                </a>
                            </li>
                            <li class="{{ (Request::is('cms/user/create') ? 'active' : '') }}">
                                <a href="{{ url('cms/user/create') }}">
                                    <i class="material-icons">group_add</i>
                                    <p>Dodaj użytkownika</p>
                                </a>
                            </li>
                            <li class="{{ (Request::is('cms/car') ? 'active' : '') }}">
                                <a href="{{ url('cms/car') }}">
                                    <i class="material-icons">directions_car</i>
                                    <p>Lista aut</p>
                                </a>
                            </li>
                            <li class="{{ (Request::is('cms/car/create') ? 'active' : '') }}">
                                <a href="{{ url('cms/car/create') }}">
                                    <i class="material-icons">control_point</i>
                                    <p>Dodaj auto</p>
                                </a>
                            </li>
                            <li class="{{ (Request::is('cms/page') ? 'active' : '') }}">
                                <a href="{{ url('cms/page') }}">
                                    <i class="material-icons">pages</i>
                                    <p>Strony</p>
                                </a>
                            </li>
                            <li class="{{ (Request::is('/cms/update') ? 'active' : '') }}">
                                <a href="{{ url('/cms/update') }}">
                                    <i class="material-icons">refresh</i>
                                    <p>Aktualizuj bazę</p>
                                </a>
                            </li>
                            <li class="{{ (Request::is('/cms/customers') ? 'active' : '') }}">
                                <a href="{{ url('/cms/customers') }}">
                                    <i class="material-icons">gavel</i>
                                    <p>Licytacje</p>
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('logout') }}"
                                    onclick="event.preventDefault();
                                             document.getElementById('logout-form').submit();">
                                    <i class="material-icons">lock_open</i>
                                    <p>Wyloguj się</p>
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                                
                            </li>
                        @endif

                    </ul>
                </div>
            </div>

            <div class="sidebar-background"></div>

            <div class="main-panel">
                <div class="container-fluid">
                    <nav class="navbar navbar-transparent navbar-absolute">
                        <div class="container-fluid">
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle" data-toggle="collapse">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                            </div>
                        </div>
                    </nav>
                </div>
                
                @yield('content')

            </div>
        </div>
    </body>

    <script src="{{ asset('js/jquery-3.2.1.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/bootstrap.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/material.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/arrive.min.js') }}"></script>
    <script src="{{ asset('js/perfect-scrollbar.jquery.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap-notify.js') }}"></script>
    <script src="{{ asset('js/material-dashboard.js') }}"></script>
    <script src="http://spin.js.org/spin.js"></script>

    <script>
        $(".alert-message").fadeIn(200).delay(1500).fadeOut(1000);
    </script>

    <script src="{{ asset('vendor/tinymce/tinymce.min.js') }}"></script>
    <script>
        tinymce.init({
          selector: 'textarea',
          height: 500,
          menubar: false,
          plugins: [
            'advlist autolink lists link image charmap print preview anchor',
            'searchreplace visualblocks code fullscreen',
            'insertdatetime media table contextmenu paste code'
          ],
          toolbar: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
          content_css: [
            '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
            '//www.tinymce.com/css/codepen.min.css']
        });
    </script>

</html>