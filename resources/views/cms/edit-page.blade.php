@extends('cms.left')

@section('content')

   <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header" data-background-color="orange">
                            <h4 class="title">Edytuj pojazd</h4>
                            <p class="category">Edytuj formularz</p>
                        </div>
                        <div class="card-content">
                        <br>
                            <div class="row">
                                <div class="col-md-12">
                                    <h4><strong>Edycja pojazdu</strong></h4>
                                    <form role="form" method="POST" action="{{ url('cms/page/' . $page->id) }}" enctype="multipart/form-data">
                                        {{ csrf_field() }}
                                        {{ method_field('PATCH') }}
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group label-floating {{ $errors->has('page') ? ' has-error' : '' }}">
                                                    <label for="page" class="control-label">Nazwa strony</label>
                                                    <input type="text" class="form-control" name="name" value="{{ $page->name }}">

                                                    @if ($errors->has('page'))
                                                        <span class="help-block">
                                                            <strong>{{ $errors->first('page') }}</strong>
                                                        </span>
                                                    @endif

                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group label-floating">
                                                    <textarea type="text" class="form-control" name="description">{{ $page->description }}</textarea>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <button type="submit" class="btn btn-orange pull-left">Edytuj stronę</button>
                                        <div class="clearfix"></div>

                                    </form>
                                </div>
                            </div>
                        
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection