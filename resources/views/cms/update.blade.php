@extends('cms.left')

@section('content')

    @if (Session::has('car_updated_home'))
        <div class="alert-message" role="alert">
            <div class="row">
                <div class="col-md-2">
                    <i class="material-icons">directions_car</i>
                </div>
                <div class="col-md-10">
                    <div class="message">
                        {{Session::get('car_updated_home')}}
                    </div>
                </div>
            </div>
        </div>
    @endif

    @if (Session::has('car_updated_allianz'))
        <div class="alert-message" role="alert">
            <div class="row">
                <div class="col-md-2">
                    <i class="material-icons">directions_car</i>
                </div>
                <div class="col-md-10">
                    <div class="message">
                        {{Session::get('car_updated_allianz')}}
                    </div>
                </div>
            </div>
        </div>
    @endif

    @if (Session::has('car_updated_rest'))
        <div class="alert-message" role="alert">
            <div class="row">
                <div class="col-md-2">
                    <i class="material-icons">directions_car</i>
                </div>
                <div class="col-md-10">
                    <div class="message">
                        {{Session::get('car_updated_rest')}}
                    </div>
                </div>
            </div>
        </div>
    @endif

    @if (Session::has('car_updated_scauto'))
        <div class="alert-message" role="alert">
            <div class="row">
                <div class="col-md-2">
                    <i class="material-icons">directions_car</i>
                </div>
                <div class="col-md-10">
                    <div class="message">
                        {{Session::get('car_updated_scauto')}}
                    </div>
                </div>
            </div>
        </div>
    @endif

    <div class="content">
        <div class="container-fluid">
            <div class="row">

                <div class="col-lg-6 col-md-6 col-sm-6 show">
                    <div class="card card-stats">
                        <div class="card-header" data-background-color="yellow">
                            <i class="material-icons">refresh</i>
                        </div>
                        <div class="card-dashboard">
                            <p class="category">Home</p>
                            
                            <h3 class="title"></h3>
                        </div>
                        <div class="card-footer">
                            <div class="stats">
                                <i class="material-icons">link</i>
                                <a href="{{ url('/cms/update/update_home') }}">Aktualizuj bazę</a>
                            </div>
                            <div class="stats pull-right">
                                <i class="material-icons">date_range</i>
                                {{-- Ostatnia akutalizacja: {{ $home->created_at }} --}}
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-6 col-md-6 col-sm-6 show">
                    <div class="card card-stats">
                        <div class="card-header" data-background-color="orange">
                            <i class="material-icons">refresh</i>
                        </div>
                        <div class="card-dashboard">
                            <p class="category">Allianz</p>
                            
                            <h3 class="title"></h3>
                        </div>
                        <div class="card-footer">
                            <div class="stats">
                                <i class="material-icons">link</i>
                                <a href="{{ url('/cms/update/update_allianz') }}">Aktualizuj bazę</a>
                            </div>
                            <div class="stats pull-right">
                                <i class="material-icons">date_range</i>
                                {{-- Ostatnia akutalizacja: {{ $allianz->created_at }} --}}
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-6 col-md-6 col-sm-6 show">
                    <div class="card card-stats">
                        <div class="card-header" data-background-color="green">
                            <i class="material-icons">refresh</i>
                        </div>
                        <div class="card-dashboard">
                            <p class="category">Rest</p>
                            <h3 class="title"></h3>
                        </div>
                        <div class="card-footer">
                            <div class="stats">
                                <i class="material-icons">link</i>
                                <a id='refresh' href="{{ url('/cms/update/update_rest') }}">Aktualizuj bazę</a>
                            </div>
                            {{-- <div class="stats pull-right">
                                <i class="material-icons">date_range</i>
                                Ostatnia akutalizacja: {{ $rest->created_at }}
                            </div> --}}
                        </div>
                    </div>
                </div>

                <div class="col-lg-6 col-md-6 col-sm-6 show">
                    <div class="card card-stats">
                        <div class="card-header" data-background-color="blue">
                            <i class="material-icons">refresh</i>
                        </div>
                        <div class="card-dashboard">
                            <p class="category">SCAuto</p>
                            <h3 class="title"></h3>
                        </div>
                        <div class="card-footer">
                            <div class="stats">
                                <i class="material-icons">link</i>
                                <a href="{{ url('/cms/update/update_scauto') }}">Aktualizuj bazę</a>
                            </div>
                            {{-- <div class="stats pull-right">
                                <i class="material-icons">date_range</i>
                                Ostatnia akutalizacja: {{ $scauto->created_at }}
                            </div> --}}
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

@endsection