@extends('cms.left')

@section('content')

    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 show">
                    <div class="card card-stats">
                        <div class="card-header" data-background-color="orange">
                            <i class="material-icons">directions_car</i>
                        </div>
                        <div class="card-dashboard">
                            <p class="category">Liczba wszystkich pojazdów</p>
                            @php
                            @endphp
                            <h3 class="title">{{ $car }}</h3>
                            
                        </div>
                        <div class="card-footer">
                            <div class="stats">
                                {{-- <i class="material-icons">link</i>
                                <a href="{{ url('/') }}">Zobacz</a> --}}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 show">
                    <div class="card card-stats">
                        <div class="card-header" data-background-color="green">
                            <i class="material-icons">people</i>
                        </div>
                        <div class="card-dashboard">
                            <p class="category">Liczba użytkowników</p>
                            <h3 class="title">{{ $users }}</h3>
                        </div>
                        <div class="card-footer">
                            <div class="stats">
                                <i class="material-icons">link</i>
                                <a href="{{ url('cms/user') }}">Zobacz</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection