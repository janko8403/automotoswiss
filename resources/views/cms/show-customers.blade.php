@extends('cms.left')

@section('content')

	{{-- @if (Session::has('auctions_destroy'))
        <div class="alert-message" role="alert">
            <div class="row">
                <div class="col-md-2">
                    <i class="material-icons">directions_car</i>
                </div>
                <div class="col-md-10">
                    <div class="message">
                        {{Session::get('auctions_destroy')}}
                    </div>
                </div>
            </div>
        </div>
    @endif --}}

	<div class="content">
	    <div class="container-fluid">
	        <div class="row">
	            <div class="col-md-12 show">
	                <div class="card">
	                    <div class="card-header" data-background-color="orange">
	                        <h4 class="title">Licytacje użytkownika </h4>
	                        <p class ="category">{{ $user->name }}</p>
	                    </div>
	                    <div class="card-content">
	                    <br>

	                    	{{-- @if ( $auctions->isEmpty())
                                <div class="alert alert-danger" role="alert">
                                    Użytkownik nie brał udziału w żadnej aukcji
                                </div>
                            @else --}}

		                        <table class="table table-hover">
		                            <thead class="text-warning">
		                                <th>ID</th>
		                                <th>Koniec aukcji</th>
		                                <th>Tytuł oferty</th>
		                                <th>Sprzedawca</th>
		                                <th>Cena</th>
		                                <th>Akcja</th>
		                            </thead>
		                            <tbody>

                                    	@php 
   											$mytime = Carbon\Carbon::now(new DateTimeZone('Europe/Berlin'));
   											$i = 1;
                                    	@endphp
		             
		                            	@foreach ( $auctions as $auction)

			                                <tr>
			                                    <td>{{ $i++ }}</td>
			                                    <td>{{ $auction->date }}</td>
			                                    <td>{{ $auction->name }}</td>
			                                    <td>
			                                    	@if ($auction->kto == 'Home')
			                                    		Axa
		                                    		@else
		                                    			{{ $auction->kto }}
			                                    	@endif
			                                    </td>
			                                    <td><strong>{{ $auction->price }}</strong> Chf</td>

			                                    <td class="td-actions text-right">

			                                    	@if ($auction->date > $mytime )
														<span rel ="tooltip" title="Aukcja trwająca" class="btn btn-danger btn-simple btn-xs green">
	                                                        <i class="material-icons">mood</i>
	                                                    </span>
                                                    @endif
                                                    @if ($auction->date < $mytime )
	                                                    <span rel="tooltip" title="Aukcja zakończona" class="btn btn-danger btn-simple btn-xs">
	                                                        <i class="material-icons">mood_bad</i>
	                                                    </span>
                                                    @endif

                                                    @if ($auction->date < $mytime )
                                                    	@if (!$auction->kto === 'sprowadzone')
		                                                    <span rel="tooltip" title="Aukcja zakończona" class="btn btn-danger btn-simple btn-xs">
		                                                        <i class="material-icons">mood_bad</i>
		                                                    </span>
	                                                    @endif
                                                    @endif

													@if ($auction->kto === 'Allianz')
				                                        <a href="{{ url('/allianz/' . $auction->auct_id) }}" rel="tooltip" title="Zobacz aukcję" class="btn btn-primary btn-simple btn-xs">
				                                            <i class="material-icons">link</i>
				                                        </a>
			                                        @endif

			                                        @if ($auction->kto === 'Home')
				                                        <a href="{{ url('/home/' . $auction->auct_id) }}" rel="tooltip" title="Zobacz aukcję" class="btn btn-primary btn-simple btn-xs">
				                                            <i class="material-icons">link</i>
				                                        </a>
			                                        @endif

			                                        @if ($auction->kto === 'SCAuto')
				                                        <a href="{{ url('/scauto/' . $auction->auct_id) }}" rel="tooltip" title="Zobacz aukcję" class="btn btn-primary btn-simple btn-xs">
				                                            <i class="material-icons">link</i>
				                                        </a>
			                                        @endif

			                                        @if ($auction->kto === 'Rest')
				                                        <a href="{{ url('/rest/' . $auction->auct_id) }}" rel="tooltip" title="Zobacz aukcję" class="btn btn-primary btn-simple btn-xs">
				                                            <i class="material-icons">link</i>
				                                        </a>
			                                        @endif

			                                        @if ($auction->kto === 'sprowadzone')
				                                        <a href="{{ url('/aukcje/' . $auction->auct_id) }}" rel="tooltip" title="Zobacz aukcję" class="btn btn-primary btn-simple btn-xs">
				                                            <i class="material-icons">link</i>
				                                        </a>
			                                        @endif

			                                    </td>
			                                </tr>
		                                @endforeach
		                                
		                            </tbody>
		                        </table>

	                        {{-- @endif --}}

	                    </div>
	                </div>
	            </div>
	        </div>
	    </div>
	</div>
	
@endsection