@extends('cms.left')

@section('content')

    @if (Session::has('user_created'))
        <div class="alert-message" role="alert">
            <div class="row">
                <div class="col-md-2">
                    <i class="material-icons">group_add</i>
                </div>
                <div class="col-md-10">
                    <div class="message">
                        {{Session::get('user_created')}}
                    </div>
                </div>
            </div>
        </div>
    @endif

    @if (Session::has('user_destroy'))
        <div class="alert-message" role="alert">
            <div class="row">
                <div class="col-md-2">
                    <i class="material-icons">group_add</i>
                </div>
                <div class="col-md-10">
                    <div class="message">
                        {{Session::get('user_destroy')}}
                    </div>
                </div>
            </div>
        </div>
    @endif

    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header" data-background-color="orange">
                            <h4 class="title">Użytkownicy</h4>
                            <p class ="category">Dodani użytkownicy</p>
                        </div>
                        <div class="card-content">
                        <br>

                            @if ($users->count() === 2)
                                <div class="alert alert-danger" role="alert">
                                    Brak użytkowników
                                </div>
                            @else

                                <table class="table table-hover">
                                    <thead class="text-warning">
                                        <th>ID</th>
                                        <th>Użytkownik</th>
                                        <th>E-mail</th>
                                        <th>Telefon</th>
                                        <th>Akcja</th>
                                    </thead>
                                    <tbody>

                                        @foreach ($users as $user)

                                            @if ($user->role_id == 1)

                                            @else

                                            <tr>
                                                <td>{{ $user->id }}</td>
                                                <td>{{ $user->name }}</td>
                                                <td>{{ $user->email }}</td>
                                                <td>{{ $user->phone }}</td>

                                                @if ($user->id == 3)
                                                    <td class="td-actions text-right">
                                                        <span>
                                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                        </span>
                                                        <span rel="tooltip" title="Użytkownika aktywny" class="btn btn-danger btn-simple btn-xs green">
                                                            <i class="material-icons">check</i>
                                                        </span>
                                                    </td>
                                                @else

                                                    <td class="td-actions text-right">
                                                        <a href="{{ url('cms/user/' . $user->id . '/edit') }}" rel="tooltip" title="Edytuj użytkownika" class="btn btn-primary btn-simple btn-xs">
                                                            <i class="material-icons">mode_edit</i>
                                                        </a>
                                                        <form class="form-horizontal" role="form" method="POST" action="{{ url('cms/user/' . $user->id) }}">
                                                            {{ csrf_field() }}
                                                            <input type="hidden" name="_method" value="delete">
                                            
                                                            <button type="submit" rel="tooltip" title="Usuń użytkownika" class="btn btn-danger btn-simple btn-xs">
                                                                <i class="material-icons">close</i>
                                                            </button>
                                                        </form>

                                                        @if ($user->hash === 'active')
                                                            <span rel="tooltip" title="Użytkownika aktywny" class="btn btn-danger btn-simple btn-xs green">
                                                                <i class="material-icons">check</i>
                                                            </span>
                                                        @else
                                                            <span rel="tooltip" title="Użytkownika nieaktywny" class="btn btn-danger btn-simple btn-xs">
                                                                <i class="material-icons">not_interested</i>
                                                            </span>
                                                        @endif
                                                    </td>
                                                @endif
                                            </tr>

                                            @endif

                                        @endforeach
                                    </tbody>
                                </table>

                            @endif

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
@endsection