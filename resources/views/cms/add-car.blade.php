@extends('cms.left')

@section('content')

   <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header" data-background-color="orange">
                            <h4 class="title">Dodaj pojazd</h4>
                            <p class="category">Wypełnij formularz</p>
                        </div>
                        <div class="card-content">
                        <br>
                            <div class="row">
                                <div class="col-md-12">
                                    <h4><strong>Wypełnij formularz aby dodać auto</strong></h4>

                                    <form role="form" method="POST" action="{{ url('cms/car') }}" enctype="multipart/form-data">
                                        {{ csrf_field() }}
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group label-floating {{ $errors->has('marka') ? ' has-error' : '' }}">
                                                    <label for="marka" class="control-label">Marka pojazdu</label>
                                                    <input type="text" class="form-control" name="marka" value="{{ old('marka') }}">

                                                    @if ($errors->has('marka'))
                                                        <span class="help-block">
                                                            <strong>{{ $errors->first('marka') }}</strong>
                                                        </span>
                                                    @endif

                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group label-floating {{ $errors->has('model') ? ' has-error' : '' }}">
                                                    <label for="model" class="control-label">Model pojazdu</label>
                                                    <input type="text" class="form-control" name="model" value="{{ old('model') }}">

                                                    @if ($errors->has('model'))
                                                        <span class="help-block">
                                                            <strong>{{ $errors->first('model') }}</strong>
                                                        </span>
                                                    @endif

                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group label-floating {{ $errors->has('version') ? ' has-error' : '' }}">
                                                    <label for="version" class="control-label">Wersja pojazdu</label>
                                                    <input type="text" class="form-control" name="version" value="{{ old('version') }}">

                                                    @if ($errors->has('version'))
                                                        <span class="help-block">
                                                            <strong>{{ $errors->first('version') }}</strong>
                                                        </span>
                                                    @endif

                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group label-floating {{ $errors->has('old') ? ' has-error' : '' }}">
                                                    <label for="old" class="control-label">Rok produkcji</label>
                                                    <input type="text" class="form-control" name="old" value="{{ old('old') }}">

                                                    @if ($errors->has('old'))
                                                        <span class="help-block">
                                                            <strong>{{ $errors->first('old') }}</strong>
                                                        </span>
                                                    @endif

                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group label-floating {{ $errors->has('capacity') ? ' has-error' : '' }}">
                                                    <label for="capacity" class="control-label">Pojemność skokowa</label>
                                                    <input type="text" class="form-control" name="capacity" value="{{ old('capacity') }}">

                                                    @if ($errors->has('capacity'))
                                                        <span class="help-block">
                                                            <strong>{{ $errors->first('capacity') }}</strong>
                                                        </span>
                                                    @endif

                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group label-floating {{ $errors->has('km') ? ' has-error' : '' }}">
                                                    <label for="km" class="control-label">Przebieg</label>
                                                    <input type="text" class="form-control" name="km" value="{{ old('km') }}">

                                                    @if ($errors->has('km'))
                                                        <span class="help-block">
                                                            <strong>{{ $errors->first('km') }}</strong>
                                                        </span>
                                                    @endif

                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group label-floating">
                                                    <label class="control-label">Vin</label>
                                                    <input type="text" class="form-control" name="vin" value="{{ old('vin') }}">
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group label-floating">
                                                    <label class="control-label">Moc</label>
                                                    <input type="text" class="form-control" name="power" value="{{ old('power') }}">
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group label-floating">
                                                    <label class="control-label">Rodzaj paliwa</label>
                                                    <input type="text" class="form-control" name="gas" value="{{ old('gas') }}">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group label-floating">
                                                    <label class="control-label">Skrzynia biegów</label>
                                                    <input type="text" class="form-control" name="transmission" value="{{ old('transmission') }}">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group label-floating">
                                                    <label class="control-label">Napęd</label>
                                                    <input type="text" class="form-control" name="drive" value="{{ old('drive') }}">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group label-floating">
                                                    <label class="control-label">Opis</label>
                                                    <textarea type="text" class="form-control" name="description">{{ old('description') }}</textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-2">
                                                <div class="form-group label-floating">
                                                    <label class="control-label">Zdjęcie pojazdu</label>
                                                    <input type="file" name="images" class="form-control" >
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-group label-floating">
                                                    <label class="control-label">Zdjęcie pojazdu</label>
                                                    <input type="file" name="images_2" class="form-control" >
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-group label-floating">
                                                    <label class="control-label">Zdjęcie pojazdu</label>
                                                    <input type="file" name="images_3" class="form-control" >
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-group label-floating">
                                                    <label class="control-label">Zdjęcie pojazdu</label>
                                                    <input type="file" name="images_4" class="form-control" >
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-group label-floating">
                                                    <label class="control-label">Zdjęcie pojazdu</label>
                                                    <input type="file" name="images_5" class="form-control" >
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-group label-floating">
                                                    <label class="control-label">Zdjęcie pojazdu</label>
                                                    <input type="file" name="images_6" class="form-control" >
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-group label-floating">
                                                    <label class="control-label">Zdjęcie pojazdu</label>
                                                    <input type="file" name="images_7" class="form-control" >
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-group label-floating">
                                                    <label class="control-label">Zdjęcie pojazdu</label>
                                                    <input type="file" name="images_8" class="form-control" >
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-group label-floating">
                                                    <label class="control-label">Zdjęcie pojazdu</label>
                                                    <input type="file" name="images_9" class="form-control" >
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="form-group label-floating">
                                                    <label class="control-label">Zdjęcie pojazdu</label>
                                                    <input type="file" name="images_10" class="form-control" >
                                                </div>
                                            </div>
                                        </div>
                                        <br><br>
                                        
                                        <button type="submit" class="btn btn-orange pull-left">Dodaj auto</button>
                                        <div class="clearfix"></div>

                                    </form>
                                </div>
                            </div>
                        
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection