@extends('cms.left')

@section('content')

    @if (Session::has('car_created'))
        <div class="alert-message" role="alert">
            <div class="row">
                <div class="col-md-2">
                    <i class="material-icons">directions_car</i>
                </div>
                <div class="col-md-10">
                    <div class="message">
                        {{Session::get('car_created')}}
                    </div>
                </div>
            </div>
        </div>
    @endif

    @if (Session::has('car_destroy'))
        <div class="alert-message" role="alert">
            <div class="row">
                <div class="col-md-2">
                    <i class="material-icons">directions_car</i>
                </div>
                <div class="col-md-10">
                    <div class="message">
                        {{Session::get('car_destroy')}}
                    </div>
                </div>
            </div>
        </div>
    @endif

    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header" data-background-color="orange">
                            <h4 class="title">Samochody</h4>
                            <p class ="category">Dodane pojazdy</p>
                        </div>
                        <div class="card-content">
                        <br>

                            @if ($cars->isEmpty())
                                <div class="alert alert-danger" role="alert">
                                    Brak pojazdów
                                </div>
                            @else

                                <table class="table table-hover">
                                    <thead class="text-warning">
                                        <th>ID</th>
                                        <th>Model pojazdu</th>
                                        <th>Rok produkcji</th>
                                        <th>Przebieg</th>
                                        <th>Akcja</th>
                                    </thead>
                                    <tbody>

                                    @foreach ($cars as $car)

                                        <tr>
                                            <td>{{ $car->id }}</td>
                                            <td>{{ $car->marka }} {{ $car->model }} {{ $car->version }}</td>
                                            <td>{{ $car->old }}</td>
                                            <td>{{ $car->km }}</td>
                                            <td class="td-actions text-right">
                                                <a href="{{ url('cms/car/' . $car->id . '/edit') }}" rel="tooltip" title="Edytuj pojazd" class="btn btn-primary btn-simple btn-xs">
                                                    <i class="material-icons">mode_edit</i>
                                                </a>
                                                <form class="form-horizontal" role="form" method="POST" action="{{ url('cms/car/' . $car->id) }}">
                                                    {{ csrf_field() }}
                                                    <input type="hidden" name="_method" value="delete">
                                    
                                                    <button type="submit" rel="tooltip" title="Usuń pojazd" class="btn btn-danger btn-simple btn-xs">
                                                        <i class="material-icons">close</i>
                                                    </button>
                                                </form>
                                                {{-- <a href="" rel="tooltip" title="Zobacz pojazd" class="btn btn-danger btn-simple btn-xs">
                                                    <i class="material-icons">link</i>
                                                </a> --}}
                                            </td>
                                        </tr>

                                    @endforeach

                                    </tbody>
                                </table>

                            @endif

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
@endsection