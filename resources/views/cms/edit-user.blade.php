@extends('cms.left')

@section('content')

   <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header" data-background-color="orange">
                            <h4 class="title">Edytuj użytkownika</h4>
                            <p class="category">Edytuj formularz</p>
                        </div>
                        <div class="card-content">
                        <br>
                            <div class="row">
                                <div class="col-md-12">
                                    <h4><strong>Wypełnij formularz aby edytować użytkownika</strong></h4>
                                    <form role="form" method="POST" action="{{ url('cms/user/'. $user->id) }}">
                                        {{ csrf_field() }}
                                        {{ method_field('PATCH') }}
                                        <div class="row">
                                            <div class="col-md-5">
                                                <div class="form-group label-floating {{ $errors->has('name') ? ' has-error' : '' }}">
                                                    <label class="control-label">Imię</label>
                                                    <input type="text" class="form-control" name="name" value="{{ $user->name }}">

                                                    @if ($errors->has('name'))
                                                        <span class="help-block">
                                                            <strong>{{ $errors->first('name') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="col-md-5 col-md-offset-1">
                                                <div class="form-group label-floating {{ $errors->has('email') ? ' has-error' : '' }}">
                                                    <label class="control-label">Email</label>
                                                    <input type="email" class="form-control" name="email" value="{{ $user->email }}">

                                                    @if ($errors->has('email'))
                                                        <span class="help-block">
                                                            <strong>{{ $errors->first('email') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-5">
                                                <div class="form-group label-floating {{ $errors->has('phone') ? ' has-error' : '' }}">
                                                    <label class="control-label">Telefon</label>
                                                    <input type="text" class="form-control" name="phone" value="{{ $user->phone }}">

                                                    @if ($errors->has('phone'))
                                                        <span class="help-block">
                                                            <strong>{{ $errors->first('phone') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="col-md-5 col-md-offset-1">
                                                <div class="form-group label-floating {{ $errors->has('password') ? ' has-error' : '' }}">
                                                    <label class="control-label">Hasło</label>
                                                    <input type="text" class="form-control" name="password" value="{{ old('password') }}">

                                                    @if ($errors->has('password'))
                                                        <span class="help-block">
                                                            <strong>{{ $errors->first('password') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <button type="submit" class="btn btn-orange pull-left">Edytuj użytkownika</button>
                                        <div class="clearfix"></div>

                                    </form>
                                </div>
                            </div>
                        
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection