@extends('page.left')

@section('content')

        <img src="images/header.jpg" class="header img-responsive">
    </div>

    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header" data-background-color="orange">
                            <h4 class="title">Wyniki wyszukiwania</h4>
                        </div>
                        <div class="card-content table-responsive">

                            <table class="table table-hover">
                                <thead class="text-primary">
                                    <th>Marka</th>
                                    <th>Model</th>
                                    <th>Typ</th>
                                    <th></th>
                                </thead>
                                <tbody>
                                    <div class="row">
                                        <div class="col-md-12">

                                            @foreach ($search_allianz as $car)
                                                <tr>
                                                    <td>{{ $car->marka }}</td>
                                                    <td>{{ $car->typ }}</td>
                                                    <td>{{ $car->model }}</td>
                                                    <td>
                                                        <a href="{{ url('/allianz/' . $car->id) }}" rel="tooltip" title="Zobacz aukcję" class="btn btn-primary btn-simple btn-xs">
                                                            <i class="material-icons">link</i>
                                                        </a>
                                                    </td>
                                                </tr>
                                            @endforeach 

                                            @foreach ($search_rest as $car)
                                                <tr>
                                                    <td>{{ $car->marka }}</td>
                                                    <td>{{ $car->typ }}</td>
                                                    <td>{{ $car->model }}</td>
                                                    <td>
                                                        <a href="{{ url('/rest/' . $car->id) }}" rel="tooltip" title="Zobacz aukcję" class="btn btn-primary btn-simple btn-xs">
                                                            <i class="material-icons">link</i>
                                                        </a>
                                                    </td>
                                                </tr>
                                            @endforeach 
                                            
                                            @foreach ($search_scauto as $car)
                                                <tr>
                                                    <td>{{ $car->marka }}</td>
                                                    <td>{{ $car->typ }}</td>
                                                    <td>{{ $car->model }}</td>
                                                    <td>
                                                        <a href="{{ url('/scauto/' . $car->id) }}" rel="tooltip" title="Zobacz aukcję" class="btn btn-primary btn-simple btn-xs">
                                                            <i class="material-icons">link</i>
                                                        </a>
                                                    </td>
                                                </tr>
                                            @endforeach 
                                            @foreach ($search_home as $car)
                                                <tr>
                                                    <td>{{ $car->car_name }}</td>
                                                    <td>----</td>
                                                    <td>----</td>
                                                    <td>
                                                        <a href="{{ url('/home/' . $car->id) }}" rel="tooltip" title="Zobacz aukcję" class="btn btn-primary btn-simple btn-xs">
                                                            <i class="material-icons">link</i>
                                                        </a>
                                                    </td>
                                                </tr>
                                            @endforeach 
                                        </div>
                                    </div>
                                </tbody>
                            </table>
                           
                        </div>

                        <div class="row">
                            <div class="col-lg-12">
                                <div class="text-center">{{ $search_allianz }}</div>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection