@extends('page.left')

@section('content')

    <div class="content">
        <div class="container-fluid">
            <div class="row">

                @php 
                    $mmytime = Carbon\Carbon::now(new DateTimeZone('Europe/Berlin'));
                @endphp

                @foreach ($rests as $rest)
                    <div class="col-md-3 col-sm-4 col-xs-12">
                        <div class="card card-stats">
                            <div class="card-header-main">
                                <a href="{{ url('/rest/' . $rest->id) }}">
                                    <img src="{{ $rest->photos_0 }}" alt="{{ $rest->marka }}">
                                </a>
                            </div>
                            <div class="card-content-car">
                                <div class="box-title-main">
                                    <h3 class="title">{{ str_limit($rest->marka . ' ' . $rest->typ . ' ' . $rest->model, 55, '') }}</h3>
                                </div>

                                <div class="time-left">Data zakończenia aukcji: <br> <span data-countdown="{{ $rest->end_date_of_auction }}"></span></div>
                                <div class="date-left">{{ $rest->end_date_of_auction }}</div>   
                            </div>
                            <div class="card-footer">
                                <a class="stats" href="{{ url('/rest/' . $rest->id) }}">Licytuj</a>
                            </div>
                        </div>
                    </div>
                @endforeach

            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="text-center">{{ $rests }}</div>
                </div>
            </div>
        </div>
    </div>

@endsection