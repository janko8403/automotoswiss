@extends('page.left')

@section('content')

    @php 
        $mytime = Carbon\Carbon::now(new DateTimeZone('Europe/Berlin'));
    @endphp

    @if (Session::has('add_auction'))
        <div class="alert-message" role="alert">
            <div class="row">
                <div class="col-md-12">
                    <div class="message">
                        {{Session::get('add_auction')}}
                    </div>
                </div>
            </div>
        </div>
    @endif

    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12 show">
                    <div class="card">
                        <div class="card-header" data-background-color="orange">
                            <h4 class="title">{!! ($rest->marka) !!} {{ $rest->typ }}{{ $rest->model }}</h4>
                            <p class="category">Czas do końca aukcji: <span>{!! ($rest->end_date_of_auction < $mytime) ? 'aukcja zakończona' : $rest->end_date_of_auction !!}</span></p>
                        </div>
                        <div class="card-content">
                            <br>
                            <div class="row">
                                <div class="col-md-6 col-md-offset-3">
                                    <img src="{{ $rest->photos_0 }}" alt="{{ $rest->marka }}" class="img-responsive img-center">
                                    <br>
                                </div>
                                <div class="col-md-12">
                                    <div class="row">
                                        @if (!empty($rest->photos_1))
                                            <div class="col-md-2">
                                                <div class="img-article">
                                                    <a href="{{ $rest->photos_1 }}" data-toggle="lightbox" data-gallery="example-gallery">
                                                        <img src="{{ $rest->photos_1 }}" alt="{{ $rest->marka }}" class="img-responsive img-center">
                                                    </a>
                                                </div>
                                            </div>
                                        @endif
                                        @if (!empty($rest->photos_2))
                                            <div class="col-md-2">
                                                <div class="img-article">
                                                    <a href="{{ $rest->photos_2 }}" data-toggle="lightbox" data-gallery="example-gallery">
                                                        <img src="{{ $rest->photos_2 }}" alt="{{ $rest->marka }}" class="img-responsive img-center">
                                                    </a>
                                                </div>
                                            </div>
                                         @endif
                                        @if (!empty($rest->photos_3))
                                            <div class="col-md-2">
                                                <div class="img-article">
                                                    <a href="{{ $rest->photos_3 }}" data-toggle="lightbox" data-gallery="example-gallery">
                                                        <img src="{{ $rest->photos_3 }}" alt="{{ $rest->marka }}" class="img-responsive img-center">
                                                    </a>
                                                </div>
                                            </div>
                                         @endif
                                        @if (!empty($rest->photos_4))
                                            <div class="col-md-2">
                                                <div class="img-article">
                                                    <a href="{{ $rest->photos_4 }}" data-toggle="lightbox" data-gallery="example-gallery">
                                                        <img src="{{ $rest->photos_4 }}" alt="{{ $rest->marka }}" class="img-responsive img-center">
                                                    </a>
                                                </div>
                                            </div>
                                         @endif
                                        @if (!empty($rest->photos_5))
                                            <div class="col-md-2">
                                                <div class="img-article">
                                                    <a href="{{ $rest->photos_5 }}" data-toggle="lightbox" data-gallery="example-gallery">
                                                        <img src="{{ $rest->photos_5 }}" alt="{{ $rest->marka }}" class="img-responsive img-center">
                                                    </a>
                                                </div>
                                            </div>
                                         @endif
                                        @if (!empty($rest->photos_6))
                                            <div class="col-md-2">
                                                <div class="img-article">
                                                    <a href="{{ $rest->photos_6 }}" data-toggle="lightbox" data-gallery="example-gallery">
                                                        <img src="{{ $rest->photos_6 }}" alt="{{ $rest->marka }}" class="img-responsive img-center">
                                                    </a>
                                                </div>
                                            </div>
                                         @endif
                                    </div>
                
                                    <div class="empty-space"></div>

                                    <div class="row">
                                        @if (!empty($rest->photos_7))
                                            <div class="col-md-2">
                                                <div class="img-article">
                                                    <a href="{{ $rest->photos_7 }}" data-toggle="lightbox" data-gallery="example-gallery">
                                                        <img src="{{ $rest->photos_7 }}" alt="{{ $rest->marka }}" class="img-responsive img-center">
                                                    </a>
                                                </div>
                                            </div>
                                        @endif
                                        @if (!empty($rest->photos_8))
                                            <div class="col-md-2">
                                                <div class="img-article">
                                                    <a href="{{ $rest->photos_8 }}" data-toggle="lightbox" data-gallery="example-gallery">
                                                        <img src="{{ $rest->photos_8 }}" alt="{{ $rest->marka }}" class="img-responsive img-center">
                                                    </a>
                                                </div>
                                            </div>
                                        @endif
                                        @if (!empty($rest->photos_9))
                                            <div class="col-md-2">
                                                <div class="img-article">
                                                    <a href="{{ $rest->photos_9 }}" data-toggle="lightbox" data-gallery="example-gallery">
                                                        <img src="{{ $rest->photos_9 }}" alt="{{ $rest->marka }}" class="img-responsive img-center">
                                                    </a>
                                                </div>
                                            </div>
                                        @endif
                                        @if (!empty($rest->photos_10))
                                            <div class="col-md-2">
                                                <div class="img-article">
                                                    <a href="{{ $rest->photos_10 }}" data-toggle="lightbox" data-gallery="example-gallery">
                                                        <img src="{{ $rest->photos_10 }}" alt="{{ $rest->marka }}" class="img-responsive img-center">
                                                    </a>
                                                </div>
                                            </div>
                                        @endif
                                        @if (!empty($rest->photos_11))
                                            <div class="col-md-2">
                                                <div class="img-article">
                                                    <a href="{{ $rest->photos_11 }}" data-toggle="lightbox" data-gallery="example-gallery">
                                                        <img src="{{ $rest->photos_11 }}" alt="{{ $rest->marka }}" class="img-responsive img-center">
                                                    </a>
                                                </div>
                                            </div>
                                        @endif
                                        @if (!empty($rest->photos_12))
                                            <div class="col-md-2">
                                                <div class="img-article">
                                                    <a href="{{ $rest->photos_12 }}" data-toggle="lightbox" data-gallery="example-gallery">
                                                        <img src="{{ $rest->photos_12 }}" alt="{{ $rest->marka }}" class="img-responsive img-center">
                                                    </a>
                                                </div>
                                            </div>
                                        @endif
                                    </div>

                                    <div class="empty-space"></div>
                                    
                                    <div class="row">
                                        @if (!empty($rest->photos_13))
                                            <div class="col-md-2">
                                                <div class="img-article">
                                                    <a href="{{ $rest->photos_13 }}" data-toggle="lightbox" data-gallery="example-gallery">
                                                        <img src="{{ $rest->photos_13 }}" alt="{{ $rest->marka }}" class="img-responsive img-center">
                                                    </a>
                                                </div>
                                            </div>
                                        @endif
                                        @if (!empty($rest->photos_14))
                                            <div class="col-md-2">
                                                <div class="img-article">
                                                    <a href="{{ $rest->photos_14 }}" data-toggle="lightbox" data-gallery="example-gallery">
                                                        <img src="{{ $rest->photos_14 }}" alt="{{ $rest->marka }}" class="img-responsive img-center">
                                                    </a>
                                                </div>
                                            </div>
                                        @endif
                                        @if (!empty($rest->photos_15))
                                            <div class="col-md-2">
                                                <div class="img-article">
                                                    <a href="{{ $rest->photos_15 }}" data-toggle="lightbox" data-gallery="example-gallery">
                                                        <img src="{{ $rest->photos_15 }}" alt="{{ $rest->marka }}" class="img-responsive img-center">
                                                    </a>
                                                </div>
                                            </div>
                                        @endif
                                        @if (!empty($rest->photos_16))
                                            <div class="col-md-2">
                                                <div class="img-article">
                                                    <a href="{{ $rest->photos_16 }}" data-toggle="lightbox" data-gallery="example-gallery">
                                                        <img src="{{ $rest->photos_16 }}" alt="{{ $rest->marka }}" class="img-responsive img-center">
                                                    </a>
                                                </div>
                                            </div>
                                        @endif
                                        @if (!empty($rest->photos_17))
                                            <div class="col-md-2">
                                                <div class="img-article">
                                                    <a href="{{ $rest->photos_17 }}" data-toggle="lightbox" data-gallery="example-gallery">
                                                        <img src="{{ $rest->photos_17 }}" alt="{{ $rest->marka }}" class="img-responsive img-center">
                                                    </a>
                                                </div>
                                            </div>
                                        @endif
                                        @if (!empty($rest->photos_18))
                                            <div class="col-md-2">
                                                <div class="img-article">
                                                    <a href="{{ $rest->photos_18 }}" data-toggle="lightbox" data-gallery="example-gallery">
                                                        <img src="{{ $rest->photos_18 }}" alt="{{ $rest->marka }}" class="img-responsive img-center">
                                                    </a>
                                                </div>
                                            </div>
                                        @endif
                                    </div>

                                    <div class="empty-space"></div>
                                    
                                    <div class="row">
                                        @if (!empty($rest->photos_19))
                                            <div class="col-md-2">
                                                <div class="img-article">
                                                    <a href="{{ $rest->photos_19 }}" data-toggle="lightbox" data-gallery="example-gallery">
                                                        <img src="{{ $rest->photos_19 }}" alt="{{ $rest->marka }}" class="img-responsive img-center">
                                                    </a>
                                                </div>
                                            </div>
                                        @endif
                                        @if (!empty($rest->photos_20))
                                            <div class="col-md-2">
                                                <div class="img-article">
                                                    <a href="{{ $rest->photos_20 }}" data-toggle="lightbox" data-gallery="example-gallery">
                                                        <img src="{{ $rest->photos_20 }}" alt="{{ $rest->marka }}" class="img-responsive img-center">
                                                    </a>
                                                </div>
                                            </div>
                                        @endif
                                        @if (!empty($rest->photos_21))
                                            <div class="col-md-2">
                                                <div class="img-article">
                                                    <a href="{{ $rest->photos_21 }}" data-toggle="lightbox" data-gallery="example-gallery">
                                                        <img src="{{ $rest->photos_21 }}" alt="{{ $rest->marka }}" class="img-responsive img-center">
                                                    </a>
                                                </div>
                                            </div>
                                        @endif
                                        @if (!empty($rest->photos_22))
                                            <div class="col-md-2">
                                                <div class="img-article">
                                                    <a href="{{ $rest->photos_22 }}" data-toggle="lightbox" data-gallery="example-gallery">
                                                        <img src="{{ $rest->photos_22 }}" alt="{{ $rest->marka }}" class="img-responsive img-center">
                                                    </a>
                                                </div>
                                            </div>
                                        @endif
                                        @if (!empty($rest->photos_23))
                                            <div class="col-md-2">
                                                <div class="img-article">
                                                    <a href="{{ $rest->photos_23 }}" data-toggle="lightbox" data-gallery="example-gallery">
                                                        <img src="{{ $rest->photos_23 }}" alt="{{ $rest->marka }}" class="img-responsive img-center">
                                                    </a>
                                                </div>
                                            </div>
                                        @endif
                                        @if (!empty($rest->photos_24))
                                            <div class="col-md-2">
                                                <div class="img-article">
                                                    <a href="{{ $rest->photos_24 }}" data-toggle="lightbox" data-gallery="example-gallery">
                                                        <img src="{{ $rest->photos_24 }}" alt="{{ $rest->marka }}" class="img-responsive img-center">
                                                    </a>
                                                </div>
                                            </div>
                                        @endif
                                    </div>

                                    <div class="empty-space"></div>
                                    
                                    <div class="row">
                                        @if (!empty($rest->photos_25))
                                            <div class="col-md-2">
                                                <div class="img-article">
                                                    <a href="{{ $rest->photos_25 }}" data-toggle="lightbox" data-gallery="example-gallery">
                                                        <img src="{{ $rest->photos_25 }}" alt="{{ $rest->marka }}" class="img-responsive img-center">
                                                    </a>
                                                </div>
                                            </div>
                                        @endif
                                        @if (!empty($rest->photos_26))
                                            <div class="col-md-2">
                                                <div class="img-article">
                                                    <a href="{{ $rest->photos_26 }}" data-toggle="lightbox" data-gallery="example-gallery">
                                                        <img src="{{ $rest->photos_26 }}" alt="{{ $rest->marka }}" class="img-responsive img-center">
                                                    </a>
                                                </div>
                                            </div>
                                        @endif
                                        @if (!empty($rest->photos_27))
                                            <div class="col-md-2">
                                                <div class="img-article">
                                                    <a href="{{ $rest->photos_27 }}" data-toggle="lightbox" data-gallery="example-gallery">
                                                        <img src="{{ $rest->photos_27 }}" alt="{{ $rest->marka }}" class="img-responsive img-center">
                                                    </a>
                                                </div>
                                            </div>
                                        @endif
                                        @if (!empty($rest->photos_28))
                                            <div class="col-md-2">
                                                <div class="img-article">
                                                    <a href="{{ $rest->photos_28 }}" data-toggle="lightbox" data-gallery="example-gallery">
                                                        <img src="{{ $rest->photos_28 }}" alt="{{ $rest->marka }}" class="img-responsive img-center">
                                                    </a>
                                                </div>
                                            </div>
                                        @endif
                                        @if (!empty($rest->photos_29))
                                            <div class="col-md-2">
                                                <div class="img-article">
                                                    <a href="{{ $rest->photos_29 }}" data-toggle="lightbox" data-gallery="example-gallery">
                                                        <img src="{{ $rest->photos_29 }}" alt="{{ $rest->marka }}" class="img-responsive img-center">
                                                    </a>
                                                </div>
                                            </div>
                                        @endif
                                        @if (!empty($rest->photos_30))
                                            <div class="col-md-2">
                                                <div class="img-article">
                                                    <a href="{{ $rest->photos_30 }}" data-toggle="lightbox" data-gallery="example-gallery">
                                                        <img src="{{ $rest->photos_30 }}" alt="{{ $rest->marka }}" class="img-responsive img-center">
                                                    </a>
                                                </div>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    @if (!empty($rest->kolor = NULL))
                                        <h6><strong>Kolor:</strong> {{ $rest->kolor }}</h6>
                                    @endif
                                    <h6><strong>Pierwsza rejestracja:</strong> {{ $rest->pierwsza_rejestracja }}</h6>
                                    <h6><strong>Przebieg:</strong> {{ $rest->przebieg }} <strong>tyś. km</strong></h6>
                                    <h6><strong>Dowód własności:</strong> {{ $rest->dowod_wlasnosci }}</h6>
                                    <h6><strong>Konstrukcja:</strong> {{ $rest->konstrukcja }}</h6>
                                    <h6><strong>Opony:</strong> {{ $rest->opony }}</h6>
                                    <h6><strong>Podwozie:</strong> {{ $rest->podwozie }}</h6>
                                    <h6><strong>Stan:</strong> {{ $rest->stan }}</h6>
                                    <h6><strong>MFK:</strong> {{ $rest->mfk }}</h6>
                                    <h6><strong>Wyposazenie:</strong> {{ $rest->wyposazenie }}</h6>
                                    <h6><strong>Opis uszkodzenia:</strong> {{ $rest->opis_uszkodzenia }}</h6>
                                    <h6><strong>Strefa obrażeń:</strong> {{ $rest->strefa_obrazen }}</h6>
                                    <h6><strong>Wystrzelone poduszki:</strong>
                                        @if ($rest->wystrzelone_poduszki == 0)
                                            Nie
                                        @else
                                            {{ $rest->wystrzelone_poduszki }}
                                        @endif
                                     </h6>
                                    <h6><strong>Koniec aukcji:</strong> {{ $rest->end_date_of_auction }}</h6>
                                    <p></p>
                                </div>
                            </div>

                        </div>
                    </div>

                    @if (Auth::check() && is_user())
                        <div class="card" style="{!! ($rest->end_date_of_auction < $mytime) ? 'display:none' : '' !!}">
                            
                            <div class="card-content">
                                <div class="empty-space"></div>
                                <h4>Podaj swoją cenę we <strong>frankach szwajcarskich (Chf)</strong></h4>
                                <p style="font-size: 11px;"><strong>Oferta musi być wyższa niż poprzednia oferta. W przeciwnym wypadku nie zostanie umieszczona.</strong></p>


                                <div class="row">
                                    <div class="col-md-4">
                                        <form role="form" method="POST" action="{{ url('/opis') }}">
                                            {{ csrf_field() }}

                                            <input type="hidden" name="operator" value="{{ $rest->operator }}">
                                            <input type="hidden" name="date" value="{{ $rest->end_date_of_auction }}">
                                            <input type="hidden" name="name" value="{{ $rest->marka }} {{ $rest->typ }}{{ $rest->model }}">
                                            <input type="hidden" name="auct_id" value="{{ $rest->id }}">
                                            <input type="hidden" name="user_id" value="{{ Auth::user()->id }}">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group label-floating {{ $errors->has('price') ? ' has-error' : '' }}">
                                                        <label class="control-label">Twoja cena</label>
                                                        <input type="number" class="form-control" name="price" value="{{ old('price') }}">

                                                        @if ($errors->has('price'))
                                                            <span class="help-block">
                                                                <strong>{{ $errors->first('price') }}</strong>
                                                            </span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            <button type="submit" class="btn btn-orange">Licytuję</button>
                                            <div class="clearfix"></div>
                                        </form>
                                    </div>
                                </div>
                            </div>

                        </div>
                    @else
                        <div class="card">
                            <div class="card-content">
                                <div class="empty-space"></div>
                                <h4>Musisz być zalogowanym aby licytować!</h4>
                            </div>
                        </div>
                    @endif

                </div>
            </div>
        </div>
    </div>

@endsection