@extends('page.left')

@section('content')

    <div class="content">
        <div class="container-fluid">
            <div class="row">

                @php 
                    $mytime = Carbon\Carbon::now(new DateTimeZone('Europe/Berlin'));
                @endphp

                @foreach ($homes as $home)
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="card card-stats">
                            <div class="card-header-main">
                                <a href="{{ url('/home/' . $home->id) }}">
                                    <img src="{{ $home->photos_0 }}" alt="{{ $home->car_name }}">
                                </a>
                            </div>
                            <div class="card-content-car">
                                <div class="box-title-main">
                                    <h3 class="title">{{ $home->car_name }}</h3>
                                </div>    
                                <div class="time-left">Data zakończenia aukcji: <br> <span data-countdown="{{ $home->end_date_of_auction }}"></span></div>
                                <div class="date-left">{{ $home->end_date_of_auction }}</div>  
                            </div>
                            <div class="card-footer">
                                <a class="stats" href="{{ url('/home/' . $home->id) }}">Licytuj</a>
                            </div>
                        </div>
                    </div>
                @endforeach

            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="text-center">{{ $homes }}</div>
                </div>

            </div>
        </div>
    </div>

@endsection