@extends('page.left')

@section('content')

		<div id="map" class="header"></div>
   </div>

   @if (Session::has('send_form'))
        <div class="alert-message" role="alert">
            <div class="row">
                <div class="col-md-2">
                    <i class="material-icons">send</i>
                </div>
                <div class="col-md-10">
                    <div class="message">
                        {{Session::get('send_form')}}
                    </div>
                </div>
            </div>
        </div>
    @endif
                
	<div class="content">
	    <div class="container-fluid">
	        <div class="row">
	            <div class="col-md-12 show">
	                <div class="card">
	                    <div class="card-header" data-background-color="orange">
	                        <h4 class="title">Kontakt</h4>
	                        <p class="category">Skontaktuj się z nami</p>
	                    </div>
	                    <div class="card-content">
	                    <br>
	                        <div class="row">
	                            <div class="col-md-6">
	                                <h4><strong>AutoMotoSwiss</strong> <br>
	                                    Robert <strong>500388446</strong> <br>
										Edyta <strong>692466330</strong>  
	                                </h4>
	                                <p>Skontaktuj się z nami – my z przyjemnością dowieziemy twoje auto do celu.</p>

	                            </div>
	                            <div class="col-md-6">
	                                <h4><strong>Napisz do nas</strong></h4>

	                                <form role="form" method="POST" action="{{ url('/kontakt') }}">
									    {{ csrf_field() }}
									    <div class="row">
									        <div class="col-md-6">
									            <div class="form-group label-floating {{ $errors->has('name') ? ' has-error' : '' }}">
									                <label class="control-label">Imię</label>
									                <input type="text" class="form-control" name="name" value="{{ old('name') }}">

									                @if ($errors->has('name'))
									                    <span class="help-block">
									                        <strong>{{ $errors->first('name') }}</strong>
									                    </span>
									                @endif
									            </div>
									        </div>
									        <div class="col-md-6">
									            <div class="form-group label-floating {{ $errors->has('email') ? ' has-error' : '' }}">
									                <label class="control-label">Email</label>
									                <input type="email" class="form-control" name="email" value="{{ old('email') }}">

									                @if ($errors->has('email'))
									                    <span class="help-block">
									                        <strong>{{ $errors->first('email') }}</strong>
									                    </span>
									                @endif
									            </div>
									        </div>
									    </div>
									    <div class="row">
									        <div class="col-md-12">
									            <div class="form-group label-floating">
									                <label class="control-label">Telefon</label>
									                <input type="text" class="form-control" name="phone">
									            </div>
									        </div>
									    </div>
									    
									    
									    <div class="row">
									        <div class="col-md-12">
									            <div class="form-group">
									                <div class="form-group label-floating">
									                    <label class="control-label">Treść wiadomości</label>
									                    <textarea class="form-control" rows="5" name="text"></textarea>
									                </div>
									            </div>
									        </div>
									    </div>
									    <button type="submit" class="btn btn-orange pull-right">Wyślij wiadomość</button>
									    <div class="clearfix"></div>
									</form>
	                                
	                            </div>
	                        </div>
	                    
	                    </div>
	                </div>
	            </div>
	        </div>
		</div>
	</div>

@endsection