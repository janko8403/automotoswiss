@extends('page.left')

@section('content')

    <div class="content">
        <div class="container-fluid">
            <div class="row">

                @php 
                    $mytime = Carbon\Carbon::now(new DateTimeZone('Europe/Berlin'));
                @endphp

                @foreach ($allianzs as $allianz)
                    <div class="col-md-3 col-sm-4 col-xs-12">
                        <div class="card card-stats">
                            <div class="card-header-main">
                                <a href="{{ url('/allianz/' . $allianz->id) }}">
                                    <img src="{{ $allianz->photos_0 }}" alt="{{ $allianz->marka }}">
                                </a>
                            </div>
                            <div class="card-content-car">
                                <div class="box-title-main">
                                    <h3 class="title">{{ str_limit($allianz->marka . ' ' . $allianz->typ . ' ' . $allianz->model, 55, '') }}</h3>
                                </div>
                                    
                                {{-- <div class="date">{{ $allianz->links_to_photos }}</div> --}}
                                <div class="time-left">Data zakończenia aukcji: <br> <span data-countdown="{{ $allianz->end_date_of_auction }}"></span></div>
                                <div class="date-left">{{ $allianz->end_date_of_auction }}</div>
                            </div>
                            <div class="card-footer">
                                <a class="stats" href="{{ url('/allianz/' . $allianz->id) }}">Licytuj</a>
                            </div>
                        </div>
                    </div>
                @endforeach

            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="text-center">{{ $allianzs }}</div>
                </div>
            </div>
        </div>
    </div>

@endsection