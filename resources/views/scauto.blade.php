@extends('page.left')

@section('content')

    <div class="content">
        <div class="container-fluid">
            <div class="row">

                @php 
                    $mytime = Carbon\Carbon::now(new DateTimeZone('Europe/Berlin'));
                @endphp
                     

                @foreach ($scautos as $scauto)
                    <div class="col-md-3 col-sm-4 col-xs-12">
                        <div class="card card-stats">
                            <div class="card-header-main">
                                <a href="{{ url('/scauto/' . $scauto->id) }}">
                                    <img src="{{ $scauto->photos_0 }}" alt="{{ $scauto->marka }}">
                                </a>
                            </div>
                            <div class="card-content-car">
                                <div class="box-title-main">
                                    <h3 class="title">{{ str_limit($scauto->marka . ' ' . $scauto->typ . ' ' . $scauto->model, 55, '') }}</h3>
                                </div>
                                    
                                {{-- <div class="date">{{ $scauto->links_to_photos }}</div> --}}
                                <div class="time-left">Data zakończenia aukcji: <br> <span data-countdown="{{ $scauto->end_date_of_auction }}"></span></div>
                                <div class="date-left">{{ $scauto->end_date_of_auction }}</div>   
                            </div>
                            <div class="card-footer">
                                <a class="stats" href="{{ url('/scauto/' . $scauto->id) }}">Licytuj</a>
                            </div>
                        </div>
                    </div>
                @endforeach

            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="text-center">{{ $scautos }}</div>
                </div>

            </div>
        </div>
    </div>

@endsection