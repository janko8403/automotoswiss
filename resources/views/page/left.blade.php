<!doctype html>
<html lang="{{ app()->getLocale() }}">

<head>
    <meta charset="utf-8" />
    <link rel="apple-touch-icon" sizes="76x76" href="/apple-icon.png" />
    <link rel="icon" type="image/png" href="/favicon.png" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />

    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/material-dashboard.css') }}" rel="stylesheet" />
   
    <link href="{{ asset('css/demo.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/style.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/ekko-lightbox.css') }}" rel="stylesheet" />
    
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600,700,800,900&subset=latin,latin-ext" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300|Material+Icons' rel='stylesheet' type='text/css'>

</head>

    <body>
        <div class="wrapper">
            <div class="sidebar" data-color="purple" data-image="images/sidebar-1.jpg">
        
                <div class="logo">
                    <a href="{{url('/')}}" class="simple-text">
                        <img src="{{ asset('images/logo.png') }}" alt="">
                    </a>
                </div>

                <div class="sidebar-wrapper">
                    <ul class="nav">
                        <li class="{{ (Request::is('o-nas') ? 'active' : '') }}">
                            <a href="{{url('o-nas')}}">
                                <i class="material-icons">bubble_chart</i>
                                <p>O nas</p>
                            </a>
                        </li>
                        <li class="{{ (Request::is('transport') ? 'active' : '') }}">
                            <a href="{{url('/transport')}}">
                                <i class="material-icons">local_shipping</i>
                                <p>Transport</p>
                            </a>
                        </li>

                        <li>
                            <a data-toggle="collapse" href="#pagesExamples">
                                <i class="material-icons">content_paste</i>
                                <p> Aukcje
                                    <b class="caret"></b>
                                </p>
                            </a>
                            <div class="collapse" id="pagesExamples">
                                <ul class="nav">
                                    <li class="{{ (Request::is('axa') ? 'active' : '') }}">
                                        <a href="{{url('/axa')}}">
                                            <span class="sidebar-mini"> A </span>
                                            <span class="sidebar-normal"> Axa </span>
                                        </a>
                                    </li>
                                    <li class="{{ (Request::is('rest') ? 'active' : '') }}">
                                        <a href="{{url('/rest')}}">
                                            <span class="sidebar-mini"> R </span>
                                            <span class="sidebar-normal"> Rest </span>
                                        </a>
                                    </li>
                                    <li class="{{ (Request::is('scauto') ? 'active' : '') }}">
                                        <a href="{{url('/scauto')}}">
                                            <span class="sidebar-mini"> SC </span>
                                            <span class="sidebar-normal"> SCAuto</span>
                                        </a>
                                    </li>
                                    <li class="{{ (Request::is('allianz') ? 'active' : '') }}">
                                        <a href="{{url('/allianz')}}">
                                            <span class="sidebar-mini"> A </span>
                                            <span class="sidebar-normal"> Allianz </span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        
                        <li class="{{ (Request::is('aukcje') ? 'active' : '') }}">
                            <a href="{{url('/aukcje')}}">
                                <i class="material-icons">directions_car</i>
                                <p>Auta sprowadzone</p>
                            </a>
                        </li>
                        <li class="{{ (Request::is('kontakt') ? 'active' : '') }}">
                            <a href="{{url('/kontakt')}}">
                                <i class="material-icons">location_on</i>
                                <p>Kontakt</p>
                            </a>
                        </li>
                        @if (Auth::guest())
                            <li class="{{ (Request::is('zarejestruj-sie') ? 'active' : '') }}">
                                <a href="{{url('/zarejestruj-sie')}}">
                                    <i class="material-icons">play_for_work</i>
                                    <p>Zarejestruj się</p>
                                </a>
                            </li>
                        @endif
                        @if ( Auth::check() && is_admin())
                            <li class="{{ (Request::is('cms/dashboard') ? 'active' : '') }}">
                                <a href="{{ url('cms/dashboard') }}">
                                    <i class="material-icons">dashboard</i>
                                    <p>Wróc do Dashboard</p>
                                </a>
                            </li>
                        @endif
                        @if ( Auth::check() && is_admin() || is_user())
                            <li>
                                <a href="{{ route('logout') }}"
                                    onclick="event.preventDefault();
                                             document.getElementById('logout-form').submit();">
                                    <i class="material-icons">lock_open</i>
                                    <p>Wyloguj się</p>
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                                
                            </li>
                        @else
                            <li class="{{ (Request::is('zaloguj-sie') ? 'active' : '') }}">
                                <a href="{{url('/zaloguj-sie')}}">
                                    <i class="material-icons">vpn_key</i>
                                    <p>Zaloguj się</p>
                                </a>
                            </li>
                        @endif
                        @if ( Auth::check() && is_user())
                            <li class="{{ (Request::is('moje-aukcje') ? 'active' : '') }}">
                                <a href="{{url('/moje-aukcje')}}">
                                    <i class="material-icons text-gray">notifications</i>
                                    <p>Moje aukcje</p>
                                </a>
                            </li>
                        @endif
                    </ul>

                    @php
                        $usd = file_get_contents("http://api.nbp.pl/api/exchangerates/rates/a/usd?format=json");
                        $eur = file_get_contents("http://api.nbp.pl/api/exchangerates/rates/a/eur?format=json");
                        $w_usd= $usd;
                        $w_eur= $eur;
                        $a_usd = json_decode($w_usd, true);
                        $a_eur = json_decode($w_eur, true);

                        $usd = $a_usd['rates'][0]['mid'];
                        $eur = $a_eur['rates'][0]['mid'];
                       
                    @endphp

                    <div class="waluty">
                        <div class="title">Aktualne kursy walut:</div>
                        <div class="waluta">EUR: <strong>{{ str_limit($eur, 4, '') }}</strong></div>
                        <div class="waluta">USD: <strong>{{ str_limit($usd, 4, '') }}</strong></div>
                    </div>
                </div>
            </div>

            <div class="sidebar-background"></div>

            <div class="main-panel">
                <div class="container-fluid">
                    <nav class="navbar navbar-transparent navbar-absolute">
                        <div class="container-fluid">
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle" data-toggle="collapse">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                            </div>

                            <div class="collapse navbar-collapse">
                                <div class="navbar-form navbar-right">
                                    <form meth="GET" action="{{ url('/search') }}">
                                        <input type="text" class="form-control-my" name="q" placeholder="Szukaj...">
                                        <button type="submit" class="btn-search">
                                             <i class="material-icons">search</i>
                                        </button>
                                    </form>
                                </div>
                            </div>

                        </div>
                    </nav>
                
                @yield('content')

                {{-- <div class="cookie-disclaimer">
                    <div class="cookie-close accept-cookie"><i class="material-icons">close</i></div>
                    <div class="container">
                        <p>Strona używa cookies aby polepszyć Twoje doznania podczas korzystania z niej. Zaakceptuj naszą politykę <a href="{{ url('cookie') }}">cookie</a>. 
                        <p><button type="button" class="btn btn-success accept-cookie">Akceptuję</button></p>
                    </div>
                </div> --}}

                <footer class="footer">
                    <div class="container-fluid">
                        <p class="copyright pull-left">
                            ul. Kwiatowa 52 Miętne 08-400 Garwolin
                        </p>
                        <p class="copyright pull-right">
                            <a href="{{ url('cookie') }}">Polityka prywatności</a>
                        </p>
                    </div>
                </footer>
            </div>
        </div>
    </body>
    
    <script src="{{ asset('js/jquery-3.2.1.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/bootstrap.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/material.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/arrive.min.js') }}"></script>
    <script src="{{ asset('js/perfect-scrollbar.jquery.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap-notify.js') }}"></script>
    <script src="{{ asset('js/material-dashboard.js') }}"></script>
    <script src="{{ asset('js/ekko-lightbox.min.js') }}"></script>
    <script src="{{ asset('js/ekko-lightbox.min.js.map') }}"></script>
    <script src="{{ asset('js/jquery.countdown.js') }}"></script>
    <script src="{{ asset('js/index.js') }}"></script>
    <script src="{{ asset('js/jquery.autocomplete.min.js') }}"></script>

    {{-- script>
        $(function(){
            var currencies = [
                @foreach ($scautos as $scauto)
                  {
                    value: '{{ $scauto->marka }} {{ $scauto->typ }}{{ $scauto->model }}',
                    data: '{{ url('/scauto/' . $scauto->id) }}'
                  },
              @endforeach
              @foreach ($rests as $rest)
                {
                    value: '{{ $rest->marka }} {{ $rest->typ }}{{ $rest->model }}',
                    data: '{{ url('/rest/' . $rest->id) }}'
                  },
              @endforeach
              @foreach ($allianzs as $allianz)
                {
                    value: '{{ $allianz->marka }} {{ $allianz->typ }}{{ $allianz->model }}',
                    data: '{{ url('/allianz/' . $allianz->id) }}'
                  },
              @endforeach
            ];
            
            $('#autocomplete').autocomplete({
              lookup: currencies,
              onSelect: function (suggestion) {
                var thehtml = '<strong>Currency Name:</strong> ' + suggestion.value + ' <br> <strong>Symbol:</strong> ' + suggestion.data;
                $('#outputcontent').html(thehtml);
              }
            });
            
          });
    </script> --}}

    <script>
        $(document).on('click', '[data-toggle="lightbox"]', function(event) {
            event.preventDefault();
            $(this).ekkoLightbox();
        });
    </script>
    <script>
        $('[data-countdown]').each(function() {
           var $this = $(this), finalDate = $(this).data('countdown');
           $this.countdown(finalDate, function(event) {
             $this.html(event.strftime('%D dni %Hh %Mm %Ss'));
           });
        });
    </script>

    <!-- MAPS -->
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAGsVXGASqs4OSmUeNNcZZpXyvu04Y8tPw&sensor=false"></script>
    <script type="text/javascript">
                function initialize() {
      var mapOptions = {
                        zoom: 12,
                        caleControl : false,
                        scrollwheel : false,
                        zoomControl : false,
                        center: new google.maps.LatLng(51.922139, 21.600460), 
                        styles: [{featureType:'all',stylers:[{saturation:-100},{gamma:0.50}]}]
                    };
      var map = new google.maps.Map(document.getElementById('map'),
                                    mapOptions);
      var image = 'images/marker.png';
      var myLatLng = new google.maps.LatLng(51.922139, 21.600460);
      var beachMarker = new google.maps.Marker({
          position: myLatLng,
          map: map,
          icon: image
      });
    }
    google.maps.event.addDomListener(window, 'load', initialize);
    </script>
    <!-- END MAPS -->

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-111437746-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-111437746-1');
    </script>

    <script>
        $(".alert-message").fadeIn(200).delay(1500).fadeOut(1000);
    </script>

</html>