@extends('page.left')

@section('content')

    @if (Session::has('add_auction'))
        <div class="alert-message" role="alert">
            <div class="row">
                <div class="col-md-12">
                    <div class="message">
                        {{Session::get('add_auction')}}
                    </div>
                </div>
            </div>
        </div>
    @endif

    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12 show">
                    <div class="card">
                        <div class="card-header" data-background-color="orange">
                            <h4 class="title">{{ $sprowadzone->marka }} {{ $sprowadzone->model }} {{ $sprowadzone->version }} </h4>
                            <p class="category">Rok produkcji:<span> {{ $sprowadzone->old }}</span></p>
                        </div>
                        <div class="card-content">
                            <br>
                            <div class="row">
                                <div class="col-md-6 col-md-offset-3">
                                    <img src="{{ asset('storage/car/' . str_slug($sprowadzone->model, '-') . '/img/' . $sprowadzone->images) }}" alt="{{ $sprowadzone->marka }}" class="img-responsive img-center">
                                    <br>
                                </div>
                                <div class="col-md-12">
                                    <div class="row">

                                        @if (!empty($sprowadzone->images_2))
                                            <div class="col-md-2 col-md-offset-1">
                                                <div class="img-article">
                                                    <a href="{{ asset('storage/car/' . str_slug($sprowadzone->model, '-') . '/img/' . $sprowadzone->images_2) }}" data-toggle="lightbox" data-gallery="example-gallery">
                                                        <img src="{{ asset('storage/car/' . str_slug($sprowadzone->model, '-') . '/img/' . $sprowadzone->images_2) }}" alt="{{ $sprowadzone->marka }}" class="img-responsive img-center">
                                                    </a>
                                                </div>
                                            </div>
                                         @endif

                                        @if (!empty($sprowadzone->images_3))
                                            <div class="col-md-2">
                                                <div class="img-article">
                                                    <a href="{{ asset('storage/car/' . str_slug($sprowadzone->model, '-') . '/img/' . $sprowadzone->images_3) }}" data-toggle="lightbox" data-gallery="example-gallery">
                                                        <img src="{{ asset('storage/car/' . str_slug($sprowadzone->model, '-') . '/img/' . $sprowadzone->images_3) }}" alt="{{ $sprowadzone->marka }}" class="img-responsive img-center">
                                                    </a>
                                                </div>
                                            </div>
                                        @endif

                                        @if (!empty($sprowadzone->images_4))
                                            <div class="col-md-2">
                                                <div class="img-article">
                                                    <a href="{{ asset('storage/car/' . str_slug($sprowadzone->model, '-') . '/img/' . $sprowadzone->images_4) }}" data-toggle="lightbox" data-gallery="example-gallery">
                                                        <img src="{{ asset('storage/car/' . str_slug($sprowadzone->model, '-') . '/img' . $sprowadzone->images_4) }}" alt="{{ $sprowadzone->marka }}" class="img-responsive img-center">
                                                    </a>
                                                </div>
                                            </div>
                                        @endif

                                        @if (!empty($sprowadzone->images_5))
                                            <div class="col-md-2">
                                                <div class="img-article">
                                                    <a href="{{ asset('storage/car/' . str_slug($sprowadzone->model, '-') . '/img/' . $sprowadzone->images_5) }}" data-toggle="lightbox" data-gallery="example-gallery">
                                                        <img src="{{ asset('storage/car/' . str_slug($sprowadzone->model, '-') . '/img/' . $sprowadzone->images_5) }}" alt="{{ $sprowadzone->marka }}" class="img-responsive img-center">
                                                    </a>
                                                </div>
                                            </div>
                                        @endif

                                        @if (!empty($sprowadzone->images_6))
                                            <div class="col-md-2">
                                                <div class="img-article">
                                                    <a href="{{ asset('storage/car/' . str_slug($sprowadzone->model, '-') . '/img/' . $sprowadzone->images_6) }}" data-toggle="lightbox" data-gallery="example-gallery">
                                                        <img src="{{ asset('storage/car/' . str_slug($sprowadzone->model, '-') . '/img/' . $sprowadzone->images_6) }}" alt="{{ $sprowadzone->marka }}" class="img-responsive img-center">
                                                    </a>
                                                </div>
                                            </div>
                                        @endif

                                    </div>
                                    <br>
                                </div>
                                <div class="col-md-12">
                                    <div class="row">

                                        @if (!empty($sprowadzone->images_7))
                                            <div class="col-md-2 col-md-offset-1">
                                                <div class="img-article">
                                                    <a href="{{ asset('storage/car/' . str_slug($sprowadzone->model, '-') . '/img/' . $sprowadzone->images_7) }}" data-toggle="lightbox" data-gallery="example-gallery">
                                                        <img src="{{ asset('storage/car/' . str_slug($sprowadzone->model, '-') . '/img/' . $sprowadzone->images_7) }}" alt="{{ $sprowadzone->marka }}" class="img-responsive img-center">
                                                    </a>
                                                </div>
                                            </div>
                                        @endif

                                        @if (!empty($sprowadzone->images_8))
                                            <div class="col-md-2">
                                                <div class="img-article">
                                                    <a href="{{ asset('storage/car/' . str_slug($sprowadzone->model, '-') . '/img/' . $sprowadzone->images_8) }}" data-toggle="lightbox" data-gallery="example-gallery">
                                                        <img src="{{ asset('storage/car/' . str_slug($sprowadzone->model, '-') . '/img/' . $sprowadzone->images_8) }}" alt="{{ $sprowadzone->marka }}" class="img-responsive img-center">
                                                    </a>
                                                </div>
                                            </div>
                                        @endif

                                        @if (!empty($sprowadzone->images_9))
                                            <div class="col-md-2">
                                                <div class="img-article">
                                                    <a href="{{ asset('storage/car/' . str_slug($sprowadzone->model, '-') . '/img/' . $sprowadzone->images_9) }}" data-toggle="lightbox" data-gallery="example-gallery">
                                                        <img src="{{ asset('storage/car/' . str_slug($sprowadzone->model, '-') . '/img/' . $sprowadzone->images_9) }}" alt="{{ $sprowadzone->marka }}" class="img-responsive img-center">
                                                    </a>
                                                </div>
                                            </div>
                                        @endif

                                        @if (!empty($sprowadzone->images_10))
                                            <div class="col-md-2">
                                                <div class="img-article">
                                                    <a href="{{ asset('storage/car/' . str_slug($sprowadzone->model, '-') . '/img/' . $sprowadzone->images_10) }}" data-toggle="lightbox" data-gallery="example-gallery">
                                                        <img src="{{ asset('storage/car/' . str_slug($sprowadzone->model, '-') . '/img/' . $sprowadzone->images_10) }}" alt="{{ $sprowadzone->marka }}" class="img-responsive img-center">
                                                    </a>
                                                </div>
                                            </div>
                                        @endif
            
                                    </div>
                                    <br>
                                </div>
                                <div class="col-md-12">
                                    <h6><strong>Marka:</strong> {{ $sprowadzone->marka }}</h6>
                                    <h6><strong>Model:</strong> {{ $sprowadzone->model }}</h6>
                                    <h6><strong>Wersja:</strong> {{ $sprowadzone->version }}</h6>
                                    <h6><strong>Pierwsza rejestracja:</strong> {{ $sprowadzone->old }}</h6>
                                    <h6><strong>Pojemność:</strong> {{ $sprowadzone->capacity}}</h6>
                                    <h6><strong>Przebieg:</strong> {{ $sprowadzone->km}} <strong>tyś. km</strong></h6>
                                    <h6><strong>Vin:</strong> {{ $sprowadzone->vin}}</h6>
                                    <h6><strong>Moc:</strong> {{ $sprowadzone->power}}</h6>
                                    <h6><strong>Rodzaj paliwa:</strong> {{ $sprowadzone->gas}}</h6>
                                    <h6><strong>Skrzynia biegów:</strong> {{ $sprowadzone->transmission}}</h6>
                                    <h6><strong>Napęd:</strong> {{ $sprowadzone->drive}}</h6>
                                    <h6><strong>Opis:</strong></h6>
                                    {!! $sprowadzone->description !!}
                                </div>
                            </div>

                        </div>
                    </div>

                    @php 
                        $mytime = Carbon\Carbon::now(new DateTimeZone('Europe/Berlin'));
                    @endphp


                    @if (Auth::check() && is_user())
                        <div class="card">
                            
                            <div class="card-content">
                                
                                <div class="empty-space"></div>

                                <h4>Podaj swoją cenę</h4>
                                <p style="font-size: 11px;"><strong>Oferta musi być wyższa niż poprzednia oferta. W przeciwnym wypadku nie zostanie umieszczona.</strong></p>

                                <div class="row">
                                    <div class="col-md-4">
                                        <form role="form" method="POST" action="{{ url('/opis') }}">
                                            {{ csrf_field() }}

                                            <input type="hidden" name="operator" value="sprowadzone">
                                            <input type="hidden" name="date" value="{{ $mytime }} ">
                                            <input type="hidden" name="name" value="{{ $sprowadzone->marka }} {{ $sprowadzone->model }} {{ $sprowadzone->version }}">
                                            <input type="hidden" name="auct_id" value="{{ $sprowadzone->id }}">
                                            <input type="hidden" name="user_id" value="{{ Auth::user()->id }}">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group label-floating {{ $errors->has('price') ? ' has-error' : '' }}">
                                                        <label class="control-label">Twoja cena</label>
                                                        <input type="number" class="form-control" name="price" value="{{ old('price') }}">

                                                        @if ($errors->has('price'))
                                                            <span class="help-block">
                                                                <strong>{{ $errors->first('price') }}</strong>
                                                            </span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            <button type="submit" class="btn btn-orange">Licytuję</button>
                                            <div class="clearfix"></div>
                                        </form>
                                    </div>
                                </div>

                            </div>
                        </div>
                    @else
                        <div class="card">
                            <div class="card-content">
                                <div class="empty-space"></div>
                                <h4>Musisz być zalogowanym aby licytować!</h4>
                            </div>
                        </div>
                    @endif

                </div>
            </div>
        </div>
    </div>

@endsection