@extends('page.left')

@section('content')

		<img src="images/header.jpg" class="header img-responsive">
    </div>
                
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12 show">
                    <div class="card">
                        <div class="card-header" data-background-color="orange">
                            <h4 class="title">{{ $page->name }}</h4>
                            <p class="category">Czytaj więcej</p>
                        </div>
                        <div class="card-content">
                        <br>
                            {!! $page->description !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
@endsection