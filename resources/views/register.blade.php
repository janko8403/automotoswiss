@extends('page.left')

@section('content')

    @if (Session::has('register_user'))
        <div class="alert-message" role="alert">
            <div class="row">
                <div class="col-md-2">
                    <i class="material-icons">group_add</i>
                </div>
                <div class="col-md-10">
                    <div class="message">
                        {{Session::get('register_user')}}
                    </div>
                </div>
            </div>
        </div>
    @endif

	<div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12 show">
                    <div class="card">
                        <div class="card-header" data-background-color="orange">
                            <h4 class="title">Zarejestruj się</h4>
                            <p class ="category">Uzyskaj dostęp wysyłając nam swojego maila </p>
                        </div>
                        <div class="card-content">
                            <div class="col-md-10 col-md-offset-1">
                                <h4><strong>Napisz do nas w celu uzyskania dostępu</strong></h4>

                                <form role="form" method="POST" action="{{ url('/zarejestruj-sie') }}">
                                        {{ csrf_field() }}
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group label-floating {{ $errors->has('name') ? ' has-error' : '' }}">
                                                    <label class="control-label">Imię i Nazwisko</label>
                                                    <input type="text" class="form-control" name="name" value="{{ old('name') }}">

                                                    @if ($errors->has('name'))
                                                        <span class="help-block">
                                                            <strong>{{ $errors->first('name') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group label-floating {{ $errors->has('email') ? ' has-error' : '' }}">
                                                    <label class="control-label">Email</label>
                                                    <input type="email" class="form-control" name="email" value="{{ old('email') }}">

                                                    @if ($errors->has('email'))
                                                        <span class="help-block">
                                                            <strong>{{ $errors->first('email') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group label-floating {{ $errors->has('phone') ? ' has-error' : '' }}">
                                                    <label class="control-label">Telefon</label>
                                                    <input type="text" class="form-control" name="phone" value="{{ old('phone') }}">

                                                    @if ($errors->has('phone'))
                                                        <span class="help-block">
                                                            <strong>{{ $errors->first('phone') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <button type="submit" class="btn btn-orange pull-left">Wyślij wiadomość</button>
                                        <div class="clearfix"></div>
                                </form>

                            </div>
                            <div class="empty-space"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
@endsection