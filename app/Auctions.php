<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Auctions extends Model
{
    public function users()
    {
    	return $this->belongsTo('App\User') ->withTimestamps();
    }
}
