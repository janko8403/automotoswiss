<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Mail\Mailable;
use Nexmo\Laravel\Facade\Nexmo;
use Session;


class UsersController extends Controller
{

    public function __construct()
    {
        $this->middleware('admin_permission');
    }
    
    public function index()
    {
        $users = User::orderBy('id', 'asc')->get();
        // $regs = Reg::orderBy('id', 'asc')->get();
        return view('cms/users', compact('users'));
    }

    public function create()
    {
        return view('cms/add-user');
    }

     public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required', 'email' => 'required|unique:users', 'password' => 'required|min:6', 'phone' => 'required'], [
            'required' => 'To pole jest wymagane', 'unique' => 'Taki e-mail juz istnieje w bazie', 'min' => 'Musisz podać minimalnie 6 znaków']
        );

        $name = $request['name'];
        $email = $request['email'];
        $password = bcrypt($request['password']);
        $phone = $request['phone'];
        $hash = 'active';
        $role_id = 2;
        
        $user = new User();
        $user->name = $name;
        $user->email = $email;
        $user->password = $password;
        $user->phone = $phone;
        $user->role_id = $role_id;
        $user->hash = $hash;

        $user->save();

        $data = array(
            'subject' => 'Mail ze strony AutoCarSwiss',
            'name' => $name,
            'email' => $email,
            'phone' => $phone,
            'pass' => $request['password']
        );  

        Mail::send('cms/description-client', $data, function($message) use($data) {
            $message->to($data['email']);
            $message->from('automotoswiss0@gmail.com');
            $message->subject($data['subject']);
        });


        Mail::send('cms/description-admin', $data, function($message) use($data) {
            $message->to('autoera@interia.pl');
            $message->from('automotoswiss0@gmail.com');
            $message->subject($data['subject']);
        });

        Session::flash('user_created', 'Użytkownik dodany poprawnie');
        return redirect('cms/user');
    }

    public function edit($id)
    {
        $user = User::findOrFail($id);
        return view('cms/edit-user', compact('user'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required', 'email' => 'required', 'password' => 'required|min:6', 'phone' => 'required'], [
            'required' => 'To pole jest wymagane', 'min' => 'Musisz podać minimalnie 6 znaków']
        );

        // $hash = 'active';

        $user = User::findOrFail($id);
        $user->name = $request->name;
        $user->email = $request->email;
        if (!empty($request->password)) {
            $user->password = bcrypt($request->password);
            $user->hash = 'active';
        }
        $user->phone = $request->phone;

        $user->save();

        $data = array(
            'subject' => 'Mail ze strony AutoCarSwiss',
            'name' => $request->name,
            'email' => $request->email,
            'phone' => $request->phone,
            'pass' => $request['password']
        );  

        Mail::send('cms/description-client', $data, function($message) use($data) {
            $message->to($data['email']);
            $message->from('automotoswiss0@gmail.com');
            $message->subject($data['subject']);
        });


        Mail::send('cms/description-admin', $data, function($message) use($data) {
            $message->to('autoera@interia.pl');
            $message->from('automotoswiss0@gmail.com');
            $message->subject($data['subject']);
        });

        
        return redirect('cms/user');
    }
    
    public function destroy($id)
    {
        $user = User::find($id);
        $user->delete();

        Session::flash('user_destroy', 'Użytkownik usunięty poprawnie');
        return redirect('cms/user');
    }
}
