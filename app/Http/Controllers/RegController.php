<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Mail\Mailable;
use Nexmo\Laravel\Facade\Nexmo;
use Session;


class RegController extends Controller
{
    public function index()
    {
        return view('register');
    }

    
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required', 'email' => 'required|unique:users', 'phone' => 'required|min:9'], [
            'required' => 'To pole jest wymagane', 'unique' => 'Taki e-mail juz istnieje w bazie', 'min' => 'Podaj minimalnie 9 liczb']
        );

        $name = $request['name'];
        $email = $request['email'];
        $phone = $request['phone'];
        $role_id = 2;
        $password = bcrypt('zaq1z');
        $hash = 'zaq1z';
        
        $user = new User();
        $user->name = $name;
        $user->email = $email;
        $user->phone = $phone;
        $user->role_id = $role_id;
        $user->password = $password;
        $user->hash = $hash;

        $user->save();

        $data = array(
            'subject' => 'Chcę uzyskac dostęp do strony!',
            'name' => $name,
            'email' => $email,
            'phone' => $phone 
        );  

        Mail::send('description', $data, function($message) use($data) {
            $message->to('autoera@interia.pl');
            $message->from('automotoswiss0@gmail.com');
            $message->subject($data['subject']);
        });

        Session::flash('register_user', 'Dziękujemy za zgłoszenie! Niebawem dostaniesz maila z dostępami');
        return redirect('/zarejestruj-sie');
    }

    // public function edit($id)
    // {
    //     $user = User::findOrFail($id);
    //     return view('cms/edit-user', compact('user'));
    // }

    // public function update(Request $request, $id)
    // {
    //     $this->validate($request, [
    //         'name' => 'required', 'email' => 'required', 'phone' => 'required'], [
    //         'required' => 'To pole jest wymagane']
    //     );

    //     $user = User::findOrFail($id);
    //     $user->name = $request->name;
    //     $user->email = $request->email;
    //     if (!empty($request->password)) {
    //         $user->password = bcrypt($request->password);
    //     }
    //     $user->phone = $request->phone;

    //     $user->save();

        
    //     return redirect('cms/user');
    // }

    // public function destroy($id)
    // {
    //     $user = User::find($id);
    //     $user->delete();

    //     Session::flash('user_destroy', 'Użytkownik usunięty poprawnie');
    //     return redirect('cms/user');
    // }
}
