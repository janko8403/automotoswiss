<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Page;
use Auth;

class EditPagesController extends Controller
{
	public function __construct()
    {
        $this->middleware('admin_permission');
    }

    public function index()
    {
    	$pages = Page::orderBy('id', 'asc')->get();
    	return view('cms/pages', compact('pages'));
    }

    public function edit($id)
    {
        
        $page = Page::findOrFail($id);
        return view('cms/edit-page', compact('page'));
    }

     public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required'], [
            'required' => 'To pole jest wymagane']
        );

        $page = Page::findOrFail($id);
        $page->name = $request->name;
        $page->description = $request->description;
        
        $page->save();

        return back();
    }

}
