<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;
use App\User;
use App\Auctions;

class CustomersController extends Controller
{
	public function __construct()
    {
        $this->middleware('admin_permission');
    }

    public function customers()
    {
    	$auctions = Auctions::orderBy('id', 'asc')->get();
    	$users = User::orderBy('id', 'asc')->get();
    	return view('/cms/customers', compact('users', 'auctions'));
    }

    public function show($user_id)
    {

        $auctions = DB::table('auctions')
        // ->select(DB::raw('MAX(price) as price'))
        ->select(DB::raw('MAX(price) as price, kto, name, date, auct_id'))
        ->where('user_id', $user_id)
        ->groupBy('auct_id', 'kto', 'name', 'date')
        ->get();

        // $auctions = Auctions::selectRaw('max(price) as price')
        // ->where('user_id', $user_id)
        // ->groupBy('auct_id', 'kto')
        // ->get();

        // dd($auctions);

        // $auctions = Auctions::where('user_id', $user_id)->get();
        $user = User::findOrFail($user_id);
       
        return view('/cms/show-customers', compact('auctions', 'user'));
    }
}
