<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CarAllianz;
use Auth;
use Carbon\Carbon;

class AllianzController extends Controller
{
	public function __construct()
    {
        $this->middleware('admin_user_permission');
    }

    public function index()
    {
        $mytime = Carbon::now();
    	$allianzs = CarAllianz::where('end_date_of_auction', '>', $mytime)->orderBy('end_date_of_auction', 'asc')->paginate(12);
    	return view('/allianz', compact('allianzs'));
    }

    public function show($id)
    {
        $allianz = CarAllianz::findOrFail($id);
        return view('/show-allianz', compact('allianz'));
    }
}
