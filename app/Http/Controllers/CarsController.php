<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Car;
use Session;


class CarsController extends Controller
{

    public function __construct()
    {
        $this->middleware('admin_permission');
    }

    public function index()
    {
        $cars = Car::orderBy('id', 'asc')->get();
        return view('cms/cars', compact('cars'));
    }

   
    public function create()
    {
        return view('cms/add-car');
    }

    
    public function store(Request $request)
    {
        $this->validate($request, [
            'marka' => 'required', 'model'  => 'required', 'version'  => 'required', 'old'  => 'required', 'capacity'  => 'required', 'km' => 'required'], [
            'required' => 'To pole jest wymagane']
        );

        $marka = $request['marka'];
        $model = $request['model'];
        $version = $request['version'];
        $old = $request['old'];
        $capacity = $request['capacity'];
        $km = $request['km'];
        $vin = $request['vin'];
        $power = $request['power'];
        $gas = $request['gas'];
        $transmission = $request['transmission'];
        $drive = $request['drive'];
        $price = $request['price'];
        $color = $request['color'];
        $description = $request['description'];

        
        $car = new Car();
        $car->marka = $marka;
        $car->model = $model;
        $car->version = $version;
        $car->old = $old;
        $car->capacity = $capacity;
        $car->km = $km;
        $car->vin = $vin;
        $car->power = $power;
        $car->gas = $gas;
        $car->transmission = $transmission;
        $car->drive = $drive;
        $car->color = $color; 
        $car->price = $price;
        $car->description = $description;

        if ($request->file('images')) {
            $car_images_path_1 = 'public/car/' . str_slug($request['model'], '-') . '/img';
            $upload_path = $request->file('images')->store($car_images_path_1);
            $images_filename = str_replace($car_images_path_1 . '', '', $upload_path);
            $car->images = $images_filename;
        }

        if ($request->file('images_2')) {
            $car_images_path_2 = 'public/car/' . str_slug($request['model'], '-') . '/img';
            $upload_path = $request->file('images_2')->store($car_images_path_2);
            $images_filename = str_replace($car_images_path_2 . '', '', $upload_path);
            $car->images_2 = $images_filename;
        }

        if ($request->file('images_3')) {
            $car_images_path_3 = 'public/car/' . str_slug($request['model'], '-') . '/img';
            $upload_path = $request->file('images_3')->store($car_images_path_3);
            $images_filename = str_replace($car_images_path_3 . '', '', $upload_path);
            $car->images_3 = $images_filename;
        }

        if ($request->file('images_4')) {
            $car_images_path_4 = 'public/car/' . str_slug($request['model'], '-') . '/img';
            $upload_path = $request->file('images_4')->store($car_images_path_4);
            $images_filename = str_replace($car_images_path_4 . '', '', $upload_path);
            $car->images_4 = $images_filename;
        }

        if ($request->file('images_5')) {
            $car_images_path_5 = 'public/car/' . str_slug($request['model'], '-') . '/img';
            $upload_path = $request->file('images_5')->store($car_images_path_5);
            $images_filename = str_replace($car_images_path_5 . '', '', $upload_path);
            $car->images_5 = $images_filename;
        }

        if ($request->file('images_6')) {
            $car_images_path_6 = 'public/car/' . str_slug($request['model'], '-') . '/img';
            $upload_path = $request->file('images_6')->store($car_images_path_6);
            $images_filename = str_replace($car_images_path_6 . '', '', $upload_path);
            $car->images_6 = $images_filename;
        }

        if ($request->file('images_7')) {
            $car_images_path_7 = 'public/car/' . str_slug($request['model'], '-') . '/img';
            $upload_path = $request->file('images_7')->store($car_images_path_7);
            $images_filename = str_replace($car_images_path_7 . '', '', $upload_path);
            $car->images_7 = $images_filename;
        }

        if ($request->file('images_8')) {
            $car_images_path_8 = 'public/car/' . str_slug($request['model'], '-') . '/img';
            $upload_path = $request->file('images_8')->store($car_images_path_8);
            $images_filename = str_replace($car_images_path_8 . '', '', $upload_path);
            $car->images_8 = $images_filename;
        }

        if ($request->file('images_9')) {
            $car_images_path_9 = 'public/car/' . str_slug($request['model'], '-') . '/img';
            $upload_path = $request->file('images_9')->store($car_images_path_9);
            $images_filename = str_replace($car_images_path_9 . '', '', $upload_path);
            $car->images_9 = $images_filename;
        }

        if ($request->file('images_10')) {
            $car_images_path_10 = 'public/car/' . str_slug($request['model'], '-') . '/img';
            $upload_path = $request->file('images_10')->store($car_images_path_10);
            $images_filename = str_replace($car_images_path_10 . '', '', $upload_path);
            $car->images_10 = $images_filename;
        }

        $car->save();

        Session::flash('car_created', 'Pojazd dodany poprawnie');
        return redirect('cms/car');
    }

    public function edit($id)
    {
        
        $car = Car::findOrFail($id);
        return view('cms/edit-car', compact('car'));
    }

   
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'marka' => 'required', 'model'  => 'required', 'version'  => 'required', 'old'  => 'required', 'capacity'  => 'required', 'km' => 'required'], [
            'required' => 'To pole jest wymagane']
        );

        $car = Car::findOrFail($id);
        $car->marka = $request->marka;
        $car->model = $request->model;
        $car->version = $request->version;
        $car->old = $request->old;
        $car->capacity = $request->capacity;
        $car->km = $request->km;
        $car->vin = $request->vin;
        $car->power = $request->power;
        $car->gas = $request->gas;
        $car->transmission = $request->transmission;
        $car->drive = $request->drive;
        $car->color = $request->color; 
        $car->price = $request->price;
        $car->description = $request->description;
        
        if ($request->file('images')) {
            $car_images_path = 'public/car/' . str_slug($request->model, '-') . '/img';
            $upload_path = $request->file('images')->store($car_images_path);
            $images_filename = str_replace($car_images_path . '', '', $upload_path);
            $car->images = $images_filename;
        }

        if ($request->file('images_2')) {
            $car_images_path = 'public/car/' . str_slug($request['model'], '-') . '/img';
            $upload_path = $request->file('images')->store($car_images_path);
            $images_filename = str_replace($car_images_path . '', '', $upload_path);
            $car->images_2 = $images_filename;
        }

        if ($request->file('images_3')) {
            $car_images_path = 'public/car/' . str_slug($request['model'], '-') . '/img';
            $upload_path = $request->file('images')->store($car_images_path);
            $images_filename = str_replace($car_images_path . '', '', $upload_path);
            $car->images_3 = $images_filename;
        }

        if ($request->file('images_4')) {
            $car_images_path = 'public/car/' . str_slug($request['model'], '-') . '/img';
            $upload_path = $request->file('images')->store($car_images_path);
            $images_filename = str_replace($car_images_path . '', '', $upload_path);
            $car->images_4 = $images_filename;
        }

        if ($request->file('images_5')) {
            $car_images_path = 'public/car/' . str_slug($request['model'], '-') . '/img';
            $upload_path = $request->file('images')->store($car_images_path);
            $images_filename = str_replace($car_images_path . '', '', $upload_path);
            $car->images_5 = $images_filename;
        }

        if ($request->file('images_6')) {
            $car_images_path = 'public/car/' . str_slug($request['model'], '-') . '/img';
            $upload_path = $request->file('images')->store($car_images_path);
            $images_filename = str_replace($car_images_path . '', '', $upload_path);
            $car->images_6 = $images_filename;
        }

        if ($request->file('images_7')) {
            $car_images_path = 'public/car/' . str_slug($request['model'], '-') . '/img';
            $upload_path = $request->file('images')->store($car_images_path);
            $images_filename = str_replace($car_images_path . '', '', $upload_path);
            $car->images_7 = $images_filename;
        }

        if ($request->file('images_8')) {
            $car_images_path = 'public/car/' . str_slug($request['model'], '-') . '/img';
            $upload_path = $request->file('images')->store($car_images_path);
            $images_filename = str_replace($car_images_path . '', '', $upload_path);
            $car->images_8 = $images_filename;
        }

        if ($request->file('images_9')) {
            $car_images_path = 'public/car/' . str_slug($request['model'], '-') . '/img';
            $upload_path = $request->file('images')->store($car_images_path);
            $images_filename = str_replace($car_images_path . '', '', $upload_path);
            $car->images_9 = $images_filename;
        }

        if ($request->file('images_10')) {
            $car_images_path = 'public/car/' . str_slug($request['model'], '-') . '/img';
            $upload_path = $request->file('images')->store($car_images_path);
            $images_filename = str_replace($car_images_path . '', '', $upload_path);
            $car->images_10 = $images_filename;
        }

        $car->save();

        return back();
    }

    
    public function destroy($id)
    {
        $car = Car::find($id);
        $car->delete();

        Session::flash('car_destroy', 'Pojazd usunięty poprawnie');
        return redirect('cms/car');
    }

}
