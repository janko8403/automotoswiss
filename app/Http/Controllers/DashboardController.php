<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
use App\Car;
use App\CarAllianz;
use App\CarRest;
use App\CarScauto;
use App\CarHome;

class DashboardController extends Controller
{

	public function __construct()
    {
        $this->middleware('admin_permission');
    }

    public function dashboard()
    {
        $users = User::where('role_id', 2)->count();
        $cars_0 = Car::orderBy('id', 'asc')->count();
        $cars_1 = CarAllianz::orderBy('id', 'asc')->count();
        $cars_2 = CarRest::orderBy('id', 'asc')->count();
        $cars_3 = CarScauto::orderBy('id', 'asc')->count();
        $cars_4 = CarHome::orderBy('id', 'asc')->count();


        $car = $cars_0 + $cars_1 + $cars_2 + $cars_3 + $cars_4;
                    
        return view('cms/dashboard', compact('users', 'car'));
    }
}
