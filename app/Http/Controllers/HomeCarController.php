<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CarHome;
use Auth;
use Carbon\Carbon;
use DateTimeZone;


class HomeCarController extends Controller
{

	public function index()
	{
		$mytime = Carbon::now(new DateTimeZone('Europe/Berlin'));
		$homes = CarHome::where('end_date_of_auction', '>', $mytime)->orderBy('end_date_of_auction', 'asc')->paginate(12);
    	return view('/axa', compact('homes'));
	}
    public function show($id)
    {
        $home = CarHome::findOrFail($id);
        return view('/show-home', compact('home'));
    }
}
