<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Mail\Mailable;
use Nexmo\Laravel\Facade\Nexmo;
use Session;

class FormController extends Controller
{

	public function index()
    {
        return view('contact');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required', 'email' => 'required'], [
            'required' => 'To pole jest wymagane']
        );

        $name = $request['name'];
        $email = $request['email'];
        $phone = $request['phone'];
        $text = $request['text'];
        
        // $registeruser = new RegisterUser();
        // $registeruser->name = $name;
        // $registeruser->email = $email;
        // $registeruser->phone = $phone;

        // $registeruser->save();

        $data = array(
            'subject' => 'Mail ze strony automotoswiss.pl',
            'name' => $name,
            'email' => $email,
            'phone' => $phone,
            'text' => $text
        );  

        Mail::send('form', $data, function($message) use($data) {
            $message->to('autoera@interia.pl');
            $message->from('automotoswiss0@gmail.com');
            $message->subject($data['subject']);
        });

        // $nexmo = app('Nexmo\Client');

        // $nexmo->message()->send([
        //     'to'   => '48660450916',
        //     'from' => 'AutomotoSwiss',
        //     'text' => 'Mail ze strony automotoswiss.pl',
        // ]);

        Session::flash('send_form', 'Mail wysłany poprawnie');
        return redirect('/kontakt');
    }
}
