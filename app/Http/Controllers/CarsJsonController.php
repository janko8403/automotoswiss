<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CarAllianz;
use App\CarScauto;
use App\CarRest;
use Auth;

class CarsJsonController extends Controller
{
    public function cars_json()
    {
    	$allianzs = CarAllianz::orderBy('id', 'asc')->get();
    	$scautos = CarScauto::orderBy('id', 'asc')->get();
    	$rests = CarRest::orderBy('id', 'asc')->get();
    	return view('/cars_json', compact('allianzs', 'scautos', 'rests'));
    }
}
