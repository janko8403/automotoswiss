<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Classes\swissCars;
// use App\Classes\SwissCarsH;
use Auth;
use App\CarRest;
use App\CarAllianz;
use App\CarScauto;
use App\CarHome;
use Session;

class UpdateController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin_permission');
    }

    public function index()
    {
        $home = CarHome::orderBy('created_at', 'desc')->first();
        $rest = CarRest::orderBy('created_at', 'desc')->first();
        $allianz = CarAllianz::orderBy('created_at', 'desc')->first();
        $scauto = CarScauto::orderBy('created_at', 'desc')->first();
    	return view('cms/update', compact('home', 'rest', 'allianz', 'scauto'));
    }

    public function update_home()
    {

        $swissCarsInit = new SwissCars();

        $swissCarsInit -> checkLogin();
        $arrayWithCars_home = $swissCarsInit -> getCars('home');
    
        foreach ($arrayWithCars_home as $cars_home) {
            if (isset($cars_home["links_to_photos"][0] )) {
                $photos_0 = $cars_home["links_to_photos"][0]; 
            }

            if (isset($cars_home["links_to_photos"][1] )) {
                $photos_1 = $cars_home["links_to_photos"][1]; 
            }
            if (isset($cars_home["links_to_photos"][2] )) {
                $photos_2 = $cars_home["links_to_photos"][2]; 
            }
            if (isset($cars_home["links_to_photos"][3] )) {
                $photos_3 = $cars_home["links_to_photos"][3]; 
            }
            if (isset($cars_home["links_to_photos"][4] )) {
                $photos_4 = $cars_home["links_to_photos"][4]; 
            }
            if (isset($cars_home["links_to_photos"][5] )) {
                $photos_5 = $cars_home["links_to_photos"][5]; 
            }
            if (isset($cars_home["links_to_photos"][6] )) {
                $photos_6 = $cars_home["links_to_photos"][6]; 
            }
            if (isset($cars_home["links_to_photos"][7] )) {
                $photos_7 = $cars_home["links_to_photos"][7]; 
            }
            if (isset($cars_home["links_to_photos"][8] )) {
                $photos_8 = $cars_home["links_to_photos"][8]; 
            }
            if (isset($cars_home["links_to_photos"][9] )) {
                $photos_9 = $cars_home["links_to_photos"][9]; 
            }
            if (isset($cars_home["links_to_photos"][10] )) {
                $photos_10 = $cars_home["links_to_photos"][10]; 
            }
            if (isset($cars_home["links_to_photos"][11] )) {
                $photos_11 = $cars_home["links_to_photos"][11]; 
            }
            if (isset($cars_home["links_to_photos"][12] )) {
                $photos_12 = $cars_home["links_to_photos"][12]; 
            }
            if (isset($cars_home["links_to_photos"][13] )) {
                $photos_13 = $cars_home["links_to_photos"][13]; 
            }
            if (isset($cars_home["links_to_photos"][14] )) {
                $photos_14 = $cars_home["links_to_photos"][14]; 
            }
            if (isset($cars_home["links_to_photos"][15] )) {
                $photos_15 = $cars_home["links_to_photos"][15]; 
            }
            if (isset($cars_home["links_to_photos"][16] )) {
                $photos_16 = $cars_home["links_to_photos"][16]; 
            }
            if (isset($cars_home["links_to_photos"][17] )) {
                $photos_17 = $cars_home["links_to_photos"][17]; 
            }
            if (isset($cars_home["links_to_photos"][18] )) {
                $photos_18 = $cars_home["links_to_photos"][18]; 
            }
            if (isset($cars_home["links_to_photos"][19] )) {
                $photos_19 = $cars_home["links_to_photos"][19]; 
            }
            if (isset($cars_home["links_to_photos"][20] )) {
                $photos_20 = $cars_home["links_to_photos"][20]; 
            }
            if (isset($cars_home["links_to_photos"][21] )) {
                $photos_21 = $cars_home["links_to_photos"][21]; 
            }
            if (isset($cars_home["links_to_photos"][22] )) {
                $photos_22 = $cars_home["links_to_photos"][22]; 
            }
            if (isset($cars_home["links_to_photos"][23] )) {
                $photos_23 = $cars_home["links_to_photos"][23]; 
            }
            if (isset($cars_home["links_to_photos"][24] )) {
                $photos_24 = $cars_home["links_to_photos"][24]; 
            }
            if (isset($cars_home["links_to_photos"][25] )) {
                $photos_25 = $cars_home["links_to_photos"][25]; 
            }
            if (isset($cars_home["links_to_photos"][26] )) {
                $photos_26 = $cars_home["links_to_photos"][26]; 
            }
            if (isset($cars_home["links_to_photos"][27] )) {
                $photos_27 = $cars_home["links_to_photos"][27]; 
            }
            if (isset($cars_home["links_to_photos"][28] )) {
                $photos_28 = $cars_home["links_to_photos"][28]; 
            }
            if (isset($cars_home["links_to_photos"][29] )) {
                $photos_29 = $cars_home["links_to_photos"][29]; 
            }
            if (isset($cars_home["links_to_photos"][30] )) {
                $photos_30 = $cars_home["links_to_photos"][30]; 
            }

            if (isset($cars_home["description"])) {
                $description = $cars_home["description"]; 
            }

            if (isset($cars_home["end_date_of_auction"])) {
                $end_date_of_auction = $cars_home["end_date_of_auction"]; 
            }

            if (isset($cars_home["carName"])) {
                $car_name = $cars_home["carName"]; 
            }


            if (isset($cars_home["originalAuctionLink"])) {
                $linkoryginal = $cars_home["originalAuctionLink"]; 
            }

            $car_home = CarHome::updateOrCreate(['photos_0' => $photos_0]);
        
            $car_home->operator = 'Home';

            if (isset($photos_0)) {
                $car_home->photos_0 = $photos_0;
            }

            if (isset($photos_1)) {
                $car_home->photos_1 = $photos_1;
            }

            if (isset($photos_2)) {
                $car_home->photos_2 = $photos_2;
            }

            if (isset($photos_3)) {
                $car_home->photos_3 = $photos_3;
            }

            if (isset($photos_4)) {
                $car_home->photos_4 = $photos_4;
            }

            if (isset($photos_5)) {
                $car_home->photos_5 = $photos_5;
            }
            if (isset($photos_6)) {
                $car_home->photos_6 = $photos_6;
            }
            if (isset($photos_7)) {
                $car_home->photos_7 = $photos_7;
            }
            if (isset($photos_8)) {
                $car_home->photos_8 = $photos_8;
            }
            if (isset($photos_9)) {
                $car_home->photos_9 = $photos_9;
            }
            if (isset($photos_10)) {
                $car_home->photos_10 = $photos_10;
            }
            if (isset($photos_11)) {
                $car_home->photos_11 = $photos_11;
            }
            if (isset($photos_12)) {
                $car_home->photos_12 = $photos_12;
            }
            if (isset($photos_13)) {
                $car_home->photos_13 = $photos_13;
            }
            if (isset($photos_14)) {
                $car_home->photos_14 = $photos_14;
            }
            if (isset($photos_15)) {
                $car_home->photos_15 = $photos_15;
            }
            if (isset($photos_16)) {
                $car_home->photos_16 = $photos_16;
            }
            if (isset($photos_17)) {
                $car_home->photos_17 = $photos_17;
            }
            if (isset($photos_18)) {
                $car_home->photos_18 = $photos_18;
            }
            if (isset($photos_19)) {
                $car_home->photos_19 = $photos_19;
            }
            if (isset($photos_20)) {
                $car_home->photos_20 = $photos_20;
            }
            if (isset($photos_21)) {
                $car_home->photos_21 = $photos_21;
            }
            if (isset($photos_22)) {
                $car_home->photos_22 = $photos_22;
            }
            if (isset($photos_23)) {
                $car_home->photos_23 = $photos_23;
            }
            if (isset($photos_24)) {
                $car_home->photos_24 = $photos_24;
            }
            if (isset($photos_25)) {
                $car_home->photos_25 = $photos_25;
            }
            if (isset($photos_26)) {
                $car_home->photos_26 = $photos_26;
            }
            if (isset($photos_27)) {
                $car_home->photos_27 = $photos_27;
            }
            if (isset($photos_28)) {
                $car_home->photos_28 = $photos_28;
            }
            if (isset($photos_29)) {
                $car_home->photos_29 = $photos_29;
            }
            if (isset($photos_30)) {
                $car_home->photos_30 = $photos_30;
            }

            if (isset($description)) {
                $car_home->description = $description;
            }

            if (isset($end_date_of_auction)) {
                $car_home->end_date_of_auction = $end_date_of_auction;
            }

            if (isset($car_home_name)) {
                $car_home->car_name = $car_home_name;
            }

            if (isset($linkoryginal)) {
                $car_home->originalauctionlink = $linkoryginal;
            }

            $car_home ->save();
        }

        Session::flash('car_updated_home', 'Pojazdy HOME zaktualizowane poprawnie');
        return redirect('cms/update');
    }

    public function update_rest()
    {

        $swissCarsInit = new SwissCars();

        $swissCarsInit -> checkLogin();
        $arrayWithCars_rest = $swissCarsInit -> getCars('rest');
    
        foreach ($arrayWithCars_rest as $cars_rest) {
            if (isset($cars_rest["links_to_photos"][0] )) {
                $photos_0 = $cars_rest["links_to_photos"][0]; 
            }

            if (isset($cars_rest["links_to_photos"][1] )) {
                $photos_1 = $cars_rest["links_to_photos"][1]; 
            }
            if (isset($cars_rest["links_to_photos"][2] )) {
                $photos_2 = $cars_rest["links_to_photos"][2]; 
            }
            if (isset($cars_rest["links_to_photos"][3] )) {
                $photos_3 = $cars_rest["links_to_photos"][3]; 
            }
            if (isset($cars_rest["links_to_photos"][4] )) {
                $photos_4 = $cars_rest["links_to_photos"][4]; 
            }
            if (isset($cars_rest["links_to_photos"][5] )) {
                $photos_5 = $cars_rest["links_to_photos"][5]; 
            }
            if (isset($cars_rest["links_to_photos"][6] )) {
                $photos_6 = $cars_rest["links_to_photos"][6]; 
            }
            if (isset($cars_rest["links_to_photos"][7] )) {
                $photos_7 = $cars_rest["links_to_photos"][7]; 
            }
            if (isset($cars_rest["links_to_photos"][8] )) {
                $photos_8 = $cars_rest["links_to_photos"][8]; 
            }
            if (isset($cars_rest["links_to_photos"][9] )) {
                $photos_9 = $cars_rest["links_to_photos"][9]; 
            }
            if (isset($cars_rest["links_to_photos"][10] )) {
                $photos_10 = $cars_rest["links_to_photos"][10]; 
            }
            if (isset($cars_rest["links_to_photos"][11] )) {
                $photos_11 = $cars_rest["links_to_photos"][11]; 
            }
            if (isset($cars_rest["links_to_photos"][12] )) {
                $photos_12 = $cars_rest["links_to_photos"][12]; 
            }
            if (isset($cars_rest["links_to_photos"][13] )) {
                $photos_13 = $cars_rest["links_to_photos"][13]; 
            }
            if (isset($cars_rest["links_to_photos"][14] )) {
                $photos_14 = $cars_rest["links_to_photos"][14]; 
            }
            if (isset($cars_rest["links_to_photos"][15] )) {
                $photos_15 = $cars_rest["links_to_photos"][15]; 
            }
            if (isset($cars_rest["links_to_photos"][16] )) {
                $photos_16 = $cars_rest["links_to_photos"][16]; 
            }
            if (isset($cars_rest["links_to_photos"][17] )) {
                $photos_17 = $cars_rest["links_to_photos"][17]; 
            }
            if (isset($cars_rest["links_to_photos"][18] )) {
                $photos_18 = $cars_rest["links_to_photos"][18]; 
            }
            if (isset($cars_rest["links_to_photos"][19] )) {
                $photos_19 = $cars_rest["links_to_photos"][19]; 
            }
            if (isset($cars_rest["links_to_photos"][20] )) {
                $photos_20 = $cars_rest["links_to_photos"][20]; 
            }
            if (isset($cars_rest["links_to_photos"][21] )) {
                $photos_21 = $cars_rest["links_to_photos"][21]; 
            }
            if (isset($cars_rest["links_to_photos"][22] )) {
                $photos_22 = $cars_rest["links_to_photos"][22]; 
            }
            if (isset($cars_rest["links_to_photos"][23] )) {
                $photos_23 = $cars_rest["links_to_photos"][23]; 
            }
            if (isset($cars_rest["links_to_photos"][24] )) {
                $photos_24 = $cars_rest["links_to_photos"][24]; 
            }
            if (isset($cars_rest["links_to_photos"][25] )) {
                $photos_25 = $cars_rest["links_to_photos"][25]; 
            }
            if (isset($cars_rest["links_to_photos"][26] )) {
                $photos_26 = $cars_rest["links_to_photos"][26]; 
            }
            if (isset($cars_rest["links_to_photos"][27] )) {
                $photos_27 = $cars_rest["links_to_photos"][27]; 
            }
            if (isset($cars_rest["links_to_photos"][28] )) {
                $photos_28 = $cars_rest["links_to_photos"][28]; 
            }
            if (isset($cars_rest["links_to_photos"][29] )) {
                $photos_29 = $cars_rest["links_to_photos"][29]; 
            }
            if (isset($cars_rest["links_to_photos"][30] )) {
                $photos_30 = $cars_rest["links_to_photos"][30]; 
            }

            if (isset($cars_rest["description"]["Marka"])) {
                $marka = $cars_rest["description"]["Marka"]; 
            }

            if (isset($cars_rest["description"]["Model"])) {
                $model = $cars_rest["description"]["Model"]; 
            }

            if (isset($cars_rest["description"]["Podwozie"])) {
                $podwozie = $cars_rest["description"]["Podwozie"]; 
            }

            if (isset($cars_rest["description"]["Pierwsza rejestracja"])) {
                $pierwsza_rejestracja = $cars_rest["description"]["Pierwsza rejestracja"]; 
            }

            if (isset($cars_rest["description"]["Kolor"])) {
                $kolor = $cars_rest["description"]["Kolor"]; 
            }

            if (isset($cars_rest["description"]["Stan"])) {
                $stan = $cars_rest["description"]["Stan"]; 
            }

            if (isset($cars_rest["description"]["Opony"])) {
                $opony = $cars_rest["description"]["Opony"]; 
            }

            if (isset($cars_rest["description"]["Konstrukcja"])) {
                $konstrukcja = $cars_rest["description"]["Konstrukcja"]; 
            }

            if (isset($cars_rest["description"]["Dowod wlasnosci"])) {
                $dowod_wlasnosci = $cars_rest["description"]["Dowod wlasnosci"]; 
            }

            if (isset($cars_rest["description"]["Przebieg"])) {
                $przebieg = $cars_rest["description"]["Przebieg"]; 
            }

            if (isset($cars_rest["description"]["MFK"])) {
                $mfk = $cars_rest["description"]["MFK"]; 
            }

            if (isset($cars_rest["description"]["Wyposazenie"])) {
                $wyposazenie = $cars_rest["description"]["Wyposazenie"]; 
            }

            if (isset($cars_rest["description"]["Opis uszkodzen"])) {
                $opis_uszkodzenia = $cars_rest["description"]["Opis uszkodzen"]; 
            }

            if (isset($cars_rest["description"]["Strefa obrazen"])) {
                $strefa_obrazen = $cars_rest["description"]["Strefa obrazen"]; 
            }

            if (isset($cars_rest["description"]["Wystrzelone poduszki"])) {
                $wystrzelone_poduszki = $cars_rest["description"]["Wystrzelone poduszki"]; 
            }

            if (isset($cars_rest["description"]["Koniec"])) {
                $koniec = $cars_rest["description"]["Koniec"]; 
            }

            if (isset($cars_rest["end_date_of_auction"])) {
                $end_date_of_auction = $cars_rest["end_date_of_auction"]; 
            }

            $car_rest = CarRest::updateOrCreate(['photos_0' => $photos_0]);
        
			$car_rest->operator = 'Rest';

            if (isset($photos_0)) {
			     $car_rest->photos_0 = $photos_0;
            }

            if (isset($photos_1)) {
			     $car_rest->photos_1 = $photos_1;
            }

            if (isset($photos_2)) {
			     $car_rest->photos_2 = $photos_2;
            }

            if (isset($photos_3)) {
			     $car_rest->photos_3 = $photos_3;
            }

            if (isset($photos_4)) {
			     $car_rest->photos_4 = $photos_4;
            }

            if (isset($photos_5)) {
			     $car_rest->photos_5 = $photos_5;
            }
            if (isset($photos_6)) {
                $car_rest->photos_6 = $photos_6;
            }
            if (isset($photos_7)) {
                $car_rest->photos_7 = $photos_7;
            }
            if (isset($photos_8)) {
                $car_rest->photos_8 = $photos_8;
            }
            if (isset($photos_9)) {
                $car_rest->photos_9 = $photos_9;
            }
            if (isset($photos_10)) {
                $car_rest->photos_10 = $photos_10;
            }
            if (isset($photos_11)) {
                $car_rest->photos_11 = $photos_11;
            }
            if (isset($photos_12)) {
                $car_rest->photos_12 = $photos_12;
            }
            if (isset($photos_13)) {
                $car_rest->photos_13 = $photos_13;
            }
            if (isset($photos_14)) {
                $car_rest->photos_14 = $photos_14;
            }
            if (isset($photos_15)) {
                $car_rest->photos_15 = $photos_15;
            }
            if (isset($photos_16)) {
                $car_rest->photos_16 = $photos_16;
            }
            if (isset($photos_17)) {
                $car_rest->photos_17 = $photos_17;
            }
            if (isset($photos_18)) {
                $car_rest->photos_18 = $photos_18;
            }
            if (isset($photos_19)) {
                $car_rest->photos_19 = $photos_19;
            }
            if (isset($photos_20)) {
                $car_rest->photos_20 = $photos_20;
            }
            if (isset($photos_21)) {
                $car_rest->photos_21 = $photos_21;
            }
            if (isset($photos_22)) {
                $car_rest->photos_22 = $photos_22;
            }
            if (isset($photos_23)) {
                $car_rest->photos_23 = $photos_23;
            }
            if (isset($photos_24)) {
                $car_rest->photos_24 = $photos_24;
            }
            if (isset($photos_25)) {
                $car_rest->photos_25 = $photos_25;
            }
            if (isset($photos_26)) {
                $car_rest->photos_26 = $photos_26;
            }
            if (isset($photos_27)) {
                $car_rest->photos_27 = $photos_27;
            }
            if (isset($photos_28)) {
                $car_rest->photos_28 = $photos_28;
            }
            if (isset($photos_29)) {
                $car_rest->photos_29 = $photos_29;
            }
            if (isset($photos_30)) {
                $car_rest->photos_30 = $photos_30;
            }

            if (isset($marka)) {
			     $car_rest->marka = $marka;
            }

            if (isset($podwozie)) {
			     $car_rest->podwozie = $podwozie;
            }

            if (isset($model)) {
			     $car_rest->model = $model;
            }

            if (isset($opony)) {
			     $car_rest->opony = $opony;
            }

            if (isset($konstrukcja)) {
			     $car_rest->konstrukcja = $konstrukcja;
            }

            if (isset($pierwsza_rejestracja)) {
			     $car_rest->pierwsza_rejestracja = $pierwsza_rejestracja;
            }

            if (isset($mfk)) {
                $car_rest->mfk = $mfk;
            }

            if (isset($stan)) {
                $car_rest->stan = $stan;
            }

            if (isset($przebieg)) {
			     $car_rest->przebieg = $przebieg;
            }

            if (isset($dowod_wlasnosci)) {
			     $car_rest->dowod_wlasnosci = $dowod_wlasnosci;
            }

            if (isset($wyposazenie)) {
			     $car_rest->wyposazenie = $wyposazenie;
            }

            if (isset($opis_uszkodzenia)) {
			     $car_rest->opis_uszkodzenia = $opis_uszkodzenia;
            }

            if (isset($wystrzelone_poduszki)) {
                $car_rest->wystrzelone_poduszki = $wystrzelone_poduszki;
            }

            if (isset($strefa_obrazen)) {
		  	   $car_rest->strefa_obrazen = $strefa_obrazen;
            }

            if (isset($koniec)) {
			     $car_rest->koniec = $koniec;
            }

            if (isset($end_date_of_auction)) {
			     $car_rest->end_date_of_auction = $end_date_of_auction;
            }
            $car_rest ->save();
        }

    	Session::flash('car_updated_rest', 'Pojazdy REST zaktualizowane poprawnie');
        return redirect('cms/update');
    }

    public function update_allianz()
    {

        $swissCarsInit = new SwissCars();

        $swissCarsInit -> checkLogin();
        $arrayWithCars_allianz = $swissCarsInit -> getCars('zabezpieczone-tresci');
    
        foreach ($arrayWithCars_allianz as $cars_allianz) {
            if (isset($cars_allianz["links_to_photos"][0] )) {
                $photos_0 = $cars_allianz["links_to_photos"][0]; 
            }

            if (isset($cars_allianz["links_to_photos"][1] )) {
                $photos_1 = $cars_allianz["links_to_photos"][1]; 
            }
            if (isset($cars_allianz["links_to_photos"][2] )) {
                $photos_2 = $cars_allianz["links_to_photos"][2]; 
            }
            if (isset($cars_allianz["links_to_photos"][3] )) {
                $photos_3 = $cars_allianz["links_to_photos"][3]; 
            }
            if (isset($cars_allianz["links_to_photos"][4] )) {
                $photos_4 = $cars_allianz["links_to_photos"][4]; 
            }
            if (isset($cars_allianz["links_to_photos"][5] )) {
                $photos_5 = $cars_allianz["links_to_photos"][5]; 
            }
            if (isset($cars_allianz["links_to_photos"][6] )) {
                $photos_6 = $cars_allianz["links_to_photos"][6]; 
            }
            if (isset($cars_allianz["links_to_photos"][7] )) {
                $photos_7 = $cars_allianz["links_to_photos"][7]; 
            }
            if (isset($cars_allianz["links_to_photos"][8] )) {
                $photos_8 = $cars_allianz["links_to_photos"][8]; 
            }
            if (isset($cars_allianz["links_to_photos"][9] )) {
                $photos_9 = $cars_allianz["links_to_photos"][9]; 
            }
            if (isset($cars_allianz["links_to_photos"][10] )) {
                $photos_10 = $cars_allianz["links_to_photos"][10]; 
            }
            if (isset($cars_allianz["links_to_photos"][11] )) {
                $photos_11 = $cars_allianz["links_to_photos"][11]; 
            }
            if (isset($cars_allianz["links_to_photos"][12] )) {
                $photos_12 = $cars_allianz["links_to_photos"][12]; 
            }
            if (isset($cars_allianz["links_to_photos"][13] )) {
                $photos_13 = $cars_allianz["links_to_photos"][13]; 
            }
            if (isset($cars_allianz["links_to_photos"][14] )) {
                $photos_14 = $cars_allianz["links_to_photos"][14]; 
            }
            if (isset($cars_allianz["links_to_photos"][15] )) {
                $photos_15 = $cars_allianz["links_to_photos"][15]; 
            }
            if (isset($cars_allianz["links_to_photos"][16] )) {
                $photos_16 = $cars_allianz["links_to_photos"][16]; 
            }
            if (isset($cars_allianz["links_to_photos"][17] )) {
                $photos_17 = $cars_allianz["links_to_photos"][17]; 
            }
            if (isset($cars_allianz["links_to_photos"][18] )) {
                $photos_18 = $cars_allianz["links_to_photos"][18]; 
            }
            if (isset($cars_allianz["links_to_photos"][19] )) {
                $photos_19 = $cars_allianz["links_to_photos"][19]; 
            }
            if (isset($cars_allianz["links_to_photos"][20] )) {
                $photos_20 = $cars_allianz["links_to_photos"][20]; 
            }
            if (isset($cars_allianz["links_to_photos"][21] )) {
                $photos_21 = $cars_allianz["links_to_photos"][21]; 
            }
            if (isset($cars_allianz["links_to_photos"][22] )) {
                $photos_22 = $cars_allianz["links_to_photos"][22]; 
            }
            if (isset($cars_allianz["links_to_photos"][23] )) {
                $photos_23 = $cars_allianz["links_to_photos"][23]; 
            }
            if (isset($cars_allianz["links_to_photos"][24] )) {
                $photos_24 = $cars_allianz["links_to_photos"][24]; 
            }
            if (isset($cars_allianz["links_to_photos"][25] )) {
                $photos_25 = $cars_allianz["links_to_photos"][25]; 
            }
            if (isset($cars_allianz["links_to_photos"][26] )) {
                $photos_26 = $cars_allianz["links_to_photos"][26]; 
            }
            if (isset($cars_allianz["links_to_photos"][27] )) {
                $photos_27 = $cars_allianz["links_to_photos"][27]; 
            }
            if (isset($cars_allianz["links_to_photos"][28] )) {
                $photos_28 = $cars_allianz["links_to_photos"][28]; 
            }
            if (isset($cars_allianz["links_to_photos"][29] )) {
                $photos_29 = $cars_allianz["links_to_photos"][29]; 
            }
            if (isset($cars_allianz["links_to_photos"][30] )) {
                $photos_30 = $cars_allianz["links_to_photos"][30]; 
            }

            if (isset($cars_allianz["description"]["Marka"])) {
                $marka = $cars_allianz["description"]["Marka"]; 
            }
            if (isset($cars_allianz["description"]["Typ"])) {  
                $typ = $cars_allianz["description"]["Typ"]; 
            }
            if (isset($cars_allianz["description"]["Model"])) {
                $model = $cars_allianz["description"]["Model"]; 
            }
            if (isset($cars_allianz["description"]["Nowy"])) {
                $nowy = $cars_allianz["description"]["Nowy"]; 
            }
            if (isset($cars_allianz["description"]["Kolor"])) {
                $kolor = $cars_allianz["description"]["Kolor"]; 
            }
            if (isset($cars_allianz["description"]["VIN"])) {
                $vin = $cars_allianz["description"]["VIN"]; 
            }
            if (isset($cars_allianz["description"]["MFK"])) {
                $mfk = $cars_allianz["description"]["MFK"]; 
            }
            if (isset($cars_allianz["description"]["Pierwsza rejestracja"])) {
                $pierwsza_rejestracja = $cars_allianz["description"]["Pierwsza rejestracja"]; 
            }
            if (isset($cars_allianz["description"]["Przebieg"])) {
                $przebieg = $cars_allianz["description"]["Przebieg"]; 
            }
            if (isset($cars_allianz["description"]["Dowod wlasnosci"])) {
                $dowod_wlasnosci = $cars_allianz["description"]["Dowod wlasnosci"]; 
            }
            if (isset($cars_allianz["description"]["Stan"])) {
                $stan = $cars_allianz["description"]["Stan"];  
            }
           
            if (isset($cars_allianz["description"]["Opis uszkodzenia"])) {
                $opis_uszkodzenia = $cars_allianz["description"]["Opis uszkodzenia"]; 
            }
            if (isset($cars_allianz["description"]["Typ uszkodzenia"])) {
                $typ_uszkodzenia = $cars_allianz["description"]["Typ uszkodzenia"];  
            }
            if (isset($cars_allianz["description"]["Poczatek"])) {
                $poczatek = $cars_allianz["description"]["Poczatek"];  
            }

            if (isset($cars_allianz["description"]["Wczesniejsze uszkodzenia"])) {
                $wczesniejsze_uszkodzenia = $cars_allianz["description"]["Wczesniejsze uszkodzenia"];  
            }

            if (isset($cars_allianz["description"]["Lokalizacja uszkodzenia"])) {
                $lokalizacja_uszkodzenia = $cars_allianz["description"]["Lokalizacja uszkodzenia"];  
            }
            
            if (isset($cars_allianz["end_date_of_auction"])) {
                $end_date_of_auction = $cars_allianz["end_date_of_auction"]; 
            }

            $car_allianz = CarAllianz::updateOrCreate(['photos_0' => $photos_0]);
        
			$car_allianz->operator = 'Allianz';

            if (isset($photos_0)) {
			     $car_allianz->photos_0 = $photos_0;
            }

            if (isset($photos_1)) {
			     $car_allianz->photos_1 = $photos_1;
            }

            if (isset($photos_2)) {
			     $car_allianz->photos_2 = $photos_2;
            }

            if (isset($photos_3)) {
			     $car_allianz->photos_3 = $photos_3;
            }

            if (isset($photos_4)) {
			     $car_allianz->photos_4 = $photos_4;
            }

            if (isset($photos_5)) {
			     $car_allianz->photos_5 = $photos_5;
            }
            if (isset($photos_6)) {
                $car_allianz->photos_6 = $photos_6;
            }
            if (isset($photos_7)) {
                $car_allianz->photos_7 = $photos_7;
            }
            if (isset($photos_8)) {
                $car_allianz->photos_8 = $photos_8;
            }
            if (isset($photos_9)) {
                $car_allianz->photos_9 = $photos_9;
            }
            if (isset($photos_10)) {
                $car_allianz->photos_10 = $photos_10;
            }
            if (isset($photos_11)) {
                $car_allianz->photos_11 = $photos_11;
            }
            if (isset($photos_12)) {
                $car_allianz->photos_12 = $photos_12;
            }
            if (isset($photos_13)) {
                $car_allianz->photos_13 = $photos_13;
            }
            if (isset($photos_14)) {
                $car_allianz->photos_14 = $photos_14;
            }
            if (isset($photos_15)) {
                $car_allianz->photos_15 = $photos_15;
            }
            if (isset($photos_16)) {
                $car_allianz->photos_16 = $photos_16;
            }
            if (isset($photos_17)) {
                $car_allianz->photos_17 = $photos_17;
            }
            if (isset($photos_18)) {
                $car_allianz->photos_18 = $photos_18;
            }
            if (isset($photos_19)) {
                $car_allianz->photos_19 = $photos_19;
            }
            if (isset($photos_20)) {
                $car_allianz->photos_20 = $photos_20;
            }
            if (isset($photos_21)) {
                $car_allianz->photos_21 = $photos_21;
            }
            if (isset($photos_22)) {
                $car_allianz->photos_22 = $photos_22;
            }
            if (isset($photos_23)) {
                $car_allianz->photos_23 = $photos_23;
            }
            if (isset($photos_24)) {
                $car_allianz->photos_24 = $photos_24;
            }
            if (isset($photos_25)) {
                $car_allianz->photos_25 = $photos_25;
            }
            if (isset($photos_26)) {
                $car_allianz->photos_26 = $photos_26;
            }
            if (isset($photos_27)) {
                $car_allianz->photos_27 = $photos_27;
            }
            if (isset($photos_28)) {
                $car_allianz->photos_28 = $photos_28;
            }
            if (isset($photos_29)) {
                $car_allianz->photos_29 = $photos_29;
            }
            if (isset($photos_30)) {
                $car_allianz->photos_30 = $photos_30;
            }

            if (isset($marka)) {
			     $car_allianz->marka = $marka;
            }

            if (isset($typ)) {
                $car_allianz->typ = $typ;
            }

            if (isset($przebieg)) {
			     $car_allianz->przebieg = $przebieg;
            }

            if (isset($model)) {
			     $car_allianz->model = $model;
            }

            if (isset($nowy)) {
			     $car_allianz->nowy = $nowy;
            }

            if (isset($kolor)) {
			     $car_allianz->kolor = $kolor;
            }

            if (isset($pierwsza_rejestracja)) {
			     $car_allianz->pierwsza_rejestracja = $pierwsza_rejestracja;
            }

            if (isset($vin)) {
                $car_allianz->vin = $vin;
            }

            if (isset($mfk)) {
                $car_allianz->mfk = $mfk;
            }

            if (isset($stan)) {
			     $car_allianz->stan = $stan;
            }

            if (isset($dowod_wlasnosci)) {
			     $car_allianz->dowod_wlasnosci = $dowod_wlasnosci;
            }

		    if (isset($opis_uszkodzenia)) {
			     $car_allianz->opis_uszkodzenia = $opis_uszkodzenia;
            }

            if (isset($typ_uszkodzenia)) {
                $car_allianz->typ_uszkodzenia = $typ_uszkodzenia;
            }

            if (isset($wczesniejsze_uszkodzenia)) {
                $car_allianz->wczesniejsze_uszkodzenia = $wczesniejsze_uszkodzenia;
            }

            if (isset($lokalizacja_uszkodzenia)) {
                $car_allianz->lokalizacja_uszkodzenia = $lokalizacja_uszkodzenia;
            }
			
            if (isset($end_date_of_auction)) {
			     $car_allianz->end_date_of_auction = $end_date_of_auction;
            }
			
            $car_allianz ->save();
        }

    	Session::flash('car_updated_allianz', 'Pojazdy ALLIANZ zaktualizowane poprawnie');
        return redirect('cms/update');
    }

    public function update_scauto()
    {

        $swissCarsInit = new SwissCars();

        $swissCarsInit -> checkLogin();
        $arrayWithCars_scauto = $swissCarsInit -> getCars('scauto');
    
        foreach ($arrayWithCars_scauto as $cars_scauto) {
            if (isset($cars_scauto["links_to_photos"][0] )) {
                $photos_0 = $cars_scauto["links_to_photos"][0]; 
            }

            if (isset($cars_scauto["links_to_photos"][1] )) {
                $photos_1 = $cars_scauto["links_to_photos"][1]; 
            }
            if (isset($cars_scauto["links_to_photos"][2] )) {
                $photos_2 = $cars_scauto["links_to_photos"][2]; 
            }
            if (isset($cars_scauto["links_to_photos"][3] )) {
                $photos_3 = $cars_scauto["links_to_photos"][3]; 
            }
            if (isset($cars_scauto["links_to_photos"][4] )) {
                $photos_4 = $cars_scauto["links_to_photos"][4]; 
            }
            if (isset($cars_scauto["links_to_photos"][5] )) {
                $photos_5 = $cars_scauto["links_to_photos"][5]; 
            }
            if (isset($cars_scauto["links_to_photos"][6] )) {
                $photos_6 = $cars_scauto["links_to_photos"][6]; 
            }
            if (isset($cars_scauto["links_to_photos"][7] )) {
                $photos_7 = $cars_scauto["links_to_photos"][7]; 
            }
            if (isset($cars_scauto["links_to_photos"][8] )) {
                $photos_8 = $cars_scauto["links_to_photos"][8]; 
            }
            if (isset($cars_scauto["links_to_photos"][9] )) {
                $photos_9 = $cars_scauto["links_to_photos"][9]; 
            }
            if (isset($cars_scauto["links_to_photos"][10] )) {
                $photos_10 = $cars_scauto["links_to_photos"][10]; 
            }
            if (isset($cars_scauto["links_to_photos"][11] )) {
                $photos_11 = $cars_scauto["links_to_photos"][11]; 
            }
            if (isset($cars_scauto["links_to_photos"][12] )) {
                $photos_12 = $cars_scauto["links_to_photos"][12]; 
            }
            if (isset($cars_scauto["links_to_photos"][13] )) {
                $photos_13 = $cars_scauto["links_to_photos"][13]; 
            }
            if (isset($cars_scauto["links_to_photos"][14] )) {
                $photos_14 = $cars_scauto["links_to_photos"][14]; 
            }
            if (isset($cars_scauto["links_to_photos"][15] )) {
                $photos_15 = $cars_scauto["links_to_photos"][15]; 
            }
            if (isset($cars_scauto["links_to_photos"][16] )) {
                $photos_16 = $cars_scauto["links_to_photos"][16]; 
            }
            if (isset($cars_scauto["links_to_photos"][17] )) {
                $photos_17 = $cars_scauto["links_to_photos"][17]; 
            }
            if (isset($cars_scauto["links_to_photos"][18] )) {
                $photos_18 = $cars_scauto["links_to_photos"][18]; 
            }
            if (isset($cars_scauto["links_to_photos"][19] )) {
                $photos_19 = $cars_scauto["links_to_photos"][19]; 
            }
            if (isset($cars_scauto["links_to_photos"][20] )) {
                $photos_20 = $cars_scauto["links_to_photos"][20]; 
            }
            if (isset($cars_scauto["links_to_photos"][21] )) {
                $photos_21 = $cars_scauto["links_to_photos"][21]; 
            }
            if (isset($cars_scauto["links_to_photos"][22] )) {
                $photos_22 = $cars_scauto["links_to_photos"][22]; 
            }
            if (isset($cars_scauto["links_to_photos"][23] )) {
                $photos_23 = $cars_scauto["links_to_photos"][23]; 
            }
            if (isset($cars_scauto["links_to_photos"][24] )) {
                $photos_24 = $cars_scauto["links_to_photos"][24]; 
            }
            if (isset($cars_scauto["links_to_photos"][25] )) {
                $photos_25 = $cars_scauto["links_to_photos"][25]; 
            }
            if (isset($cars_scauto["links_to_photos"][26] )) {
                $photos_26 = $cars_scauto["links_to_photos"][26]; 
            }
            if (isset($cars_scauto["links_to_photos"][27] )) {
                $photos_27 = $cars_scauto["links_to_photos"][27]; 
            }
            if (isset($cars_scauto["links_to_photos"][28] )) {
                $photos_28 = $cars_scauto["links_to_photos"][28]; 
            }
            if (isset($cars_scauto["links_to_photos"][29] )) {
                $photos_29 = $cars_scauto["links_to_photos"][29]; 
            }
            if (isset($cars_scauto["links_to_photos"][30] )) {
                $photos_30 = $cars_scauto["links_to_photos"][30]; 
            }

            if (isset($cars_scauto["description"]["Marka"])) {
                $marka = $cars_scauto["description"]["Marka"]; 
            }

            if (isset($cars_scauto["description2"])) {
                $description2 = $cars_scauto["description2"]; 
            }

            if (isset($cars_scauto["description"]["Typ"])) {
	           $typ = $cars_scauto["description"]["Typ"]; 
            }

            if (isset($cars_scauto["description"]["Model"])) {
	           $model = $cars_scauto["description"]["Model"]; 
            }

            if (isset($cars_scauto["description"]["Waga"])) {
	           $waga = $cars_scauto["description"]["Waga"]; 
            }

            if (isset($cars_scauto["description"]["Typ pojazdu"])) {
	           $typ_pojazdu = $cars_scauto["description"]["Typ pojazdu"]; 
            }

            if (isset($cars_scauto["description"]["Typ silnika"])) {
	           $typ_silnika = $cars_scauto["description"]["Typ silnika"]; 
            }

            if (isset($cars_scauto["description"]["Skrzynia biegów"])) {
	           $skrzynia_biegow = $cars_scauto["description"]["Skrzynia biegów"]; 
            }

            if (isset($cars_scauto["description"]["Napęd"])) {
	           $naped = $cars_scauto["description"]["Napęd"]; 
            }

            if (isset($cars_scauto["description"]["Moc"])) {
	           $moc = $cars_scauto["description"]["Moc"]; 
            }

            if (isset($cars_scauto["description"]["Pojemność silnika"])) {
	           $pojemnosc_silnika = $cars_scauto["description"]["Pojemność silnika"]; 
            }

            if (isset($cars_scauto["description"]["Przebieg"])) {
	           $przebieg = $cars_scauto["description"]["Przebieg"]; 
            }

            if (isset($cars_scauto["description"]["VIN"])) {
	           $vin = $cars_scauto["description"]["VIN"]; 
            }

            if (isset($cars_scauto["description"]["Pierwsza rejestracja"])) {
	           $pierwsza_rejestracja = $cars_scauto["description"]["Pierwsza rejestracja"]; 
            }

            if (isset($cars_scauto["description"]["Koniec"])) {
	           $koniec = $cars_scauto["description"]["Koniec"]; 
            }

            if (isset($cars_scauto["end_date_of_auction"])) {
	           $end_date_of_auction = $cars_scauto["end_date_of_auction"]; 
            }

            $car_scauto = CarScauto::updateOrCreate(['photos_0' => $photos_0]);
        
			$car_scauto->operator = 'SCAuto';

            if (isset($photos_0)) {
			     $car_scauto->photos_0 = $photos_0;
            }

            if (isset($photos_1)) {
			     $car_scauto->photos_1 = $photos_1;
            }

            if (isset($photos_2)) {
			     $car_scauto->photos_2 = $photos_2;
            }

            if (isset($photos_3)) {
			     $car_scauto->photos_3 = $photos_3;
            }

            if (isset($photos_4)) {
			     $car_scauto->photos_4 = $photos_4;
            }

            if (isset($photos_5)) {
			     $car_scauto->photos_5 = $photos_5;
            }
            if (isset($photos_6)) {
                $car_scauto->photos_6 = $photos_6;
            }
            if (isset($photos_7)) {
                $car_scauto->photos_7 = $photos_7;
            }
            if (isset($photos_8)) {
                $car_scauto->photos_8 = $photos_8;
            }
            if (isset($photos_9)) {
                $car_scauto->photos_9 = $photos_9;
            }
            if (isset($photos_10)) {
                $car_scauto->photos_10 = $photos_10;
            }
            if (isset($photos_11)) {
                $car_scauto->photos_11 = $photos_11;
            }
            if (isset($photos_12)) {
                $car_scauto->photos_12 = $photos_12;
            }
            if (isset($photos_13)) {
                $car_scauto->photos_13 = $photos_13;
            }
            if (isset($photos_14)) {
                $car_scauto->photos_14 = $photos_14;
            }
            if (isset($photos_15)) {
                $car_scauto->photos_15 = $photos_15;
            }
            if (isset($photos_16)) {
                $car_scauto->photos_16 = $photos_16;
            }
            if (isset($photos_17)) {
                $car_scauto->photos_17 = $photos_17;
            }
            if (isset($photos_18)) {
                $car_scauto->photos_18 = $photos_18;
            }
            if (isset($photos_19)) {
                $car_scauto->photos_19 = $photos_19;
            }
            if (isset($photos_20)) {
                $car_scauto->photos_20 = $photos_20;
            }
            if (isset($photos_21)) {
                $car_scauto->photos_21 = $photos_21;
            }
            if (isset($photos_22)) {
                $car_scauto->photos_22 = $photos_22;
            }
            if (isset($photos_23)) {
                $car_scauto->photos_23 = $photos_23;
            }
            if (isset($photos_24)) {
                $car_scauto->photos_24 = $photos_24;
            }
            if (isset($photos_25)) {
                $car_scauto->photos_25 = $photos_25;
            }
            if (isset($photos_26)) {
                $car_scauto->photos_26 = $photos_26;
            }
            if (isset($photos_27)) {
                $car_scauto->photos_27 = $photos_27;
            }
            if (isset($photos_28)) {
                $car_scauto->photos_28 = $photos_28;
            }
            if (isset($photos_29)) {
                $car_scauto->photos_29 = $photos_29;
            }
            if (isset($photos_30)) {
                $car_scauto->photos_30 = $photos_30;
            }


            if (isset($marka)) {
                $car_scauto->marka = $marka;
            }

            if (isset($description2)) {
			     $car_scauto->description2 = $description2;
            }

            if (isset($typ)) {
			     $car_scauto->typ = $typ;
            }

            if (isset($model)) {
			     $car_scauto->model = $model;
            }

            if (isset($waga)) {
			     $car_scauto->waga = $waga;
            }

            if (isset($typ_pojazdu)) {
			     $car_scauto->typ_pojazdu = $typ_pojazdu;
            }

            if (isset($typ_silnika)) {
			     $car_scauto->typ_silnika = $typ_silnika;
            }

            if (isset($skrzynia_biegow)) {
			     $car_scauto->skrzynia_biegow = $skrzynia_biegow;
            }

            if (isset($naped)) {
			     $car_scauto->naped = $naped;
            }

            if (isset($pierwsza_rejestracja)) {
			     $car_scauto->pierwsza_rejestracja = $pierwsza_rejestracja;
            }

            if (isset($moc)) {
			     $car_scauto->moc = $moc;
            }

            if (isset($pojemnosc_silnika)) {
			     $car_scauto->pojemnosc_silnika = $pojemnosc_silnika;
            }

            if (isset($przebieg)) {
			     $car_scauto->przebieg = $przebieg;
            }

            if (isset($vin)) {
			     $car_scauto->vin = $vin;
            }

            if (isset($koniec)) {
			     $car_scauto->koniec = $koniec;
            }

            if (isset($end_date_of_auction)) {
			     $car_scauto->end_date_of_auction = $end_date_of_auction;
            }

            $car_scauto ->save();
        }

    	Session::flash('car_updated_scauto', 'Pojazdy SCAuto zaktualizowane poprawnie');
        return redirect('cms/update');
    }
}
