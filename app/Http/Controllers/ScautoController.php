<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CarScauto;
use Auth;
use Carbon\Carbon;

class ScautoController extends Controller
{
	public function __construct()
    {
        $this->middleware('admin_user_permission');
    }

    public function index()
    {
        $mytime = Carbon::now();
        $scautos = CarScauto::where('end_date_of_auction', '>', $mytime)->orderBy('end_date_of_auction', 'asc')->paginate(12);
    	return view('/scauto', compact('scautos'));
    }

    public function show($id)
    {
        $scauto = CarScauto::findOrFail($id);
        return view('/show-scauto', compact('scauto'));
    }
}
