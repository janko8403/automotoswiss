<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Page;

class PagesController extends Controller
{
    
    public function about()
    {
        $page = Page::findOrFail(1);
        return view('about', compact('page'));
    }

    public function transport()
    {
        $page = Page::findOrFail(2);
        return view('transport', compact('page'));
    }

    public function cookie()
    {
        $page = Page::findOrFail(3);
        return view('cookies', compact('page'));
    }

}
