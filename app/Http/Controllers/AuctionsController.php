<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Classes\SwissCars;
use Auth;
use App\Car;
use Session;

class AuctionsController extends Controller
{
    public function index()
    {
    	$cars = Car::orderBy('id', 'asc')->get();
    	return view('/auctions', compact('cars'));
    }


    public function show($id)
    {
        $sprowadzone = Car::findOrFail($id);
        return view('/show-sprowadzne', compact('sprowadzone'));
    }
}
