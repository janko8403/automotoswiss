<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Auctions;
use Session;

class ShowController extends Controller
{
    public function __construct()
    {
        $this->middleware('user_permission');
    }

    // public function index()
    // {
    // 	return view('/show');
    // }

    // public function show($auct_id)
    // {
    // 	$auction = Auctions::findOrFail($auct_id);
    //     return view('/show', compact('auction'));
    // }

    public function store(Request $request)
    {
        $this->validate($request, [
            'price' => 'required|min:2'], [
            'required' => 'To pole jest wymagane', 'numeric' => 'Wartość liczbowa', 'min' => 'Podaj minimalnie 2 liczby']
        );

        $id = $request['id'];
        $price = $request['price'];
        $date = $request['date'];
        $name = $request['name'];
        $user_id = $request['user_id'];
        $auct_id = $request['auct_id'];
        $operator = $request['operator'];
        
        $auction = new Auctions();
        $auction->price = $price;
        $auction->date = $date;
        $auction->name = $name;
        $auction->user_id = $user_id;
        $auction->auct_id = $auct_id;
        $auction->kto = $operator;

        $auction->save();

        // $nexmo = app('Nexmo\Client');

        // $nexmo->message()->send([
        //     'to'   => '48660450916',
        //     'from' => 'AutomotoSwiss',
        //     'text' => 'Imie: ' . Auth::user()->name . ' | Tel: ' . Auth::user()->phone . ' | Operator: ' . $operator . ' | Model: ' . $name . ' | Data aukcji: ' . $date . ' | Cena:' . $price . ' ChF',
        // ]);

        Session::flash('add_auction', 'Dziękujemy za wzięcie udziału w licytacji! Twoja kwota licytacji: ' . $request['price'] .  'chf');
        // return redirect('/allianz/' . $id);
        return back();
    }

    public function update(Request $request, $auct_id)
    {
        $this->validate($request, [
            'price' => 'required|min:2'], [
            'required' => 'To pole jest wymagane', 'numeric' => 'Wartość liczbowa', 'min' => 'Podaj minimalnie 2 liczby']
        );

        $auction = new Auctions();
        $auction->price = $price;
        $auction->date = $date;
        $auction->name = $name;

        $user = Auctions::findOrFail($id);
        $user->price = $request->price;
        $user->date = $request->date;
        $user->name = $request->name;

        $user->save();
        
        return redirect('cms/user');
    }
}
