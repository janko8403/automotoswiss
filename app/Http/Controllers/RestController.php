<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CarRest;
use Auth;
use Carbon\Carbon;

class RestController extends Controller
{
	public function __construct()
    {
        $this->middleware('admin_user_permission');
    }

    public function index()
    {   
        $mytime = Carbon::now();
        $rests = CarRest::where('end_date_of_auction', '>', $mytime)->orderBy('end_date_of_auction', 'asc')->paginate(12); 
    	return view('/rest', compact('rests'));
    }

    public function show($id)
    {
        $rest = CarRest::findOrFail($id);
        return view('/show-rest', compact('rest'));
    }
}
