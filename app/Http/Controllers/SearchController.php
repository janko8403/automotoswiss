<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\CarAllianz;
use App\CarRest;
use App\CarScauto;
use App\CarHome;

class SearchController extends Controller
{
    public function search()
    {
    	$search_phrase = Input::get('q');
    	
    	$search_allianz = CarAllianz::where('marka', 'like', '%' . $search_phrase . '%')
    								->orWhere('typ', 'like', '%' .$search_phrase . '%')
    								->orWhere('model', 'like', '%' .$search_phrase . '%')->paginate(10);

		$search_rest = CarRest::where('marka', 'like', '%' . $search_phrase . '%')
    								->orWhere('model', 'like', '%' .$search_phrase . '%')->paginate(10);

		$search_scauto = CarScauto::where('marka', 'like', '%' . $search_phrase . '%')
    								->orWhere('typ', 'like', '%' .$search_phrase . '%')
    								->orWhere('model', 'like', '%' .$search_phrase . '%')->paginate(10);

		$search_home = CarHome::where('car_name', 'like', '%' . $search_phrase . '%')->paginate(10);
    								// ->orWhere('typ', 'like', '%' .$search_phrase . '%')
    								// ->orWhere('model', 'like', '%' .$search_phrase . '%');

    	return view('/search', compact('search_allianz', 'search_rest', 'search_scauto', 'search_home'));
    }
}
