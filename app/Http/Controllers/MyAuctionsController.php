<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;
use App\Auctions;
use Session;

class MyauctionsController extends Controller
{

	public function __construct()
    {
        $this->middleware('user_permission');
    }

    public function index()
    {
        // $auctions = Auctions::orderBy('id', 'asc')->get();

        $auctions = DB::table('auctions')
        // ->select(DB::raw('MAX(price) AS price'))
        ->select(DB::raw('MAX(price) AS price, kto, name, date, auct_id'))
        ->where('user_id', Auth::id())
        ->groupBy('auct_id', 'kto', 'name', 'date')
        ->get();

        // dd($auctions);
        
    	return view('/my-auctions', compact('auctions'));
    }

    public function destroy($id)
    {
        $auction = Auctions::find($id);
        $auction->delete();

        Session::flash('auctions_destroy', 'Aukcja usunięta poprawnie');
        return redirect('/moje-aukcje');
    }
}
