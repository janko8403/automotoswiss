<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CarRest extends Model
{
    protected $fillable = [
       'photos_0', 'photos_1', 'photos_2', 'photos_3', 'photos_4', 'photos_5', 'photos_6', 'photos_7', 'photos_8', 'photos_9', 'photos_10', 'photos_11', 'photos_12', 'photos_13', 'photos_14', 'photos_15', 'photos_16', 'photos_17', 'photos_18', 'photos_19', 'photos_20', 'photos_21', 'photos_22', 'photos_23', 'photos_24', 'photos_25', 'photos_26', 'photos_27', 'photos_28', 'photos_29', 'photos_30', 'marka', 'podwozie', 'model', 'opony', 'kolor', 'konstrukcja', 'pierwsza_rejestracja', 'przebieg', 'mfk', 'dowod_wlasnosci', 'wyposazenie', 'opis_uszkodzenia', 'wystrzelone_poduszki', 'koniec', 'end_date_of_auction',
    ];
}
