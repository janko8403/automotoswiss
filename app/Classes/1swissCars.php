<?php

namespace App\Classes;

class SwissCars {

    private $login_user;
    private $login_pass;
    private $login_url;
    private $http_agent;
    private $cookie_file;
    private $logged;

    public function __construct() {
        $this->login_user =  'robert';
        $this->login_pass = ']5}4.KT8';
        $this->login_url = 'http://www.swisscars.pl/wp-login.php';
        $this->http_agent = "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.6) Gecko/20070725 Firefox/2.0.0.6";
        $this->cookie_file = "cookies/cookie.txt";
    }

    public function login() {
        $redirect_to = 'http://www.swisscars.pl/wp-admin/' ;

        $login = $this->logged = $this->curlGetWpLogin($redirect_to);
        if($login === false) {
            return false;
        }
    }

    public function checkLogin() {
        $content = $this->curlFindSite("http://www.swisscars.pl/rest");
        preg_match_all('|<h2 style="margin-left:180px;">(.*?)</h2>|',$content,$checkLogin);

        if ( isset($checkLogin[1][0]) && $checkLogin[1][0] === "Aby uzyskać dostęp do tej strony należy się zalogować!" ){
            $login = $this->login();
            if($login === false){
                return "Login error";
            }

            return false;
        } else {
            return true;
        }
    }

    public function getCars($type) {

        Switch($type) {
            case 'rest':
                $categories = 'http://www.swisscars.pl/rest/';
                break;
            case 'scauto':
                $categories = 'http://www.swisscars.pl/scauto/';
                break;
            case 'zabezpieczone-tresci':
                $categories = 'http://www.swisscars.pl/zabezpieczone-tresci/';
                break;
            case 'home':
                $categories = 'http://www.swisscars.pl/';
                break;
        }

        $site = $this->curlFindSite($categories);
        $links = $this->getUrlToCars($site);

        $page = 1;
        $saveLinks [] = $links;
        $i = count($links);

        while ($i >= 100 ){
           $page++;
           $site = $this->curlFindSite($categories.'page/'.$page.'/');
           $links2 = $this->getUrlToCars($site);
           $i = count($links2);
           $saveLinks [] = $links2;
        }
        if(isset($saveLinks) && !empty($saveLinks)){
            return $this->setDataForCar($saveLinks, $type);
        }
    }

    private function getUrlToCars($content) {
        $finddiv = explode('<!--- Post Starts -->',$content);
        unset($finddiv[0]);
        foreach ($finddiv as $cars) {
            preg_match_all('|<a title="" href="(.*?)">|',$cars,$Output);
            $linksToCars []= $Output[1][0];
        }
        if(isset($linksToCars) && !empty($linksToCars)){
           return $linksToCars;
        }
    }

    private function setDataForCar($links, $pageType = '') {
        for($i = 0; $i < count($links); $i++) {
          if (!empty($links[$i]) && is_array($links[$i])){
            foreach($links[$i] as $key => $link) {
                $allLinks []= $link;
            }
          }
        }

        foreach($allLinks as $key => $link) {
            //if($key < 9 or $pageType === 'home'){
            $dataCar = $this->curlFindSite($link);

            preg_match_all('|alt="" src="(.*?)" title=|',$dataCar,$carImages);
            preg_match_all('|<img src="(.*?)" alt="" class="thumb alignleft" |',$dataCar,$miniImages);
            preg_match_all('|<p>DATA ZAKONCZENIA AUKCJI: (.*?) <span|',$dataCar,$endAuction);
            $explode = explode("<table border='1' bordercolor='#000000' ", $dataCar);
            unset($explode[0]);

            for ($x = 1; $x <= count($explode); $x++ ){
                $expl = str_replace("<br />","</td>",$explode[$x]);
                $exp2 = str_replace(":","",$expl);
                preg_match_all('|<td>(.*?)</td>|',$exp2,$carInformationsTitle);
                if($x == 2 && isset($carInformationsTitle[1][0])){
                    $type [] = "Wyposazenie";

                    $info [] = $carInformationsTitle[1][0];
                }else {

                    for($a = 0; $a < count($carInformationsTitle[1]); $a++ ){

                        if($a%2 === 0){
                            $type [] =  $carInformationsTitle[1][$a];
                        } else {
                            $info [] =  $carInformationsTitle[1][$a];
                        }
                    }
                    for($b = 0; $b < count($type); $b++ ){
                        if( isset($type[$b]) && isset($info[$b])){
                            $informations [$type[$b]] = $info[$b];
                        }
                    }
                }
            }

            if (!isset($informations)){
              $a = explode('<h3>Fahrzeuginformationen</h3>', $dataCar);
              $a2 = explode('<div id="divTab2">', $a[1]);
              if (!empty($a2[0])){
                $informations = trim($a2[0]);
              }
            }

            if (!isset($informations)){
              $informations = '';
            }

              $carArray = array(
                      "link" => $link,
                      "link_to_thumbnails" => $miniImages[1][0],
                      "links_to_photos" => $carImages[1],
                      "description" => $informations,
                      "end_date_of_auction" => $endAuction[1][0]
                  );


                if ($pageType === 'home'){
                  preg_match('/(.*)">/', $dataCar, $originalAuctionLink);

                  $a = explode('Link do oryginalnej aukcji:<br /><a href="', $dataCar);
                  $a2 = explode('">', $a[1]);
                  if (!empty($a2[0])){
                      $originalAuctionLink = trim($a2[0]);

                      $carArray['originalAuctionLink'] = $originalAuctionLink;
                  }
                }


              if (preg_match("/<table border='1' bordercolor='#000000' cellpadding='4' cellspacing='1' width='500px' style='margin-bottom:3px;'>/", $dataCar)){
                $a = explode("<table border='1' bordercolor='#000000' cellpadding='4' cellspacing='1' width='500px' style='margin-bottom:3px;'>", $dataCar);
                $a2 = explode('</table>', $a[1]);

                if (!empty($a2[0])){
                  $desc2 = "<table border='1' bordercolor='#000000' cellpadding='4' cellspacing='1' width='500px' style='margin-bottom:3px;'>".trim($a2[0]).'</table>';

                  $carArray['description2'] = $desc2;
                }
              }


              if ($pageType === 'home'){
                preg_match("/<h2 id=\"VI_DescTitle\" style=\"display:block\">(.*)<\/h2>/", $dataCar, $car);
                if (isset($car[1])){
                  $carName = trim($car[1]);
                  $carArray['carName'] = $carName;
                }
              }

              $fullDataCars[] = $carArray;
            //}

            unset($informations);
        }
        return $fullDataCars;
    }

    private function curlFindSite($url) {
        $ch = curl_init();
        curl_setopt( $ch, CURLOPT_URL, $url );
	curl_setopt( $ch, CURLOPT_COOKIEFILE, $this->cookie_file );
	curl_setopt( $ch, CURLOPT_COOKIEJAR, $this->cookie_file );
	curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false );
	curl_setopt( $ch, CURLOPT_USERAGENT, $this->http_agent );
	curl_setopt( $ch, CURLOPT_TIMEOUT, 60 );
	curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, 1 );
	curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 );
	curl_setopt( $ch, CURLOPT_REFERER, $url );

	$content = curl_exec ($ch);
        curl_close( $ch );
            return $content;
    }

    private function curlGetWpLogin($redirect_to){

        $data = array(
            'log' => $this->login_user,
            'pwd' => $this->login_pass,
            'rememberme' => 'forever',
            'redirect_to' => $redirect_to,
            'testcookie' => 1
        );

	$ch = curl_init();

	curl_setopt( $ch, CURLOPT_URL, $this->login_url );
	curl_setopt( $ch, CURLOPT_COOKIEFILE, $this->cookie_file );
	curl_setopt( $ch, CURLOPT_COOKIEJAR, $this->cookie_file );
	curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false );
	curl_setopt( $ch, CURLOPT_USERAGENT, $this->http_agent );
	curl_setopt( $ch, CURLOPT_TIMEOUT, 60 );
	curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, 1 );
	curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 );
	curl_setopt( $ch, CURLOPT_REFERER, $this->login_url );
        curl_setopt( $ch, CURLOPT_POST, 1);
	curl_setopt( $ch, CURLOPT_POSTFIELDS, http_build_query($data) );

	$content = curl_exec ($ch);
        $checkLogin = curl_getinfo($ch, CURLINFO_EFFECTIVE_URL);
        curl_close( $ch );
        if($checkLogin !== "http://www.swisscars.pl/wp-admin"){
            return false;
        }
    }
}
