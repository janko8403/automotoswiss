<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCarsTable extends Migration
{

    public function up()
    {
        Schema::create('cars', function (Blueprint $table) {
            $table->increments('id');
            $table->string('marka');
            $table->string('model');
            $table->string('version');
            $table->string('old');
            $table->string('capacity');
            $table->string('km')->nullable();
            $table->string('vin')->nullable();
            $table->string('power')->nullable();
            $table->string('gas')->nullable();
            $table->string('transmission')->nullable();
            $table->string('drive')->nullable();
            $table->string('price')->nullable();
            $table->string('color')->nullable();
            $table->text('description')->nullable();
            $table->string('images')->nullable();
            $table->string('images_2')->nullable();
            $table->string('images_3')->nullable();
            $table->string('images_4')->nullable();
            $table->string('images_5')->nullable();
            $table->string('images_6')->nullable();
            $table->string('images_7')->nullable();
            $table->string('images_8')->nullable();
            $table->string('images_9')->nullable();
            $table->string('images_10')->nullable();
            $table->timestamps();
        });

        // DB::table('cars')->insert(array('id' => '1', 'model' => 'BMW 520', 'price' => '12500', 'course' => '145000', 'state' => 'uzywany', 'color' => 'biały', 'description' => 'wszystko ok', 'images' => 'ok'));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cars');
    }
}
