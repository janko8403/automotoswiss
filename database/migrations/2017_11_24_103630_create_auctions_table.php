<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAuctionsTable extends Migration
{
    
    public function up()
    {
        Schema::create('auctions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('auct_id');
            $table->string('kto');
            $table->string('date');
            $table->string('name');
            $table->string('price');
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
        });

        // DB::table('auctions')->insert(array('id' => '1', 'auct_id' => '1', 'user_id' => '3', 'date' => '12.12.2015', 'name' => 'BMW 320', 'price' => '12000'));
    }

    public function down()
    {
        Schema::dropIfExists('auctions');
    }
}
