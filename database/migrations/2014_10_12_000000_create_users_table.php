<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('phone');
            $table->string('password');
            $table->string('hash')->nullable();
            $table->tinyInteger('role_id')->unsigned();
            $table->rememberToken();
            $table->timestamps();
        });

        DB::table('users')->insert(array('id' => '1', 'name' => 'Janusz Jurczyk', 'email' => 'test@test.pl', 'password' => bcrypt('123Jurczyk123Janusz'), 'role_id' => '1', 'phone' => '555 555 555', 'hash' => ''));
        DB::table('users')->insert(array('id' => '2', 'name' => 'Edyta Łysiak', 'email' => 'ewawer@poczta.fm', 'password' => bcrypt('cms123456_!@#$#@'), 'role_id' => '1', 'phone' => '555 555 555', 'hash' => ''));
        DB::table('users')->insert(array('id' => '3', 'name' => 'Janusz', 'email' => 'janko8403@gmail.com', 'password' => bcrypt('111'), 'role_id' => '2', 'phone' => '555 555 555', 'hash' => ''));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
