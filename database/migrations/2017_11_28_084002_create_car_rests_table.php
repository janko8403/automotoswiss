<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCarRestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('car_rests', function (Blueprint $table) {
            $table->increments('id');
            $table->string('operator')->nullable();
            $table->string('photos_0')->nullable();
            $table->string('photos_1')->nullable();
            $table->string('photos_2')->nullable();
            $table->string('photos_3')->nullable();
            $table->string('photos_4')->nullable();
            $table->string('photos_5')->nullable();
            $table->string('photos_6')->nullable();
            $table->string('photos_7')->nullable();
            $table->string('photos_8')->nullable();
            $table->string('photos_9')->nullable();
            $table->string('photos_10')->nullable();
            $table->string('photos_11')->nullable();
            $table->string('photos_12')->nullable();
            $table->string('photos_13')->nullable();
            $table->string('photos_14')->nullable();
            $table->string('photos_15')->nullable();
            $table->string('photos_16')->nullable();
            $table->string('photos_17')->nullable();
            $table->string('photos_18')->nullable();
            $table->string('photos_19')->nullable();
            $table->string('photos_20')->nullable();
            $table->string('photos_21')->nullable();
            $table->string('photos_22')->nullable();
            $table->string('photos_23')->nullable();
            $table->string('photos_24')->nullable();
            $table->string('photos_25')->nullable();
            $table->string('photos_26')->nullable();
            $table->string('photos_27')->nullable();
            $table->string('photos_28')->nullable();
            $table->string('photos_29')->nullable();
            $table->string('photos_30')->nullable();
            $table->text('marka')->nullable(); 
            $table->text('model')->nullable(); 
            $table->text('podwozie')->nullable(); 
            $table->text('pierwsza_rejestracja')->nullable(); 
            $table->text('kolor')->nullable(); 
            $table->text('stan')->nullable(); 
            $table->text('opony')->nullable(); 
            $table->text('konstrukcja')->nullable(); 
            $table->text('dowod_wlasnosci')->nullable(); 
            $table->text('przebieg')->nullable(); 
            $table->text('mfk')->nullable(); 
            $table->text('wyposazenie')->nullable();
            $table->text('opis_uszkodzenia')->nullable(); 
            $table->text('strefa_obrazen')->nullable(); 
            $table->text('wystrzelone_poduszki')->nullable(); 
            $table->string('koniec')->nullable();
            $table->string('end_date_of_auction')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('car_rests');
    }
}
