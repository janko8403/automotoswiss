<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pages', function (Blueprint $table) {
            $table->increments('id');
            $table->text('name')->nullable(); 
            $table->text('description')->nullable(); 
            $table->timestamps();
        });

        DB::table('pages')->insert(array('id' => '1', 'name' => 'O nas', 'description' => '<h5>Witam na stronie firmy która zajmuje się licytacją, kompleksową odprawą oraz transportem aut ze
                            Szwajcarskich licytacji takich jak AXA Winterthur, Allianz, Restwertborse, Swisscrashcars, Ricardo.ch</h5>

                            <h5>Do współpracy zapraszam osoby prywatne oraz firmy. W przypadku tych drugich niezbędny jest NIP
                            EU, EORI (urząd celny) oraz upoważnienie przewoźnika do odprawy celnej.</h5>

                            <h5>Pełna oferta samochodów jest dostępna dla klientów zarejestrowanych na stronie. Po rejestracji
                            otrzymasz email z instrukcjami dotyczącym korzystania z serwisu.</h5>

                            <h5>Firma działa na rynku polskim od wielu lat co zapewnia bezpieczeństwo transakcji, rzetelność obsługi
                            oraz indywidualne podejście do każdego klienta.</h5>

                            <h5>Zapraszam do kontaktu w celu ustalenia wszelkich aspektów współpracy jak również do
                            szczegółowego zapoznania się z regulaminem firmy.</h5>'));
        DB::table('pages')->insert(array('id' => '2', 'name' => 'Transport', 'description' => '<h5>Autoera zajmuje się transportem samochodów osobowych i dostawczych. Specjalność firmy to
                            transport aut na trasie Szwajcaria - Polska. Oferujemy kompleksową, profesjonalną obsługę od
                            załadunku po dostarczenie samochodu pod wskazany adres w Polsce.</h5>

                            <h5>Obszar naszej działalności obejmuje także kompleksową obsługę celną.</h5>

                            <h5>Zaangażowanie, rzetelność i bardzo dobra znajomość rynku sprawiły, że od kilkunastu lat
                            gwarantujemy Klientom usługi transportowe najwyższej jakości.</h5>

                            <h5><strong>Skontaktuj się z nami – my z przyjemnością dowieziemy twoje auto do celu.</strong></h5>'));
        DB::table('pages')->insert(array('id' => '3', 'name' => 'Cookies', 'description' => '<ol start="1">
                                <li>Serwis nie zbiera w sposób automatyczny żadnych informacji, z wyjątkiem informacji zawartych w plikach cookies.</li>
                                <li>Pliki cookies (tzw. „ciasteczka”) stanowią dane informatyczne, w szczególności pliki tekstowe, które przechowywane są w urządzeniu końcowym Użytkownika Serwisu i przeznaczone są do korzystania ze stron internetowych Serwisu. Cookies zazwyczaj zawierają nazwę strony internetowej, z której pochodzą, czas przechowywania ich na urządzeniu końcowym oraz unikalny numer.</li>
                                <li>Podmiotem zamieszczającym na urządzeniu końcowym Użytkownika Serwisu pliki cookies oraz uzyskującym do nich dostęp jest operator Serwisu sautomotoswiss.pl z siedzibą pod adresem ul. Kwiatowa 52 Miętne 08-400 Garwolin</li>
                            </ol>
                            <ol>
                                <li>a)      dostosowania zawartości stron internetowych Serwisu do preferencji Użytkownika oraz optymalizacji korzystania ze stron internetowych; w szczególności pliki te pozwalają rozpoznać urządzenie Użytkownika Serwisu i odpowiednio wyświetlić stronę internetową, dostosowaną do jego indywidualnych potrzeb;</li>
                                <li>b)      tworzenia statystyk, które pomagają zrozumieć, w jaki sposób Użytkownicy Serwisu korzystają ze stron internetowych, co umożliwia ulepszanie ich struktury i zawartości;</li>
                                <li>c)       utrzymanie sesji Użytkownika Serwisu (po zalogowaniu), dzięki której Użytkownik nie musi na każdej podstronie Serwisu ponownie wpisywać loginu i hasła;</li>
                            </ol>
                            <ol start="5">
                                <li>W ramach Serwisu stosowane są dwa zasadnicze rodzaje plików cookies: „sesyjne”  (<i>session cookies</i>) oraz „stałe” (<i>persistent cookies</i>). Cookies „sesyjne” są plikami tymczasowymi, które przechowywane są w urządzeniu końcowym Użytkownika do czasu wylogowania, opuszczenia strony internetowej lub wyłączenia oprogramowania (przeglądarki internetowej). „Stałe” pliki cookies przechowywane są w urządzeniu końcowym Użytkownika przez czas określony w parametrach plików cookies lub do czasu ich usunięcia przez Użytkownika.</li>
                                <li>W ramach Serwisu stosowane są następujące rodzaje plików cookies:</li>
                            </ol>
                            <ol>
                                <li>a)      „niezbędne” pliki cookies, umożliwiające korzystanie z usług dostępnych w ramach Serwisu, np. uwierzytelniające pliki cookies wykorzystywane do usług wymagających uwierzytelniania w ramach Serwisu;</li>
                                <li>b)      pliki cookies służące do zapewnienia bezpieczeństwa, np. wykorzystywane do wykrywania nadużyć w zakresie uwierzytelniania w ramach Serwisu;</li>
                                <li>c)       „wydajnościowe” pliki cookies, umożliwiające zbieranie informacji o sposobie korzystania ze stron internetowych Serwisu;</li>
                                <li>d)      „funkcjonalne” pliki cookies, umożliwiające „zapamiętanie” wybranych przez Użytkownika ustawień i personalizację interfejsu Użytkownika, np. w zakresie wybranego języka lub regionu, z którego pochodzi Użytkownik, rozmiaru czcionki, wyglądu strony internetowej itp.;</li>
                                <li>e)      „reklamowe” pliki cookies, umożliwiające dostarczanie Użytkownikom treści reklamowych bardziej dostosowanych do ich zainteresowań.</li>
                            </ol>
                            <ol start="7">
                                <li>W wielu przypadkach oprogramowanie służące do przeglądania stron internetowych (przeglądarka internetowa) domyślnie dopuszcza przechowywanie plików cookies w urządzeniu końcowym Użytkownika. Użytkownicy Serwisu mogą dokonać w każdym czasie zmiany ustawień dotyczących plików cookies. Ustawienia te mogą zostać zmienione w szczególności w taki sposób, aby blokować automatyczną obsługę plików cookies w ustawieniach przeglądarki internetowej bądź informować o ich każdorazowym zamieszczeniu w urządzeniu Użytkownika Serwisu. Szczegółowe informacje o możliwości i sposobach obsługi plików cookies dostępne są w ustawieniach oprogramowania (przeglądarki internetowej).</li>
                                <li>Operator Serwisu informuje, że ograniczenia stosowania plików cookies mogą wpłynąć na niektóre funkcjonalności dostępne na stronach internetowych Serwisu.</li>
                                <li>Pliki cookies zamieszczane w urządzeniu końcowym Użytkownika Serwisu i wykorzystywane mogą być również przez współpracujących z operatorem Serwisu reklamodawców oraz partnerów.</li>
                            </ol>'));
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pages');
    }
}
