<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCarScautosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('car_scautos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('operator')->nullable();
            $table->string('photos_0')->nullable();
            $table->string('photos_1')->nullable();
            $table->string('photos_2')->nullable();
            $table->string('photos_3')->nullable();
            $table->string('photos_4')->nullable();
            $table->string('photos_5')->nullable();
            $table->string('photos_6')->nullable();
            $table->string('photos_7')->nullable();
            $table->string('photos_8')->nullable();
            $table->string('photos_9')->nullable();
            $table->string('photos_10')->nullable();
            $table->string('photos_11')->nullable();
            $table->string('photos_12')->nullable();
            $table->string('photos_13')->nullable();
            $table->string('photos_14')->nullable();
            $table->string('photos_15')->nullable();
            $table->string('photos_16')->nullable();
            $table->string('photos_17')->nullable();
            $table->string('photos_18')->nullable();
            $table->string('photos_19')->nullable();
            $table->string('photos_20')->nullable();
            $table->string('photos_21')->nullable();
            $table->string('photos_22')->nullable();
            $table->string('photos_23')->nullable();
            $table->string('photos_24')->nullable();
            $table->string('photos_25')->nullable();
            $table->string('photos_26')->nullable();
            $table->string('photos_27')->nullable();
            $table->string('photos_28')->nullable();
            $table->string('photos_29')->nullable();
            $table->string('photos_30')->nullable();
            $table->text('marka')->nullable();
            $table->text('model')->nullable();
            $table->text('typ')->nullable();
            $table->text('typ_pojazdu')->nullable();
            $table->text('typ_silnika')->nullable();
            $table->text('skrzynia_biegow')->nullable();
            $table->text('naped')->nullable();
            $table->text('moc')->nullable();
            $table->text('pierwsza_rejestracja')->nullable();
            $table->text('pojemnosc_silnika')->nullable();
            $table->text('przebieg')->nullable();
            $table->text('waga')->nullable();
            $table->text('vin')->nullable();
            $table->text('description2')->nullable();
            $table->string('koniec')->nullable();
            $table->string('end_date_of_auction')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('car_scautos');
    }
}
